USE master;
IF EXISTS (SELECT * FROM sys.databases WHERE name = N'hkkdb')
BEGIN
	DROP DATABASE hkkdb
END;
CREATE DATABASE hkkdb;
	
IF EXISTS (SELECT * FROM sys.server_principals WHERE name = N'hkkadmin')
BEGIN
	DROP LOGIN hkkadmin
END;
CREATE LOGIN hkkadmin WITH PASSWORD = 'HkkAdmn-1005';
GO

USE hkkdb;

IF EXISTS (SELECT * FROM sys.database_principals WHERE name = N'hkkadmin')
BEGIN
	DROP USER hkkadmin
END;
CREATE USER [hkkadmin] FOR LOGIN [hkkadmin]
EXEC sp_addrolemember N'db_owner', N'hkkadmin'
