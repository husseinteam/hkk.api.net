﻿using System;
using NUnit.Framework;
using HKK.Core.UnitTest;
using HKK.Domain;
using HKK.Core.Engines;
using HKK.Core.Extensions.Static;
using HKK.Domain.Main.Views;
using HKK.Engine.Dhikrs;
using HKK.Core.Domain.Contracts;
using HKK.Core.Domain.Aggregate;
using System.Reflection;
using System.Linq;
using System.Threading.Tasks;
using HKK.Core.Api.CoreModel.Seeders;
using HKK.Domain.Views.Dhikrs;
using HKK.Engine.Hadiths;

namespace HKK.UnitTests.DomainTests {

    [TestFixture]
    public class DomainDataTests : DataEngineTestBase {

        [OneTimeSetUp]
        public static void InitAll() {
            Builder.RebuildDomain();
        }

        [SetUp]
        public override void Bootstrap() {
            Builder.Bootstrap();
        }

        public override void RebuildDomain() {
            Builder.RebuildDomain();
        }

        [Test]
        public void InsertsDhikrs() {
            var title = "Hakkani Zikri";
            var resp = SDhikrEngine.SeedHakkaniDhikr(title).InsertSelf();
            Assert.IsTrue(resp.HasData, resp.Message);
            Assert.IsFalse(resp.HasErrors, resp.Message);
            var zgview = resp.SingleOrFirst as DhikrGroupView;
            Assert.AreEqual(title, zgview.DhikrGroupName);
            Assert.AreNotEqual(0, zgview.ID);
            Assert.AreNotEqual(0, zgview.DhikrGroupID);
        }

        [Test]
        public async Task InsertsUser() {
            var azav = await SAzaSeeder.NewUserWith(SAzaSeeder.GetAdminSession());
            Assert.AreNotEqual(0, azav.UserID);
            Assert.AreNotEqual(0, azav.UserContactID);
            Assert.AreNotEqual(0, azav.Consents.Count);
        }

        [Test]
        public void InsertsHadiths() {
            var hadiths = SHadithEngine.CollectHadiths().Take(1001);
            var r = hadiths.InsertUs((i, response) => {
                Assert.IsTrue(response.HasData, response.Message);
                Assert.IsFalse(response.HasErrors, response.Message);
            });
            Assert.IsFalse(r.HasErrors, r.Message);
        }

        [Test]
        public void SelectByComparison() {
            var obj = new ScholarView {
                ScholarFullName = "Hüseyin Allahverdi",
                ScholarDayOfBirth = 1981.RandomDate()
            };
            Assert.IsTrue(obj.InsertThat(out var inserted).HasData);
            var type = typeof(ScholarView);
            var aggregate = type.GetProperty("ScholarFullName") as MemberInfo;
            var existing = SDataEngine.GenerateAGEngine(typeof(ScholarView)).GenericSelectByComparison(
                            aggregate, aggregate.GetValue(inserted));
            Assert.IsTrue(existing.HasData);
            Assert.IsTrue(inserted.DeleteSelf().HasData);
        }

        private IHDSResponse InsertSource() {
            var sourceView = new SourceView {
                SourceTitle = 3.RandomWords(),
                ScholarBiography = 11.RandomWords(),
                ScholarDayOfBirth = 1453.RandomNumberBetween(1205).RandomDate(),
                ScholarDayOfPassAway = 1532.RandomNumberBetween(1502).RandomDate(),
                ScholarFullName = 2.RandomFullName(),
                ScholarPicture = true.RandomImage(),
            };
            var aggResponse = sourceView.InsertSelf();
            Assert.IsTrue(aggResponse.HasData, aggResponse.Message);
            return aggResponse;
        }

    }
}
