using System;
using NUnit.Framework;
using System.Threading.Tasks;
using System.Linq;
using System.Diagnostics;
using HKK.Engine.Forecasts;
using HKK.Engine.Calendar;
using HKK.Core.Extensions.Static;

namespace HKK.UnitTests.EngineTests {
	[TestFixture]
	public class EngineTests {

		[Test]
		public async Task CalendarCityInfoOK() {
			var info = await SCalendarEngine.GetCalendarInfo("Bolu", "TR");
			Assert.AreEqual("Europe/Istanbul", info.Timezone);
		}

        [Test]
        public async Task CalendarGetOK() {
            var info = await SCalendarEngine.GetCalendarInfo("Bolu", "TR");
            var calendar = await SCalendarEngine.GetMonthlyTimes(info, 1, 2017);
            Assert.AreNotEqual(0, calendar.Timings.Count);
            calendar.Timings.ForEach(timing => {
                Assert.IsFalse(string.IsNullOrEmpty(timing.TimingImsak));
            });
        }

        [Test]
        public async Task ForecastsGetOK() {
            var dailyForecasts = await SForecastEngine.GetDailyForecasts("Bolu", "TR");
            Assert.AreEqual("Bolu", dailyForecasts.location.Location);
            dailyForecasts.forecasts.Each((i, forecast) => {
                Assert.IsNotNull(forecast.TemperatureMax);
            });
        }

    }
}
