﻿using HKK.Core.Domain.Objects;
using HKK.Core.Engines;
using HKK.Core.Extensions.Static;
using HKK.Domain.Main.Views;
using HKK.Domain.Models.Dhikrs;
using HKK.Domain.Views.Dhikrs;
using HtmlAgilityPack;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace HKK.Engine.Hadiths {
    public static class SHadithEngine {

        public static IEnumerable<HadithView> CollectHadiths() {
            var web = new HtmlWeb {
                AutoDetectEncoding = false,
                OverrideEncoding = Encoding.GetEncoding("windows-1254")
            };
            var list = new List<HadithView>();
            var hadithsFile = new FileInfo(
                Path.Combine(AppDomain.CurrentDomain.BaseDirectory, AppDomain.CurrentDomain.RelativeSearchPath ?? "", "..", "..", "Static", "hadiths.txt"));
            if (hadithsFile.Exists == false) {
                try {
                    hadithsFile = new FileInfo(
                                Path.Combine(HttpRuntime.AppDomainAppPath, "Static", "hadiths.txt"));
                } catch { }
            }
            if (hadithsFile.Exists == false) {
                using (var sr = new StreamWriter(hadithsFile.OpenWrite(), Encoding.GetEncoding("windows-1254"))) {
                    for (int page = 1; page <= 598; page++) {
                        var url = $"http://muhaddis.org/cgi-bin/dbman/db.cgi?db=ks&uid=default&view_records=1&SNo=*&nh={page}";
                        var doc = web.Load(url);
                        doc.DocumentNode.Descendants("table")
                            .Where(y => y.Attributes["align"] != null)
                            .Where(y => y.Attributes["align"].Value == "center").Each((i, table) => {
                                var rows = table.Descendants("tr");
                                string extractor(int k) {
                                    var str = rows.ElementAt(k).Descendants("td").Skip(1).First().Descendants("font").First().InnerText;
                                    return string.IsNullOrEmpty(str) ? "<??>" : str;
                                }
                                string line() {
                                    var ln = $@"{extractor(0)}#{extractor(1)}#{extractor(2)}#{extractor(3)}#{extractor(4)}";
                                    return ln;
                                }
                                sr.WriteLine(line());
                            });
                    }
                }
            }

            using (var sr = new StreamReader(hadithsFile.OpenRead(), Encoding.GetEncoding("windows-1254"))) {
                while (sr.EndOfStream == false) {
                    (string, string, string, string, string) all() {
                        var splitted = sr.ReadLine().Split('#');
                        return (
                            splitted.ElementAtOrDefault(0),
                            splitted.ElementAtOrDefault(1),
                            splitted.ElementAtOrDefault(2),
                            splitted.ElementAtOrDefault(3),
                            splitted.ElementAtOrDefault(4)
                        );
                    }
                    var (quarter, subject, source, transcriptor, hadith) = all();
                    list.Add(new HadithView {
                        HadithTranscriptorFullName = transcriptor,
                        HadithSourceTitle = source,
                        HadithText = hadith,
                        HadithQuarterKey = Guid.NewGuid().ToString("N"),
                        HadithQuarterValue = quarter,
                        HadithQuarterLanguageCode = "tr-TR",
                        HadithSubjectKey = Guid.NewGuid().ToString("N"),
                        HadithSubjectValue = subject,
                        HadithSubjectLanguageCode = "tr-TR",
                    });
                }
            }
            return list.ToArray();
        }

    }
}
