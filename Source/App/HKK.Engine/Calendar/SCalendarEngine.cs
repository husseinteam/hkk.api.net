﻿using HKK.Engine.Calendar.Constants;
using HKK.Core.Extensions.Static;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using HKK.Core.Api.APIExtensions;
using HKK.Engine.Calendar.Vectors;
using HKK.Engine.Calendar.Enums;
using HKK.Domain.Views.PrayerTimes;
using HKK.Resources.Values;
using HKK.Core.Engines;
using HKK.Core.Domain.Contracts;
using HKK.Engine.Forecasts.Enums;
using HKK.Engine.Forecasts;
using HKK.Core.Domain.Aggregate;

namespace HKK.Engine.Calendar {

    public static class SCalendarEngine {

        public static async Task<CalendarView> GetMonthlyTimes(CalendarInfo info, int month, int year, EMazhab mazhab = EMazhab.Shafii, EAdhanMethod adhanMethod = EAdhanMethod.Makkah, EAdjustmentMethod adjustmentMethod = EAdjustmentMethod.AngleBased) {

            var timeZone = info.Timezone.Replace("/", "%2F");
            var method = (int)adhanMethod; //Umm al-Qura, Makkah
            var route = $"calendar?latitude={info.Latitude}&longitude={info.Longitude}&timezonestring={timeZone}&method={(int)adhanMethod}&month={month}&year={year}&school={(int)mazhab}&latitudeAdjustmentMethod={(int)adjustmentMethod}".Replace(",", ".");
            var response = await "http://api.aladhan.com/".RequestRouteModifying(route, (json) => {
                var cal = new CalendarView() {
                    CalendarKey = info.Location,
                    CalendarLatitude = info.Latitude.ParseExactDecimal(),
                    CalendarLongitude = info.Longitude.ParseExactDecimal(),
                    CalendarMonth = month,
                    CalendarYear = year
                };
                var pattern = @"(\s\([+|-]\d\d\))";
                foreach (var data in json["data"]) {
                    var timings = data["timings"];
                    var date = data["date"];
                    cal.Timings.Add(new TimingView() {
                        TimingReadableDate = Convert.ToDateTime(date["readable"]),
                        TimingTimestamp = Convert.ToInt32(date["timestamp"]),
                        TimingFajr = timings["Fajr"].ToString().ReplaceByPattern(pattern, ""),
                        TimingSunrise = timings["Sunrise"].ToString().ReplaceByPattern(pattern, ""),
                        TimingDhuhr = timings["Dhuhr"].ToString().ReplaceByPattern(pattern, ""),
                        TimingAsr = timings["Asr"].ToString().ReplaceByPattern(pattern, ""),
                        TimingSunset = timings["Sunset"].ToString().ReplaceByPattern(pattern, ""),
                        TimingMaghrib = timings["Maghrib"].ToString().ReplaceByPattern(pattern, ""),
                        TimingIsha = timings["Isha"].ToString().ReplaceByPattern(pattern, ""),
                        TimingImsak = timings["Imsak"].ToString().ReplaceByPattern(pattern, ""),
                        TimingMidnight = timings["Midnight"].ToString().ReplaceByPattern(pattern, ""),
                    });
                }
                return cal;
            });
            return response.ResponseObject;

        }

        public static async Task<IHDSResponse> UpsertMonthlyTimings(int month, int year) {
            var calendarEngine = SDataEngine.GenerateAGEngine<CalendarView>();
            var existingCalendarResponse = await calendarEngine.SelectBy(glView => glView.CalendarMonth == month && glView.CalendarYear == year);
            if (existingCalendarResponse.HasData == false) {
                var resp = SLocationsEngine.Locals(ECountry.Turkiye).Map((i, city) => {
                    var calendarInfo = AsyncExtensions.AwaitResult(() => GetCalendarInfo(city, "tr"));
                    return AsyncExtensions.AwaitResult(() => GetMonthlyTimes(calendarInfo, month, year));
                }).InsertUs();
                return resp;
            } else {
                return existingCalendarResponse;
            }
        }

        public static async Task<CalendarInfo> GetCalendarInfo(string city, string countryCode) {

            var response = await "http://api.aladhan.com/".RequestRouteModifying($"cityInfo?city={city}&country={countryCode}", json => {
                var info = json["data"];
                return new CalendarInfo() {
                    Latitude = info["latitude"].ToString(),
                    Longitude = info["longitude"].ToString(),
                    Timezone = info["timezone"].ToString(),
                    Location = city
                };
            });
            return response.ResponseObject;

        }

        public static async Task<SpatialAddressInfo> GetReverseSpatialAddressInfo(string latitude, string longitude) {
            var response = await "https://maps.googleapis.com/".RequestRouteModifying(
                $"maps/api/geocode/json?latlng={latitude.ParseExactDecimalString()},{longitude.ParseExactDecimalString()}&result_type=administrative_area_level_2&key={Keys.GoogleMapsAPIKey}",
                json => {
                    var result = json["results"].First;
                    var addressInfo = new SpatialAddressInfo();
                    foreach (var component in result["address_components"]) {
                        if (component["types"][0].ToString() == "administrative_area_level_2") {
                            addressInfo.Region = component["short_name"].ToString();
                        }
                        if (component["types"][0].ToString() == "administrative_area_level_1") {
                            addressInfo.City = component["short_name"].ToString();
                        }
                        if (component["types"][0].ToString() == "country") {
                            addressInfo.Country = component["long_name"].ToString();
                            addressInfo.CountryCode = component["short_name"].ToString();
                        }
                    }
                    return addressInfo;
                });
            return response.ResponseObject;
        }

    }

}
