﻿
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HKK.Engine.Calendar.Enums {
    public enum EMazhab : int {

        [Description("Shafii")]
        Shafii = 0,
        [Description("Hanafi")]
        Hanafi = 1

    }
}
