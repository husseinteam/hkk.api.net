﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HKK.Engine.Calendar.Enums {

    public enum EAdhanMethod : int {

        [Description("University of Islamic Sciences")]
        Karachi = 1,
        [Description("Islamic Society of North America")]
        ISNA = 2,
        [Description("Muslim World League")]
        MWL = 3,
        [Description("Umm al-Qura")]
        Makkah = 4,
        [Description("Egyptian General Authority of Survey")]
        Egyptian = 5

    }

}
