﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HKK.Engine.Calendar.Enums {

    public enum EAdjustmentMethod : int {

        [Description("Middle of the Night")]
        MiddleOfNight = 1,
        [Description("One Seventh")]
        OneSeventh = 2,
        [Description("Angle Based")]
        AngleBased = 3

    }

}
