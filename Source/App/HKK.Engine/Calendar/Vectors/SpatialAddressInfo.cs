﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HKK.Engine.Calendar.Vectors {

    public class SpatialAddressInfo {

        public string Region { get; set; }
        public string City { get; set; }
        public string CountryCode { get; set; }
        public string Country { get; set; }

    }

}
