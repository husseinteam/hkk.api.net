﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HKK.Engine.Calendar.Vectors {

	public class CalendarInfo {

		public string Latitude { get; set; }
		public string Longitude { get; set; }
		public string Timezone { get; set; }
		public string Location { get; set; }

    }

}
