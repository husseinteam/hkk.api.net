﻿using HKK.Core.Domain.Contracts;
using HKK.Core.Domain.Objects;
using HKK.Core.Engines;
using HKK.Core.Extensions.Static;
using HKK.Domain.Models.Dhikrs;
using HKK.Domain.Views.Dhikrs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HKK.Engine.Dhikrs {
    public static class SDhikrEngine {

        public static DhikrGroupView SeedHakkaniDhikr(string zikhirGroupTitle) {
            var hakkaniGroupView = new DhikrGroupView() {
                DhikrGroupName = zikhirGroupTitle,
                DhikrGroupAppeal = "Allahım niyyet ettim zat-ı ilahiyyeni zikretmeye.",
                DhikrGroupEndowment = "Bu okuduklarımızdan hâsıl olan sevabı bilhassa Efendimizin (Sallallahü Aleyhi ve Sellem) Mübarek Rûh-u Şerîfine, Enbiya ve Evliyaların da rûhlarına, hassaten Şâh-ı Nakşibendi Hz.lerinin Rûh-u Şerîfine, Mevlânâ Şeyh Abdullah el Fâizi Dağıstanî Hz.lerinin Rûh-u Şerîfine, Mevlânâ Sultan-u Evliya Şeyh Muhammed Nazım El Hakkani Hz.lerinin Ruh-u Şerîfine bahusus Mevlânâ Sultan-u Evliya Şeyh Muhammed Mehmet El Hakkani  Er Rabbani Hz.lerinin Ruhaniyetlerine hediyye eyledim. Ya Rabbi sen vâsıl eyle. Âmin.",
            };
            var zikhirs = new[] {
                    new DhikrView() {
                        DhikrGroupTitle = zikhirGroupTitle,
                        DhikrTitle = "BİSMİLLAHİRRAHMANİRRAHİM",
                        DhikrTranscript = "BİSMİLLAHİRRAHMANİRRAHİM",
                        DhikrTarget = 1,
                    },
                    new DhikrView() {
                        DhikrGroupTitle = zikhirGroupTitle,
                        DhikrTitle = "Kelime-i Şehâdet",
                        DhikrTranscript = "Eşhedu en lâ ilâhe illallâh ve eşhedü enne Muhammeden Abdühü ve Resülühü",
                        DhikrTarget = 3,
                    },
                    new DhikrView() {
                        DhikrGroupTitle = zikhirGroupTitle,
                        DhikrTitle = "Estâğfirullah",
                        DhikrTranscript = "Estâğfirullah",
                        DhikrTarget = 70,
                    },
                    new DhikrView() {
                        DhikrGroupTitle = zikhirGroupTitle,
                        DhikrTitle = "Fatiha-i Şerife",
                        DhikrTranscript = "Bismillahirrahmânirrahîm.. Elhamdü lillâhi rabbil'alemin. Errahmânir'rahim. Mâliki yevmiddin. İyyâke na'budü ve iyyâke neste'în. İhdinessırâtel müstakîm. Sırâtellezine en'amte aleyhim ğayrilmağdûbi aleyhim ve leddâllîn. Amîn",
                        DhikrTarget = 1,
                    },
                    new DhikrView() {
                        DhikrGroupTitle = zikhirGroupTitle,
                        DhikrTitle = "Amen er Rasulü",
                        DhikrTranscript = "Bismillahirrahmânirrahîm. AmenerRasulü bima ünzile ileyhi mirrabbihi vel mü’minun, küllün amene billahi vemelaiketihi ve kütübihi ve rusülih, la nüferriku beyne ehadin min rusülih, ve kalu semi’na ve ata’na gufraneke rabbena ve ileykelmesir. La yükellifullahü nefsenilla vüs’aha, leha ma kesebet ve aleyha mektesebet, rabbena latüahızna innesiyna ev ahta’na, rabbena vela tahmil aleyna ısran kema hameltehü alelleziyne min gablina, rabbena vela tühammilna, mala takatelena bih, va’fü anna, vağfirlena, verhamna, ente mevlana fensurna alel gavmil kafiriyn. Amîn",
                        DhikrTarget = 1,
                    },
                    new DhikrView() {
                        DhikrGroupTitle = zikhirGroupTitle,
                        DhikrTitle = "Inşirah",
                        DhikrTranscript = "Bismillahirrahmânirrahîm. Elem neşrah leke sadrak Ve vada'na 'anke vizrake Elleziy enkada zahrake Ve refa'na leke zikrake Feinne me'al'usri yüsran İnne me'al'usri yüsran Feiza ferağte fensab Ve ila rabbike ferğab. Allâh-ü Ekber",
                        DhikrTarget = 7,
                    },
                    new DhikrView() {
                        DhikrGroupTitle = zikhirGroupTitle,
                        DhikrTitle = "İhlas-ı Şerif",
                        DhikrTranscript = "Bismillahirrahmânirrahîm. Kul hüvellâhü ehad. Allâhüssamed. Lem yelid ve lem yûled. Ve lem yekün lehû küfüven ehad. Allâh-ü Ekber",
                        DhikrTarget = 11,
                    },
                    new DhikrView() {
                        DhikrGroupTitle = zikhirGroupTitle,
                        DhikrTitle = "Felak",
                        DhikrTranscript = "Bismillahirrahmânirrahîm. Kul e'ûzü birabbil felak. Min şerri mâ halak. Ve min şerri ğasikın izâ vekab. Ve min şerrinneffâsâti fil'ukad. Ve min şerri hâsidin izâ hased. Allâh-ü Ekber",
                        DhikrTarget = 1,
                    },
                    new DhikrView() {
                        DhikrGroupTitle = zikhirGroupTitle,
                        DhikrTitle = "Nâs",
                        DhikrTranscript = "Bismillahirrahmânirrahîm. Kul e'ûzü birabbinnâs. Melikinnâs. İlâhinnâs. Min şerrilvesvâsilhannâs. Ellezî yüvesvisü fî sudûrinnâsi. Minelcinneti vennâs. Allâh-ü Ekber",
                        DhikrTarget = 1,
                    },
                    new DhikrView() {
                        DhikrGroupTitle = zikhirGroupTitle,
                        DhikrTitle = "Kelime-i Tevhid",
                        DhikrTranscript = "Lâ İlâhe İllallah",
                        DhikrTarget = 9,
                    },
                    new DhikrView() {
                        DhikrGroupTitle = zikhirGroupTitle,
                        DhikrTitle = "Kelime-i Tevhid",
                        DhikrTranscript = "Lâ İlâhe İllallah Muhammedûn Resulullah",
                        DhikrTarget = 1,
                    },
                    new DhikrView() {
                        DhikrGroupTitle = zikhirGroupTitle,
                        DhikrTitle = "Salavat-ı Şerife",
                        DhikrTranscript = "Allâhümme salli âla Muhammedin ve âla Âli Muhammedin ve Sellim",
                        DhikrTarget = 10,
                    },
                    new DhikrView() {
                        DhikrGroupTitle = zikhirGroupTitle,
                        DhikrTitle = "Fatiha-i Şerife",
                        DhikrTranscript = "Bismillahirrahmânirrahîm. Elhamdü lillâhi rabbil'alemin. Errahmânir'rahim. Mâliki yevmiddin. İyyâke na'budü ve iyyâke neste'în. İhdinessırâtel müstakîm. Sırâtellezine en'amte aleyhim ğayrilmağdûbi aleyhim ve leddâllîn. Amîn",
                        DhikrTarget = 1,
                    },
                    new DhikrView() {
                        DhikrGroupTitle = zikhirGroupTitle,
                        DhikrTitle = "Lafza-ı Celâl",
                        DhikrTranscript = "Allah",
                        DhikrTarget = 1500,
                    },
                    new DhikrView() {
                        DhikrGroupTitle = zikhirGroupTitle,
                        DhikrTitle = "Tövbe",
                        DhikrTranscript = "Estağfürullah",
                        DhikrTarget = 1000,
                    },
                    new DhikrView() {
                        DhikrGroupTitle = zikhirGroupTitle,
                        DhikrTitle = "Kelime-i Tevhid",
                        DhikrTranscript = "Lâ İlâhe İllallah",
                        DhikrTarget = 1000,
                    },
                    new DhikrView() {
                        DhikrGroupTitle = zikhirGroupTitle,
                        DhikrTitle = "Salavat-ı Şerife",
                        DhikrTranscript = "Allâhümme salli âla Muhammedin ve âla Âli Muhammedin ve Sellim",
                        DhikrTarget = 300,
                    },
                    new DhikrView() {
                        DhikrGroupTitle = zikhirGroupTitle,
                        DhikrTitle = "İhlas-ı Şerif",
                        DhikrTranscript = "Bismillahirrahmânirrahîm. Kul hüvellâhü ehad. Allâhüssamed. Lem yelid ve lem yûled. Ve lem yekün lehû küfüven ehad. Allâh-ü Ekber",
                        DhikrTarget = 100,
                    },
                    new DhikrView() {
                        DhikrGroupTitle = zikhirGroupTitle,
                        DhikrTitle = "Salavat-ı Şerife",
                        DhikrTranscript = "Allâhümme salli âla Muhammedin ve âla Âli Muhammedin ve Sellim",
                        DhikrTarget = 100,
                    },
                    new DhikrView() {
                        DhikrGroupTitle = zikhirGroupTitle,
                        DhikrTitle = "BAĞIŞLAMA",
                        DhikrTranscript = "Bu okuduklarımızdan hâsıl olan sevabı bilhassa Efendimizin (Sallallahü Aleyhi ve Sellem) Mübarek Rûh-u Şerîfine, Enbiya ve Evliyaların da rûhlarına, hassaten Şâh-ı Nakşibendi Hz.lerinin Rûh-u Şerîfine, Mevlânâ Şeyh Abdullah el Fâizi Dağıstanî Hz.lerinin Rûh-u Şerîfine, Mevlânâ Sultan-u Evliya Şeyh Muhammed Nazım El Hakkani Hz.lerinin Ruh-u Şerîfine bahusus Mevlânâ Sultan-u Evliya Şeyh Muhammed Mehmet El Hakkani  Er Rabbani Hz.lerinin Ruhaniyetlerine hediyye eyledim. Ya Rabbi sen vâsıl eyle. Âmin.",
                        DhikrTarget = 1,
                    },
                    new DhikrView() {
                        DhikrGroupTitle = zikhirGroupTitle,
                        DhikrTitle = "Fatiha-i Şerife",
                        DhikrTranscript = "Bismillahirrahmânirrahîm. Elhamdü lillâhi rabbil'alemin. Errahmânir'rahim. Mâliki yevmiddin. İyyâke na'budü ve iyyâke neste'în. İhdinessırâtel müstakîm. Sırâtellezine en'amte aleyhim ğayrilmağdûbi aleyhim ve leddâllîn. Amîn",
                        DhikrTarget = 1,
                    }
                };
            hakkaniGroupView.Dhikrs.AddRange(zikhirs.Map((index, dhikr) => { dhikr.DhikrOrder = index + 1; return dhikr; }));
            return hakkaniGroupView;

        }

    }
}
