﻿using HKK.Engine.Calendar.Constants;
using HKK.Core.Extensions.Static;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using HKK.Core.Api.APIExtensions;
using HKK.Engine.Forecasts.Vectors;
using HKK.Engine.Calendar.Vectors;
using HKK.Engine.Calendar.Enums;
using HKK.Domain.Views.PrayerTimes;
using HKK.Resources.Values;
using System.IO;
using System.Drawing;
using System.Drawing.Imaging;
using HKK.Engine.Calendar;
using HKK.Engine.Forecasts.Enums;
using HKK.Core.Engines;
using HKK.Domain.Main.Views;
using HKK.Core.Domain.Contracts;
using HKK.Core.Domain.Objects;

namespace HKK.Engine.Forecasts {

    public static class SForecastEngine {

        public static async Task<(CalendarInfo location, IEnumerable<ForecastVector> forecasts)> GetDailyForecasts(string city, string countryCode, string lang = "tr") {
            CalendarInfo location = await SCalendarEngine.GetCalendarInfo(city, countryCode);
            var response = await "https://api.darksky.net/"
                .RequestRouteModifying($"forecast/{Keys.DarkSkyAPIKey}/{location.Latitude},{location.Longitude}?lang={lang}", json => {
                    var fcList = new List<ForecastVector>();
                    var date = DateTime.Now;
                    foreach (var forecast in json["daily"]["data"]) {
                        var icon = forecast["icon"].ToString().Replace("-", "_");
                        var file = Resources.Images.Forecasts.ResourceManager.GetObject(icon) as Bitmap;
                        using (var ms = new MemoryStream()) {
                            if (file != null) {
                                file.Save(ms, ImageFormat.Png);
                            }
                            fcList.Add(new ForecastVector {
                                CurrentDate = Convert.ToDouble(forecast["time"]).FromUnixTime(),
                                WindSpeed = Convert.ToDouble(forecast["windSpeed"]),
                                TemperatureMin = Convert.ToDouble(forecast["temperatureMin"]),
                                TemperatureMax = Convert.ToDouble(forecast["temperatureMax"]),
                                Summary = forecast["summary"].ToString(),
                                IconCode = icon,
                                IconData = ms.ToArray()
                            });
                        }
                        

                    }
                    return fcList.AsEnumerable();
                });
            return await Task.FromResult((location: location, forecasts: response.ResponseObject));

        }

        public static async Task<IHDSResponse> UpsertCountryWideWeeklyForecasts(string city, string countryCode) {

            var forecastEngine = SDataEngine.GenerateAGEngine<ForecastView>();
            var calendarEngine = SDataEngine.GenerateAGEngine<CalendarView>();

            var now = String.Format(Formats.ComparableDateFormat, DateTime.Now);
            var existingTodayForecastsResponse = await forecastEngine.SelectAll(fcView => fcView.ForecastDateString == now);
            if (existingTodayForecastsResponse.HasData == false) {
                ConstructCountryWideDailyForecasts().Each((i, daily) => {
                    daily.forecasts.Each(async (j, forecast) => {
                        IHDSResponse existingCalendarResponse = await calendarEngine.SelectByOne(geoView => geoView.CalendarKey == city);
                        var forecastViewResponse = new ForecastView {
                            ForecastIconData = forecast.IconData,
                            ForecastIconCode = forecast.IconCode,
                            ForecastSummary = forecast.Summary,
                            ForecastTemperatureMax = forecast.TemperatureMax,
                            ForecastTemperatureMin = forecast.TemperatureMin,
                            ForecastCurrentDate = forecast.CurrentDate,
                            ForecastWindSpeed = forecast.WindSpeed,
                            CalendarKey = city,
                            CalendarLatitude = daily.location.Latitude.ParseExactDecimal(),
                            CalendarLongitude = daily.location.Longitude.ParseExactDecimal(),
                            ForecastCalendarID = existingCalendarResponse.EntityID
                        }.InsertThat(out var forecastAgg);
                        if (forecastViewResponse.HasErrors) {
                            // TODO rollback action
                        }
                    });
                });
            } else {
                existingTodayForecastsResponse = await forecastEngine.SelectAll();
                var maxDate = existingTodayForecastsResponse.Items.Max(todayForecasts => todayForecasts.ForecastCurrentDate);
                existingTodayForecastsResponse = new HDSResponse(
                    existingTodayForecastsResponse.Items
                    .Filter((i, todayForecasts) => todayForecasts.ForecastCurrentDate < maxDate)
                    .LimitTo(31));
            }

            return existingTodayForecastsResponse;
        }

        public static IEnumerable<(CalendarInfo location, IEnumerable<ForecastVector> forecasts)> ConstructCountryWideDailyForecasts(ECountry country = ECountry.Turkiye) {
            var dailyForecasts = SLocationsEngine.Locals(ECountry.Turkiye).Map((i, city) => {
                Thread.Sleep(100);
                return AsyncExtensions.AwaitResult(() => GetDailyForecasts(city, "TR"));
            });
            return dailyForecasts;
        }

    }

}
