﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HKK.Engine.Forecasts.Vectors {
    public class ForecastVector {
        public DateTime CurrentDate { get; set; }
        public string Summary { get; set; }
        public double WindSpeed { get; set; }
        public double TemperatureMin { get; set; }
        public double TemperatureMax { get; set; }
        public string IconCode { get; set; }
        public byte[] IconData { get; set; }
    }
}
