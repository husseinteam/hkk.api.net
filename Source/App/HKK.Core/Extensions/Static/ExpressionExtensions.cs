﻿using System;
using System.Linq.Expressions;
using System.Reflection;

namespace HKK.Core.Extensions.Static {
    public static class ExpressionExtensions {

        public static MemberInfo ResolveMember<TDomain, TProp>(this Expression<Func<TDomain, TProp>> selector) {

            return selector.Body.ExposeMember();

        }

        public static MemberInfo ExposeMember(this Expression body) {

            MemberInfo prop = null;
            if (body is MemberExpression member) {
                prop = member.Member as PropertyInfo;
                if (prop == null) {
                    prop = member.Member as FieldInfo;
                }
            } else if (body is UnaryExpression unary) {
                if (unary.Operand is MemberExpression umember) {
                    prop = umember.Member as PropertyInfo;
                    if (prop == null) {
                        prop = umember.Member as FieldInfo;
                    }
                }
            } else if (body is InvocationExpression invocation) {
                if (invocation.Expression is MemberExpression umember) {
                    prop = umember.Member as PropertyInfo;
                    if (prop == null) {
                        prop = umember.Member as FieldInfo;
                    }
                }
            }
            return prop;

        }

        public static Type ExposeType(this Expression body) {

            Type t = null;
            if (body is MemberExpression member) {
                t = member.Member.DeclaringType;
            } else if (body is UnaryExpression unary) {
                t = (unary.Operand as MemberExpression)?.Member.DeclaringType;
            }
            return t == null ? t : (t.IsGenericType ? t.GenericTypeArguments[0] : t);

        }

        public static bool IsParameterTypeOf<TEntity>(this BinaryExpression self) {
            return self.Right.IsParameterTypeOf<TEntity>()
                || self.Left.IsParameterTypeOf<TEntity>();
        }

        public static bool IsParameterTypeOf<TEntity>(this Expression self) {
            return self.ExposeType()?.Equals(typeof(TEntity)) == true;
        }


        public static object Resultify(this Expression self) {

            try {
                return Expression.Lambda<Func<object>>(Expression.Convert(self, typeof(object))).Compile().Invoke();
            } catch (InvalidOperationException) {
                return null;
            }
        }

        public static void ExtractComposit(this Expression expression, Action<MemberInfo> caseMember,
            Action<object> caseConstant) {
            if (expression is MemberExpression) {
                caseMember((expression as MemberExpression).Member as PropertyInfo);
            } else if (expression is UnaryExpression) {
                caseMember((expression as UnaryExpression).ExposeMember());
            } else if (expression is BinaryExpression) {
                var binaryexp = expression as BinaryExpression;
                binaryexp.Right.ExtractComposit(caseMember, caseConstant);
            } else if (expression.NodeType == ExpressionType.Call) {
                caseConstant(expression.Resultify());
            } else {
                caseConstant((expression as ConstantExpression).Value);
            }
        }

        public static string NodeTypeString(this BinaryExpression self) {

            switch (self.NodeType) {
                case ExpressionType.Add:
                    return "+";
                case ExpressionType.AddChecked:
                    return "+";
                case ExpressionType.And:
                    return "&";
                case ExpressionType.AndAlso:
                    return "&&";
                case ExpressionType.ArrayLength:
                    throw new NotImplementedException();
                case ExpressionType.ArrayIndex:
                    throw new NotImplementedException();
                case ExpressionType.Call:
                    throw new NotImplementedException();
                case ExpressionType.Coalesce:
                    throw new NotImplementedException();
                case ExpressionType.Conditional:
                    throw new NotImplementedException();
                case ExpressionType.Constant:
                    throw new NotImplementedException();
                case ExpressionType.Convert:
                    throw new NotImplementedException();
                case ExpressionType.ConvertChecked:
                    throw new NotImplementedException();
                case ExpressionType.Divide:
                    return "/";
                case ExpressionType.Equal:
                    return "=";
                case ExpressionType.ExclusiveOr:
                    return "^";
                case ExpressionType.GreaterThan:
                    return ">";
                case ExpressionType.GreaterThanOrEqual:
                    return ">=";
                case ExpressionType.Invoke:
                    throw new NotImplementedException();
                case ExpressionType.Lambda:
                    throw new NotImplementedException();
                case ExpressionType.LeftShift:
                    return "<<";
                case ExpressionType.LessThan:
                    return "<";
                case ExpressionType.LessThanOrEqual:
                    return "<=";
                case ExpressionType.ListInit:
                    throw new NotImplementedException();
                case ExpressionType.MemberAccess:
                    throw new NotImplementedException();
                case ExpressionType.MemberInit:
                    throw new NotImplementedException();
                case ExpressionType.Modulo:
                    return "%";
                case ExpressionType.Multiply:
                    return "*";
                case ExpressionType.MultiplyChecked:
                    throw new NotImplementedException();
                case ExpressionType.Negate:
                    return "-";
                case ExpressionType.UnaryPlus:
                    return "+";
                case ExpressionType.NegateChecked:
                    throw new NotImplementedException();
                case ExpressionType.New:
                    throw new NotImplementedException();
                case ExpressionType.NewArrayInit:
                    throw new NotImplementedException();
                case ExpressionType.NewArrayBounds:
                    throw new NotImplementedException();
                case ExpressionType.Not:
                    return "~";
                case ExpressionType.NotEqual:
                    return "!=";
                case ExpressionType.Or:
                    return "|";
                case ExpressionType.OrElse:
                    return "||";
                case ExpressionType.Parameter:
                    throw new NotImplementedException();
                case ExpressionType.Power:
                    return "^";
                case ExpressionType.Quote:
                    throw new NotImplementedException();
                case ExpressionType.RightShift:
                    return ">>";
                case ExpressionType.Subtract:
                    return "-";
                case ExpressionType.SubtractChecked:
                    throw new NotImplementedException();
                case ExpressionType.TypeAs:
                    throw new NotImplementedException();
                case ExpressionType.TypeIs:
                    throw new NotImplementedException();
                case ExpressionType.Assign:
                    return "=";
                case ExpressionType.Block:
                    throw new NotImplementedException();
                case ExpressionType.DebugInfo:
                    throw new NotImplementedException();
                case ExpressionType.Decrement:
                    throw new NotImplementedException();
                case ExpressionType.Dynamic:
                    throw new NotImplementedException();
                case ExpressionType.Default:
                    throw new NotImplementedException();
                case ExpressionType.Extension:
                    throw new NotImplementedException();
                case ExpressionType.Goto:
                    throw new NotImplementedException();
                case ExpressionType.Increment:
                    throw new NotImplementedException();
                case ExpressionType.Index:
                    throw new NotImplementedException();
                case ExpressionType.Label:
                    throw new NotImplementedException();
                case ExpressionType.RuntimeVariables:
                    throw new NotImplementedException();
                case ExpressionType.Loop:
                    throw new NotImplementedException();
                case ExpressionType.Switch:
                    throw new NotImplementedException();
                case ExpressionType.Throw:
                    throw new NotImplementedException();
                case ExpressionType.Try:
                    throw new NotImplementedException();
                case ExpressionType.Unbox:
                    throw new NotImplementedException();
                case ExpressionType.AddAssign:
                    return "+=";
                case ExpressionType.AndAssign:
                    return "&=";
                case ExpressionType.DivideAssign:
                    return "/=";
                case ExpressionType.ExclusiveOrAssign:
                    return "^=";
                case ExpressionType.LeftShiftAssign:
                    throw new NotImplementedException();
                case ExpressionType.ModuloAssign:
                    throw new NotImplementedException();
                case ExpressionType.MultiplyAssign:
                    throw new NotImplementedException();
                case ExpressionType.OrAssign:
                    throw new NotImplementedException();
                case ExpressionType.PowerAssign:
                    throw new NotImplementedException();
                case ExpressionType.RightShiftAssign:
                    throw new NotImplementedException();
                case ExpressionType.SubtractAssign:
                    throw new NotImplementedException();
                case ExpressionType.AddAssignChecked:
                    throw new NotImplementedException();
                case ExpressionType.MultiplyAssignChecked:
                    throw new NotImplementedException();
                case ExpressionType.SubtractAssignChecked:
                    throw new NotImplementedException();
                case ExpressionType.PreIncrementAssign:
                    throw new NotImplementedException();
                case ExpressionType.PreDecrementAssign:
                    throw new NotImplementedException();
                case ExpressionType.PostIncrementAssign:
                    throw new NotImplementedException();
                case ExpressionType.PostDecrementAssign:
                    throw new NotImplementedException();
                case ExpressionType.TypeEqual:
                    throw new NotImplementedException();
                case ExpressionType.OnesComplement:
                    throw new NotImplementedException();
                case ExpressionType.IsTrue:
                    throw new NotImplementedException();
                case ExpressionType.IsFalse:
                    throw new NotImplementedException();
                default:
                    throw new NotImplementedException();
            }

        }

    }
}
