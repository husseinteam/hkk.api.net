﻿using HKK.Core.Domain.Objects;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Reflection;
using System.Web.Script.Serialization;

namespace HKK.Core.Extensions.Static {

    public static class ReflectionExtensions {

        public static object CastToList(this IEnumerable<dynamic> self, Type referencedType) {
            var iterator = typeof(Enumerable).ExecuteStaticMethod("Cast", new[] { referencedType }, self);
            return typeof(Enumerable).ExecuteStaticMethod("ToList", new[] { referencedType }, iterator);
        }

        public static object GetPropValue(this object obj, string name) {
            return obj.GetPropertyInfo(name).GetValue(obj);
        }

        public static IDictionary<string, object> ToDictionary(this object obj) {
            IDictionary<string, object> result = new Dictionary<string, object>();
            PropertyDescriptorCollection properties = TypeDescriptor.GetProperties(obj);
            foreach (PropertyDescriptor property in properties) {
                result.Add(property.Name, property.GetValue(obj));
            }
            return result;
        }

        public static IEnumerable<PropertyInfo> ResolveFields<TEntity>(this TEntity self) {

            return typeof(TEntity).GetProperties();

        }

        public static IEnumerable<MethodInfo> ResolveMethods(this object self) {

            return self.GetType().GetMethods(BindingFlags.Public | BindingFlags.Instance);

        }

        public static IEnumerable<PropertyInfo> ResolveProperties(this Type type) {

            return type.GetProperties(BindingFlags.Instance | BindingFlags.Public);

        }

        public static T ToObject<T>(this IDictionary<string, string> self) {

            Func<string, string> parameterizer = (val) => (val.Contains('{') && val.Contains('}')) || (val.Contains('[') && val.Contains(']')) ? val : $@"""{val}""";
            var objstr = self.Aggregate("", (acc, kv) => acc + $@"""{kv.Key}"": {parameterizer(kv.Value)}, ").TrimTrailing(", ");
            return new JavaScriptSerializer().Deserialize<T>($"{{ {objstr} }}");

        }

        public static T ToObject<T>(this string self) {

            return JsonConvert.DeserializeObject<T>(self);

        }

        public static object ToObject(this string self, Type type) {
            if (type.Equals(typeof(String))) {
                return self;
            } else {
                var obj = new JavaScriptSerializer().Deserialize(self, type);
                return Convert.ChangeType(obj, type);
            }
        }

        public static Type ParentType(this MemberInfo member) {
            Func<Type, Type> extractor = (reflected) => (reflected.IsGenericType ? reflected.GetGenericArguments()?.FirstOrDefault() ?? reflected : reflected);
            return extractor(member.ReflectedType);
        }

        public static bool PropertyEquals(this MemberInfo member, MemberInfo other) {
            return member.Name == other.Name &&
                member.GetMemberType().Equals(other.GetMemberType()) &&
                member.ParentType().Equals(other.ParentType());
        }

        public static void SetValue<TEntity>(this MemberInfo self, TEntity entity, object value) {
            if (self is PropertyInfo prop) {
                prop.SetValue(entity, value);
            } else if (self is FieldInfo field) {
                field.SetValue(entity, value);
            }
        }

        public static object GetValue<TEntity>(this MemberInfo self, TEntity entity) {
            if (self is PropertyInfo prop) {
                return prop.GetValue(entity);
            } else if (self is FieldInfo field) {
                return field.GetValue(entity);
            } else {
                return null;
            }
        }

        public static Type GetMemberType(this MemberInfo self) {
            if (self is PropertyInfo prop) {
                return prop.PropertyType;
            } else if (self is FieldInfo field) {
                return field.FieldType;
            } else {
                return null;
            }
        }

        public static bool IsAssigned<TItem>(this MemberInfo self, TItem item) {

            var value = self.GetValue(item);
            if (value != null) {
                if (self.GetMemberType().IsEnum) {
                    return true;
                }/* else if (self.PropertyType.IsNumericType()) {
                    return value.ToInt() != 0;
                } */else if (self.GetMemberType().Equals(typeof(DateTime))) {
                    DateTime dt = DateTime.MinValue;
                    DateTime.TryParse(value.ToString(), out dt);
                    return dt != DateTime.MinValue && dt != DateTime.MaxValue;
                } else {
                    return value.ToString() != "";
                }
            } else {
                return false;
            }

        }

        public static bool IsNumericType(this Type t) {
            switch (Type.GetTypeCode(t)) {
                case TypeCode.Byte:
                case TypeCode.SByte:
                case TypeCode.UInt16:
                case TypeCode.UInt32:
                case TypeCode.UInt64:
                case TypeCode.Int16:
                case TypeCode.Int32:
                case TypeCode.Int64:
                case TypeCode.Decimal:
                case TypeCode.Double:
                case TypeCode.Single:
                    return true;
                default:
                    return false;
            }
        }

        public static bool IsConvertible(this Type self) {

            return self.GetInterfaces().Any(inf => inf.Equals(typeof(IConvertible)));

        }

        public static bool IsCollection(this Type self) {

            return self.GetInterfaces().Any(inf => new[] { typeof(ICollection<>), typeof(IList<>) }.ToList()
                .Any(type => inf.IsGenericType && inf.Equals(type.MakeGenericType(inf.GetGenericArguments()))));

        }

        public static bool IsEnumerable(this Type self) {

            return self.GetInterfaces().Any(inf => new[] { typeof(ICollection<>), typeof(IList<>), typeof(IEnumerable<>) }.ToList()
                .Any(type => inf.IsGenericTypeDefinition && inf.Equals(type.MakeGenericType(inf.GetGenericArguments()))));

        }

        public static bool Implements<T>(this Type self) {
            if (typeof(T).IsInterface) {
                return self.GetInterfaces().Any(inf => inf.IsGenericTypeDefinition ?
                    inf.Equals(typeof(T).MakeGenericType(inf.GetGenericArguments())) : inf.Equals(typeof(T)));
            } else {
                return typeof(T).IsGenericTypeDefinition 
                    ? self.IsSubclassOf(typeof(T).MakeGenericType(self.GetGenericArguments())) 
                    : self.IsSubclassOf(typeof(T));
            }
        }
        public static bool HasGenericArgument(this Type self, Type type) {

            return self.GetInterfaces().Any(inf => inf.IsGenericTypeDefinition &&
                inf.GetGenericArguments().Any(ga => ga.Equals(type)));

        }
        public static TInterface Breath<TInterface>(this Type self)
            where TInterface : class {

            return Activator.CreateInstance(self) as TInterface;

        }

        public static T Materialize<T>(this Type self, params object[] args)
            where T : class {
            if (self.Equals(typeof(T))) {
                throw new ArgumentException($"Materialize<T> Error Same Types: <{self.Name}> <{typeof(T).Name}>");
            }
            return Activator.CreateInstance(self, args).As<T>();

        }

        public static bool OfType<T>(this Type self) {

            return self.Equals(typeof(T));

        }
        public static bool IsTypeOf<T>(this object self) {

            return self.GetType().OfType<T>();

        }

        public static IEnumerable<Type> GenericArguments(this object self) {
            if (self.GetType().IsGenericType) {
                return self.GetType().GetGenericArguments();
            } else {
                throw new ArgumentException($"{self.GetType().Name} is Not a Generic Type");
            }
        }

        public static bool HasAttribute<TAttribute>(this PropertyInfo self)
            where TAttribute : Attribute {
            return self.GetCustomAttribute<TAttribute>() != null;
        }

        public static Type GetTypeByName(this Assembly assembly, string name) {
            return assembly.GetTypes().SingleOrDefault(t => t.Name == name);
        }

        public static PropertyInfo GetPropertyInfo(this object self, string name) {
            return self.GetType().ResolveProperties().SingleOrDefault(prop => prop.Name == name);
        }

        public static T GetInstanceByName<T>(this Assembly assembly, string name)
            where T : class {
            var t = assembly.GetTypeByName(name);
            return Activator.CreateInstance(t) as T;
        }

        public static object ExecuteMethod(this object self, string methodName, Type[] genericTypes,
            params object[] arguments) {

            var type = self.GetType();
            var mi = type.GetMethod(methodName, BindingFlags.Instance | BindingFlags.Public | BindingFlags.FlattenHierarchy);
            genericTypes.ToList().ForEach(gt => mi = mi.MakeGenericMethod(gt));
            if (mi == null) {
                throw new NullReferenceException($"{methodName} cannot be found on type: {type.ToString()}");
            }
            try {
                return mi.Invoke(self, arguments?.ToArray());
            } catch (Exception ex) {
                var willThrown = new Exception($"{methodName} threw inner exception on type: {type.ToString()}", ex);
                throw willThrown;
            }
        }

        public static object ExecuteStaticMethod(this Type staticType, string methodName, Type[] genericTypes,
            params object[] arguments) {

            var mi = staticType.GetMethod(methodName, BindingFlags.Public | BindingFlags.Static);
            genericTypes.ToList().ForEach(gt => mi = mi.MakeGenericMethod(gt));
            return mi.Invoke(null, arguments.ToArray());

        }

        public static bool IsDebugBuild(this Assembly assembly) {
            var attribute = assembly.GetCustomAttribute<DebuggableAttribute>();
            return attribute == null ? false : attribute.IsJITTrackingEnabled;
        }

        public static Assembly GetReferencingAssembly(this StackTrace trace, int overflow = 13) {
            return new StackTrace().GetFrame(1).GetMethod().ReflectedType.Assembly;
        }

    }

}
