﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace HKK.Core.Extensions.Static {
    public static class EnumerableExtensions {

        public static IEnumerable<(string template, string value)> MatchOverlayPattern(this IEnumerable<string> self, IEnumerable<string> template, string pattern, string combinator) {
            var templateStr = template.JoinIntoString(combinator);
            var selfStr = self.JoinIntoString(combinator);
            foreach (var t in template) {
                foreach (var s in self) {
                    if (t.IsMatch(pattern, out var firstGroup)) {
                        var itemIndex = selfStr.IndexOf(combinator + firstGroup) + firstGroup.Length + 2 * combinator.Length;
                        var value = selfStr.SubstringFromTo(combinator.Last(), itemIndex);
                        yield return (template: firstGroup, value: value);
                        break;
                    }
                }
            }
        }

        public static IEnumerable<T> Filter<T>(this IEnumerable<T> self, Func<int, T, bool> selector) {
            var c = 0;
            foreach (var item in self) {
                if (selector(c++, item)) {
                    yield return item;
                }
            }
        }


        public static IEnumerable<T> LimitTo<T>(this IEnumerable<T> self, int limit) {
            var takeLimit = Math.Min(limit, self.Count());
            return self.Take(takeLimit);
        }

        public static IList<T> Append<T>(this IList<T> self, T item) {

            self.Add(item);
            return self;

        }

        public static void Each<T>(this IEnumerable<T> self, Action<int, T> cursor) {

            var c = 0;
            foreach (var item in self) {
                cursor(c++, item);
            }

        }

        public static void EachIf<T>(this IEnumerable<T> self, Func<int, T, bool> cursor) {

            var c = 0;
            foreach (var item in self) {
                if (cursor(c++, item) == false) {
                    break;
                }
            }

        }

        public static string JoinIntoString(this IEnumerable<string> self, string combinator) {
            var sb = new StringBuilder(combinator);
            self.Each((i, s) => sb.AppendFormat("{0}{1}", s, combinator));
            return sb.ToString();
        }

        public static IEnumerable<TOut> Map<TIn, TOut>(this IEnumerable<TIn> self, Func<int, TIn, TOut> cursor) {

            var list = new List<TOut>();
            var c = 0;
            foreach (var item in self) {
                list.Add(cursor(c++, item));
            }
            return list.ToArray();

        }

        public static IEnumerable Transform<TIn>(this IEnumerable self, Func<TIn, object> cursor) {

            var list = new List<object>();
            foreach (TIn item in self) {
                list.Add(cursor(item));
            }
            return list.ToArray();

        }

        public static bool SelectSingleOut<TOut>(this IEnumerable<TOut> self, Func<TOut, bool> selector, out TOut single) {
            return (single = self.SingleOrDefault(selector)) != null;
        }

        public static bool SelectMultipleOut<TOut>(this IEnumerable<TOut> self, Func<TOut, bool> selector, out IEnumerable<TOut> multiple) {
            return (multiple = self.Where(selector)).Count() > 0;
        }

        public static void Times(this int count, Func<int, bool> cursor) {

            for (int i = 0; i < count; i++) {
                if (cursor(i)) {
                    break;
                }
            }

        }

        public static IEnumerable<T> Range<T>(this int count, Func<int, T> cursor) {

            for (int i = 0; i < count; i++) {
                yield return cursor(i);
            }

        }

        public static void MeanWhile<T>(this IEnumerable<T> self, Func<T, bool> selector, Action<int, T> cursor) {

            self.Each((i, s) => {
                if (selector(s)) {
                    cursor(i, s);
                }
            });

        }

    }
}
