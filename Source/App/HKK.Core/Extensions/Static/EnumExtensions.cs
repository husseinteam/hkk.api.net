﻿using HKK.Core.Api.CoreModel.Decorators;
using System;
using System.ComponentModel;
using System.Globalization;
using System.Linq;
using System.Reflection;
using System.Resources;
using System.Threading;

namespace HKK.Core.Extensions.Static {
    public static class EnumExtensions {

        public static string GetDescription<T>(this T value) {
            if (typeof(T).IsEnum) {
                return typeof(T).GetFields().SingleOrDefault(fi => fi.GetValue(value).Equals(value))?.GetCustomAttribute<DescriptionAttribute>()?.Description;
            } else {
                throw new ArgumentException("T is not an enum");
            }
        }

        public static TEnum ToEnum<TEnum>(this string self) {

            var e = Enum.GetValues(typeof(TEnum)).OfType<TEnum>().SingleOrDefault(eval => eval.GetDescription() == self);
            if (e.Equals((TEnum)Enum.Parse(typeof(TEnum), "0"))) {
                var eobj = typeof(TEnum).GetFields().SingleOrDefault(fi => fi.Name == self)?
                    .GetValue(Activator.CreateInstance<TEnum>());
                if (eobj == null) {
                    var rm = new ResourceManager(typeof(TEnum).GetCustomAttribute<ResourceLocatorAttribute>()?.ResourceType);
                    foreach (var en in Enum.GetValues(typeof(TEnum)).OfType<TEnum>()) {
                        if (self == rm?.GetString(Enum.GetName(typeof(TEnum), en), CultureInfo.DefaultThreadCurrentCulture)) {
                            e = en;
                        }
                    }
                } else {
                    e = (TEnum)eobj;
                }
            }
            return e;
        }

    }
}
