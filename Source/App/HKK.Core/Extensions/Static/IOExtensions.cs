﻿using System.Diagnostics;
using System.IO;
using System.Text.RegularExpressions;

namespace HKK.Core.Extensions.Static {

    public static class IOExtensions {

        public static byte[] ReadAllBytes(this Stream self) {

            using (var br = new BinaryReader(self)) {
                return br.ReadBytes((int)self.Length);
            }

        }


        public static FileInfo CloseFile(this FileInfo self, string handlePath) {

            var tool = new Process();
            tool.StartInfo.FileName = handlePath;
            tool.StartInfo.Arguments = self.ToString() + " /accepteula";
            tool.StartInfo.UseShellExecute = false;
            tool.StartInfo.RedirectStandardOutput = true;
            tool.Start();
            tool.WaitForExit();
            string outputTool = tool.StandardOutput.ReadToEnd();

            string matchPattern = @"(?<=\s+pid:\s+)\b(\d+)\b(?=\s+)";
            foreach (Match match in Regex.Matches(outputTool, matchPattern)) {
                Process.GetProcessById(int.Parse(match.Value)).Kill();
            }
            return self;

        }
    }

}
