﻿using HKK.Core.Api.CoreModel.Decorators;
using System;
using System.Globalization;
using System.Reflection;
using System.Resources;
using System.Threading;

namespace HKK.Core.Extensions.Static {
    public static class DecoratorExtensions {

        public static string GetEnumResource<TEnum>(this TEnum value) {

            return value.GetEnumResource(typeof(TEnum));

        }

        public static string GetEnumResource(this object value, Type enumType) {

            if (enumType.IsEnum) {
                var rm = new ResourceManager(enumType.GetCustomAttribute<ResourceLocatorAttribute>()?.ResourceType);
                return rm?.GetString(Enum.GetName(enumType, value), CultureInfo.DefaultThreadCurrentCulture);
            } else {
                throw new Exception("Must be an enum");
            }

        }

        public static string GetStringResource(this Type resourceType, string key) {

            var rm = new ResourceManager(resourceType.GetCustomAttribute<ResourceLocatorAttribute>()?.ResourceType);
            return rm?.GetString(key, CultureInfo.DefaultThreadCurrentCulture);

        }

        public static bool HasAttribute<TAttribute>(this MemberInfo self, out TAttribute attr)
            where TAttribute : Attribute {
            attr = self.GetCustomAttribute<TAttribute>();
            return attr != null;
        }

        public static bool HasAttribute<TAttribute>(this MemberInfo self)
            where TAttribute : Attribute {
            return self.GetCustomAttribute<TAttribute>() != null;
        }

        public static bool Attributes<TAttribute>(this object self)
            where TAttribute : Attribute {
            return self.GetType().GetCustomAttribute<TAttribute>() != null;
        }

    }
}
