﻿using HKK.Resources.Images;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Resources;
using System.Security.Cryptography;
using System.Text;
using static Bogus.DataSets.Name;

namespace HKK.Core.Extensions.Static {

    public static class StringExtensions {

        public static IEnumerable<string> SlicePartials(this string self, char begin, char[] end) {
            var list = new List<string>();
            for (int i = 0; i < self.Length; i++) {
                if (self[i] == begin) {
                    for (int j = i + 1; j < self.Length; j++) {
                        if (end.Any(c => c == self[j])) {
                            var str = self.Substring(i + 1, j - i - 1);
                            if (string.IsNullOrEmpty(str) == false) {
                                list.Add(str);
                                break;
                            }
                        }
                    }
                }
            }
            return list.ToArray();
        }
        public static IEnumerable<(int index, string part)> SlicePartialsWithIndexes(this string self, char begin, char[] end) {
            var list = new List<(int index, string part)>();
            int listIndex = 0;
            for (int i = 0; i < self.Length; i++) {
                if (self[i] == begin) {
                    for (int j = i + 1; j < self.Length; j++) {
                        if (end.Any(c => c == self[j])) {
                            var str = self.Substring(i + 1, j - i - 1);
                            if (string.IsNullOrEmpty(str) == false) {
                                list.Add((index: listIndex++, part: str));
                                break;
                            }
                        }
                    }
                }
            }
            return list.ToArray();
        }

        public static string SubstringTo(this string str, char to, int startFrom = 0) {
            var i = str.IndexOf(to, startFrom);
            return i > -1 ? str.Substring(startFrom, i) : str;
        }

        public static string SubstringFromTo(this string str, char to, int startFrom) {
            var i = str.IndexOf(to, startFrom);
            return i > -1 ? str.Substring(startFrom, i - startFrom) : str;
        }

        public static string SubstringFrom(this string str, char from) {
            var i = str.IndexOf(from);
            return i > -1 ? str.Substring(i + 1) : str;
        }

        public static string Puts(this string self, params object[] args) {

            return String.Format(self, args);

        }

        public static string RandomWords(this int self) {
            var lorem = new Bogus.DataSets.Lorem(locale: "tr");
            return lorem.Words(self).Aggregate("", (acc, n) => $"{acc} {n}");
        }

        public static string RandomFullName(this int nameCount) {
            var name = new Bogus.DataSets.Name(locale: "tr");
            var gender = 2.RandomNumberBetween(1) == 1 ? Gender.Male : Gender.Female;
            if (nameCount < 2) {
                throw new ArgumentOutOfRangeException($"nameCount should have been greater than 2 but is <{nameCount}>");
            }
            var nameBuilder = new StringBuilder(name.FirstName(gender));
            for (int i = 0; i < nameCount - 2; i++) {
                nameBuilder.AppendFormat(" {0}", name.FirstName(gender));
            }
            nameBuilder.AppendFormat(" {0}", name.LastName(gender));
            return nameBuilder.ToString();
        }

        public static byte[] RandomImage(this bool maleOrFemale) {
            var r = new Random();
            var user = maleOrFemale ? "ma_" + r.Next(0, 5) : "fe_" + r.Next(0, 5);
            var file = Users.ResourceManager.GetObject(user) as Bitmap;
            using (var ms = new MemoryStream()) {
                if (file != null) {
                    file.Save(ms, ImageFormat.Jpeg);
                    return ms.ToArray();
                } else {
                    return new byte[0];
                }
            }
        }

        public static DateTime RandomDate(this int year, string culture = "tr-TR") {
            var month = 12.RandomNumberBetween();
            var day = month == 2 ? 28.RandomNumberBetween() : 30.RandomNumberBetween();
            return DateTime.Parse($"{day}.{month}.{year}", CultureInfo.GetCultureInfo(culture));
        }

        public static string GenerateToken(this int count) {

            var str = new StringBuilder();
            for (int i = 0; i < count; i++) {
                str.Append(Guid.NewGuid().ToString("N"));
            }
            return str.ToString();

        }

        public static int RandomNumberBetween(this int maximum, int minimum = 1) {

            var random = new Bogus.Randomizer();
            return random.Number(minimum, maximum);

        }
        public static string RandomDigits(this int count) {

            var random = new Bogus.Randomizer();
            var str = new StringBuilder(random.Number(1, 9).ToString());
            for (int i = 1; i < count; i++) {
                str.Append(random.Number(0, 9).ToString());
            }
            return str.ToString();

        }

        public static string GenerateMD5(this string self) {

            using (var md5 = MD5.Create()) {
                byte[] inputBytes = System.Text.Encoding.ASCII.GetBytes(self);
                byte[] hashBytes = md5.ComputeHash(inputBytes);

                // Convert the byte array to hexadecimal string
                StringBuilder sb = new StringBuilder();
                for (int i = 0; i < hashBytes.Length; i++) {
                    sb.Append(hashBytes[i].ToString("X2"));
                }
                return sb.ToString();
            }

        }

        public static string TrimTrailing(this string self, string suffix) {

            var that = self;
            foreach (var c in suffix.Reversed()) {
                that = that.TrimEnd(c);
            }
            return that;
        }

        public static string Reversed(this string self) {
            var ca = self.ToCharArray();
            Array.Reverse(ca);
            return new String(ca);
        }

        public static string CorrectTurkish(this string self) {
            return self.Replace("İ", "i").Replace("Ğ", "G")
                .Replace("Ö", "O").Replace("Ü", "U")
                .Replace("Ş", "S").Replace("Ç", "C")
                .Replace("ı", "i").Replace("ğ", "g")
                .Replace("ö", "o").Replace("ü", "u")
                .Replace("ş", "s").Replace("ç", "c");
        }

        public static string Decode(this string self, string codePageName = "windows-1254") {
            Encoding enTr = Encoding.GetEncoding("windows-1254");
            Encoding unicode = Encoding.Unicode;

            byte[] unicodeBytes = unicode.GetBytes(self);
            byte[] trBytes = Encoding.Convert(unicode, enTr, unicodeBytes);
            char[] trChars = new char[enTr.GetCharCount(trBytes, 0, trBytes.Length)];
            enTr.GetChars(trBytes, 0, trBytes.Length, trChars, 0);
            string trString = new string(trChars);

            return trString;
        }

        public static string DecodeCharacters(this string self) {
            return self
                .Replace("&#252;", "ü")
                .Replace("&#246;", "ö")
                .Replace("&#231;", "ç")
                .Replace("&#220;", "Ü")
                .Replace("&#199;", "Ç")
                .Replace("&#214;", "Ö")
                .Replace("&#351;", "ş")
                .Replace("&#350;", "Ş")
                .Replace("&#304;", "İ")
                .Replace("&#305;", "i")
                .Replace("&#287;", "ğ")
                .Replace("&#286;", "Ğ")
                .Replace("&amp;", "&");
        }

    }

}
