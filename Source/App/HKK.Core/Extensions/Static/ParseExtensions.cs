﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Globalization;
using System.Linq;

namespace HKK.Core.Extensions.Static {
    public static class ParseExtensions {

        public static TObject Set<TObject>(this TObject obj, params Action<TObject>[] commits) {

            if (obj != null) {
                foreach (var commit in commits) {
                    commit(obj);
                }
            }
            return obj;
        }

        public static decimal ParseExactDecimal(this string self) {
            decimal.TryParse(self, NumberStyles.AllowDecimalPoint, CultureInfo.InvariantCulture, out var dec);
            return dec;
        }

        public static string ParseExactDecimalString(this string self) {
            decimal.TryParse(self, NumberStyles.AllowDecimalPoint, CultureInfo.InvariantCulture, out var dec);
            return dec.ToString(CultureInfo.InvariantCulture);
        }

        public static int ToInt32(this object self) {

            if (int.TryParse(self.ToString(), out var outInt)) {
                return outInt;
            }
            throw new ArgumentException($"Not an Integer: {self.GetType().Name}<{self.ToString()}>");

        }
        public static long ToInt64(this object self) {

            if (long.TryParse(self.ToString(), out var outLong)) {
                return outLong;
            }
            throw new ArgumentException($"Not a Long: {self.GetType().Name}<{self.ToString()}>");

        }

        public static Guid ToGuid<TItem>(this TItem self) {

            var outGuid = Guid.Empty;
            Guid.TryParse(self.ToString(), out outGuid);
            return outGuid;

        }

        public static DateTime ToDateTime<TItem>(this TItem self) {

            var outdt = DateTime.MinValue;
            DateTime.TryParse(self.ToString(), out outdt);
            return outdt;

        }

        public static TOut As<TIn, TOut>(this TIn self, Func<TIn, TOut> transformer) {
            return transformer(self);
        }

        public static TOut As<TOut>(this object self) {
            var type = typeof(TOut);
            try {
                if (type.Equals(typeof(DBNull))) {
                    return default(TOut);
                } else if (self.GetType().isConvertible()) {
                    return (TOut)Convert.ChangeType(self, type);
                } else {
                    return (TOut)self;
                }
            } catch (Exception) {

            }
            return default(TOut);
        }

        public static object As(this object self, Type type) {
            if (type.Equals(typeof(DBNull))) {
                return default(DBNull);
            } else if (self.GetType().isConvertible()) {
                return Convert.ChangeType(self, type);
            } else if (self.ToInt32() != -1) {
                return (object)self.ToInt32();
            } else if (self.ToGuid() != Guid.Empty) {
                return (object)self.ToGuid();
            } else if (self.ToDateTime() != DateTime.MinValue) {
                return (object)self.ToDateTime();
            } else {
                return self;
            }
        }

        public static bool isConvertible(this Type type) {
            return type.GetInterfaces().Any(intf => intf.Equals(typeof(IConvertible)));
        }

        public static bool IsAssigned(this object value) {
            return !ReferenceEquals(null, value) && value.ToString() != "0" && !value.Equals(Guid.Empty) && !value.Equals(String.Empty) && !value.Equals(DBNull.Value);
        }

        public static string ToJson<TItem>(this TItem item) {

            return JsonConvert.SerializeObject(item);

        }

        public static TItem FromJson<TItem>(this string self) {

            return JsonConvert.DeserializeObject<TItem>(self);

        }

        public static T FromJToken<T>(this JToken jToken, string key) {

            dynamic ret = jToken[key];
            if (ret == null) return default(T);
            if (ret is JObject) return JsonConvert.DeserializeObject<T>(ret.ToString());
            return (T)ret;

        }

    }
}
