﻿using HKK.Core.Api.Controller;
using HKK.Core.Domain.Aggregate;
using HKK.Core.Domain.Contracts;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http.Routing;
using System.Web.Routing;

namespace HKK.Core.Extensions.Static {

    public static class HttpExtensions {

        public static IDictionary<string, object> SelectRouteValues(this RouteData routeData) {
            if (!routeData.Values.ContainsKey("MS_SubRoutes"))
                return new Dictionary<string, object>();

            return ((IHttpRouteData[])routeData.Values["MS_SubRoutes"]).SelectMany(x => x.Values).ToDictionary(x => x.Key, y => y.Value);
        }

        public static byte[] DownloadFrom(this string url) {
            return new System.Net.WebClient().DownloadData(url);
        }

        public static string DownloadText(this Uri address) {
            return new System.Net.WebClient().DownloadString(address);
        }

        public static JsonWithHeader<IHDSResponse> ParseJsonToClient(this IHDSResponse self, HttpRequestMessage request, 
            Action<HttpHeaders> headerCursor = null) {
            var aggregateType = self.TypeOfItem();
            var transformer = aggregateType.Materialize<IAGBase>().GetClientTransformer();
            self.ReplaceItems(item => transformer.AggregateToClient(item.As<IAGBase>()));
            self.ReplaceSingle(item => transformer.AggregateToClient(item.As<IAGBase>()));
            var resp = new JsonWithHeader<IHDSResponse>(headerCursor, self, new JsonSerializerSettings { ContractResolver = TableColumnContractResolver.Instance }, Encoding.UTF8, request);
            return resp;
        }

        public static JsonWithHeader<T> ParseGenericJson<T>(this T self, HttpRequestMessage request, Action<HttpHeaders> headerCursor = null) {
            var resp = new JsonWithHeader<T>(headerCursor, self, new JsonSerializerSettings { ContractResolver = TableColumnContractResolver.Instance }, Encoding.UTF8, request);
            return resp;
        }

    }

}
