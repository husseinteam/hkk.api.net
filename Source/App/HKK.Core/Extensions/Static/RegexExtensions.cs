﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace HKK.Core.Extensions.Static {
    public static class RegexExtensions {
        
        public static bool IsMatch(this string self, string pattern, out string firstGroup) {
            firstGroup = null;
            var r = new Regex(pattern);
            var m = r.IsMatch(self);
            if (m) {
                var g = r.Match(self).Groups[1];
                if (g.IsAssigned()) {
                    firstGroup = g.Value; 
                }
            }
            return m;
        }

        public static string ReplaceByPattern(this string self, string pattern, string replacement) {
            if (self.IsMatch(pattern, out var firstGroup)) {
                return self.Replace(firstGroup, replacement);
            } else {
                return self;
            }
        }

    }
}
