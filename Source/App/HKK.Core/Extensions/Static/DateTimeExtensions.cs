﻿using System;
using System.Threading;

namespace HKK.Core.Extensions.Static {

    public static class DateTimeExtensions {

        public static string GetTranslatedDateString(this DateTime self, string format) {
            return self.ToString(format, Thread.CurrentThread.CurrentUICulture);

        }

        public static DateTime FromUnixTime(this double timestamp) {
            DateTime dt = new DateTime(1970, 1, 1, 0, 0, 0, 0, DateTimeKind.Utc);
            dt = dt.AddSeconds(timestamp);
            return dt;
        }

    }

}
