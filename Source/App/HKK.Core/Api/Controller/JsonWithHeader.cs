﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Web.Http.Results;
using Newtonsoft.Json;
using System.Net.Http.Headers;

namespace HKK.Core.Api.Controller {
    public class JsonWithHeader<T> : JsonResult<T> {

        private readonly Action<HttpHeaders> Headerer;

        public JsonWithHeader(Action<HttpHeaders> headerer, T content, JsonSerializerSettings serializerSettings, Encoding encoding, HttpRequestMessage request) : 
            base(content, serializerSettings, encoding, request) {
            this.Headerer = headerer;
        }
        public override async Task<HttpResponseMessage> ExecuteAsync(CancellationToken cancellationToken) {
            var resp = await base.ExecuteAsync(cancellationToken);
            Headerer?.Invoke(resp.Headers);
            return resp;
        }
    }
}
