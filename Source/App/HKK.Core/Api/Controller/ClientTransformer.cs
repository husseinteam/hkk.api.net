﻿using HKK.Core.Domain.Aggregate;
using HKK.Core.Extensions.Static;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HKK.Core.Api.Controller {
    public delegate object TransformHandler(object client, ETransform transform);
    internal class ClientTransformer : IClientTransformer {

        private TransformHandler _Handler;

        public object AggregateToClient(IAGBase aggregate) {
            return _Handler(aggregate, ETransform.SetClient);
        }

        public IAGBase ClientToAggregate(object client) {
            return _Handler(client, ETransform.SetAggregate).As<IAGBase>();

        }

        public IClientTransformer SetHandler(TransformHandler handler) {
            _Handler = handler;
            return this;
        }
    }

    public interface IClientTransformer {
        dynamic AggregateToClient(IAGBase aggregate);
        IAGBase ClientToAggregate(object client);
    }

}
