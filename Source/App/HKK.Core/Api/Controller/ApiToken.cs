﻿namespace HKK.Core.Api.Controller {
    public class ApiToken {

        public string AccessToken { get; set; }
        public bool Success { get; set; }

    }
}
