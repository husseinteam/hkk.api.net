﻿using System;

namespace HKK.Core.Api.Controller {
    public class Session {

        public ApiToken Token { get; set; } = new ApiToken();
        public string UserName { get; set; }
        public string Password { get; set; }
        public string ClientMessage { get; set; }
        public bool UserAlreadyExists { get; set; }
        public String ServerException { get; set; }
    }
}
