﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using HKK.Core.Api.Decorators;
using HKK.Core.Domain.Aggregate;
using HKK.Core.Extensions.Static;

namespace HKK.Core.Api.Controller {
    public class ApiMetadata {

        public GenericController ApiController { get; internal set; }
        public String ComputedResource { get => ApiController.AggregateType.Breath<IAGBase>().ResourceName; }
        public IEnumerable<(MethodInfo method, RelayerAttribute attr)> RouteInfos { get; internal set; }

    }
}
