﻿using HKK.Core.Api.Controller;
using HKK.Core.Api.Decorators;
using HKK.Core.Api.CoreModel.Decorators;
using HKK.Core.Api.Static;
using HKK.Core.Domain.Aggregate;
using HKK.Core.Extensions.Static;
using HKK.Core.Api.APIExtensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Reflection;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Http.Controllers;
using System.Web.Http.Dispatcher;
using System.Web.Http.Filters;
using System.Web.Http.Routing;
using System.Web.Http.Hosting;
using HKK.Core.Domain.Contracts;

namespace HKK.Core.Api.Controller {

    public class GenericRouter : DelegatingHandler {

        private readonly HttpConfiguration config;
        private readonly ApiMetadata metadata;
        private readonly MethodInfo method;
        private readonly RelayerAttribute attr;

        public GenericRouter(HttpConfiguration config, ApiMetadata metadata, (MethodInfo method, RelayerAttribute attr) routeInfo) {
            this.config = config;
            this.metadata = metadata;
            this.InnerHandler = new HttpControllerDispatcher(config);
            (this.method, this.attr) = routeInfo;
        }

        protected override async Task<HttpResponseMessage> SendAsync(HttpRequestMessage request, CancellationToken cancellationToken) {
            return await ExecuteAction(request, cancellationToken);
        }

        private async Task<HttpResponseMessage> ExecuteAction(HttpRequestMessage request, CancellationToken cancellationToken) {
            if (method == null) {
                return await base.SendAsync(request, cancellationToken);
            }
            var ctrDesc = new HttpControllerDescriptor(config, metadata.ComputedResource, metadata.ApiController.GetType());
            var actDesc = new ReflectedHttpActionDescriptor(ctrDesc, method);
            var actionExecutedContext = new HttpActionExecutedContext {
                ActionContext = new HttpActionContext {
                    ControllerContext = new HttpControllerContext {
                        Request = request,
                        Controller = metadata.ApiController
                    },
                    ActionDescriptor = actDesc
                },
                Response = new HttpResponseMessage()
            };
            foreach (ActionFilterAttribute filter in method.GetCustomAttributes(typeof(ActionFilterAttribute), true)) {
                await filter.OnActionExecutedAsync(actionExecutedContext, cancellationToken);
                if (actionExecutedContext.Response.StatusCode != HttpStatusCode.OK) {
                    return actionExecutedContext.Response;
                }
            }
            object[] arguments = new object[0];
            if (attr.ArgumentTypes.Length > 0) {
                ;
                if (attr.Method == HttpMethod.Get) {
                    if (attr.ArgumentTypes.Length > 1) {
                        var uri = request.RequestUri.ToString();
                        var path = $"/{attr.Path.TrimStart('/').TrimEnd('/')}/";
                        uri = uri.Last() == '/' ? uri : uri + "/";
                        var tmpltArgs = path.SlicePartials('/', new char[] { '/' });
                        var realArgs = uri.SlicePartials('/', new char[] { '/' });
                        arguments = realArgs.MatchOverlayPattern(tmpltArgs, @"{(.+?)(:.+)?}", "/").Map((i, map) => map.value.ToObject(attr.ArgumentTypes.ElementAt(i))).ToArray();
                    } else if (attr.ArgumentTypes[0].Equals(typeof(Dictionary<string, object>))) {
                        var dict = new Dictionary<string, object>();
                        var coll = HttpUtility.ParseQueryString(request.RequestUri.Query);
                        foreach (string key in coll.Keys) {
                            dict.Add(key, coll[key]);
                        }
                        arguments = new object[] { dict };
                    } else {
                        var routeData = request.GetRequestContext().RouteData;
                        arguments = attr.Path.SlicePartials(begin: '{', end: new char[] { '}', ':' })
                            .Select(arg => Convert.ChangeType(routeData.Values[arg], attr.ArgumentTypes[0])).ToArray();
                    }
                } else {
                    if (attr.ArgumentTypes.Length > 1) {
                        var uri = request.RequestUri.ToString();
                        arguments = uri.SlicePartials('/', new char[] { '}', ':' })
                            .Map((i, part) => part.ToObject(attr.ArgumentTypes.ElementAt(i))).ToArray();
                    } else if (attr.ArgumentTypes[0].Equals(typeof(long))) {
                        var uri = request.RequestUri.ToString();
                        arguments = new object[] { long.Parse(uri.Substring(uri.LastIndexOf('/') + 1)) };
                    } else {
                        var argType = attr.ArgumentTypes[0].Equals(typeof(IAGBase)) ? metadata.ApiController.AggregateType : attr.ArgumentTypes[0];
                        arguments = new object[] { (await request.Content.ReadAsStringAsync()).ToObject(argType) };
                    }
                }
            }
            var task = await metadata.ApiController.ExecuteMethod(method.Name, Type.EmptyTypes, arguments)
                .As<Task<IHttpActionResult>>();
            return await task.ExecuteAsync(cancellationToken);
        }

    }

}
