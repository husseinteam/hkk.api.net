﻿
using HKK.Core.Domain.Aggregate;
using HKK.Core.Api.CoreModel.Decorators;
using HKK.Core.Extensions.Static;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace HKK.Core.Api.Controller {
    internal class TableColumnContractResolver : DefaultContractResolver {
        public static readonly TableColumnContractResolver Instance = new TableColumnContractResolver();

        protected override JsonProperty CreateProperty(MemberInfo member, MemberSerialization memberSerialization) {
            JsonProperty property = base.CreateProperty(member, memberSerialization);
            var t = property.DeclaringType;
            if (!t.IsGenericType && t.Implements<IAGBase>()) {
                property.ShouldSerialize =
                    instance => {
                        var prop = property.DeclaringType.GetProperty(property.PropertyName);
                        return prop.HasAttribute<HavingCollectionAttribute>()  || 
                            (prop.HasAttribute<TableColumnAttribute>() && prop.GetCustomAttribute<TableColumnAttribute>().Disabled == false);
                    }; 
            }
            return property;
        }
    }
}
