﻿using HKK.Core.Api.ActionFilters;
using HKK.Core.Api.Contracts;
using HKK.Core.Api.Decorators;
using HKK.Core.Api.Controller;
using HKK.Core.Api.CoreModel.Decorators;
using HKK.Core.Api.Static;
using HKK.Core.Domain.Aggregate;
using HKK.Core.Domain.Contracts;
using HKK.Core.Domain.DML;
using HKK.Core.Domain.Objects;
using HKK.Core.Engines;
using HKK.Core.Engines.Core;
using HKK.Core.Extensions.Static;
using HKK.Core.Internalization.Strings;
using Newtonsoft.Json;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Dynamic;
using System.Globalization;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Reflection;
using System.Resources;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Filters;
using System.Web.Http.Results;

namespace HKK.Core.Api.Controller {

    public class GenericController : ApiController, IApiController {

        #region Ctor

        internal GenericController() {

            DomainType = AggregateType.Breath<IAGBase>()?.MainDomain;
            Request = new HttpRequestMessage();

        }

        public GenericController(Type aggregateType) {

            _AggregateType = aggregateType;
            DomainType = _AggregateType.Breath<IAGBase>()?.MainDomain;
            Request = new HttpRequestMessage();

        }

        #endregion

        #region Abstract

        private readonly Type _AggregateType;
        public virtual Type AggregateType => _AggregateType;

        #endregion

        #region Properties

        private Type DomainType { get; set; }

        public string ListRoute => "list";

        public static Dictionary<EApiRoutes, string> Routes(Type domainType) {
            return new Dictionary<EApiRoutes, string>() {
                { EApiRoutes.All, "all" },
                { EApiRoutes.Columns, "columns" },
                { EApiRoutes.Fields, "fields/{argument:long}" },
                { EApiRoutes.List, "list" },
                { EApiRoutes.One, "one/{argument:long}" },
                { EApiRoutes.Translations, "translations" },
                { EApiRoutes.Insert, "insert" },
                { EApiRoutes.Edit, "edit" },
                { EApiRoutes.Delete, "delete" },
            }.Select(pair => new KeyValuePair<EApiRoutes, string>(pair.Key,
                $"{SRPIValues.CurrentRemoteAddress}/{domainType.Name.ToLower()}/{pair.Value}")).ToDictionary(p => p.Key, p => p.Value);
        }

        #endregion

        #region Get Methods
        [Tahditli, Vazife("Aza"), Kaide("lists")]
        [RouteGet(path: "all", ArgumentTypes = new[] { typeof(Dictionary<string, object>) })]
        public async Task<IHttpActionResult> All([FromUri] Dictionary<string, object> query) {
            var eng = GenerateAGEngine();
            var listResponse = await eng.GenericSelectAll();
            var count = listResponse.Items.Count();
            var ranges = "";
            if (query != null) {
                //sort
                if (query.ContainsKey("sort")) {
                    var sort = query["sort"].ToString().ToObject<string[]>();
                    for (int i = 0; i < sort.Length; i += 2) {
                        var propName = sort[i];
                        var order = sort[i + 1];
                        if (order == "DESC") {
                            listResponse.Items.OrderByDescending(item => item.GetType().GetProperty(propName).GetValue(item));
                        } else {
                            listResponse.Items.OrderBy(item => item.GetType().GetProperty(propName).GetValue(item));
                        }
                    }
                }
                //range
                if (query.ContainsKey("range")) {
                    var range = query["range"].ToString().ToObject<int[]>();
                    if (range[1] == -1) {
                        ranges = $"{range[0]}-{count}/{count}";
                    } else {
                        listResponse = new HDSResponse(listResponse.Items.Skip(range[0]).Take(range[1] - range[0]));
                    }
                    ranges = $"{range[0]}-{range[1]}/{count}";
                }
                //filter
                if (query.ContainsKey("filter")) {
                    var filter = query["filter"].ToString().ToObject<Dictionary<string, object>>();
                    foreach (var entry in filter) {
                        listResponse = new HDSResponse(listResponse.Items.Where(
                            item => item.GetType().GetProperty(entry.Key).GetValue(item).ToString().Contains(entry.Value.ToString())));
                    }
                }
            }

            return listResponse.ParseJsonToClient(Request, (h) => h.Add("X-Content-Range", ranges));
        }

        [RouteGet(path: "grid-fields")]
        [Tahditli, Vazife("Aza"), Kaide("lists")]
        public async Task<IHttpActionResult> GridFields() {
            return await Task.FromResult(PickFields().ParseGenericJson(Request));
        }

        [RouteGet(path: "one/{argument:int:min(1)}", ArgumentTypes = new[] { typeof(long) })]
        public async Task<IHttpActionResult> One(long id) {
            var eng = GenerateAGEngine();
            return (await eng.GenericSelectByID(id)).ParseJsonToClient(Request);
        }

        [RouteGet(path: "list")]
        public async Task<IHttpActionResult> List() {
            var eng = GenerateDOEngine();
            var response = await eng.GenericSelectAll();
            if (response.HasData) {
                var list = response.Items.Select(q =>
                    new ListCallElement { id = q.ID, text = q.TextValue }).ToList();
                list.Insert(0, new ListCallElement { id = 0L, text = Preformats.FieldPlaceholder.Puts(_AggregateType.GetStringResource("TitleEntity")) });
                return new HDSResponse(list).ParseGenericJson(Request);
            }
            return response.ParseJsonToClient(Request);
        }

        [RouteGet(path: "translations")]
        public async Task<IHttpActionResult> Translations() {
            var rm = new ResourceManager(_AggregateType.GetCustomAttribute<ResourceLocatorAttribute>()?.ResourceType);
            var set = rm.GetResourceSet(CultureInfo.DefaultThreadCurrentCulture, true, true);
            var dict = new Dictionary<string, string>();
            foreach (DictionaryEntry it in set) {
                string key = it.Key.ToString();
                string resource = it.Value.ToString();
                dict[key] = resource;
            }
            return await Task.FromResult(dict.ParseGenericJson(Request));
        }

        [RouteGet(path: "columns")]
        public async Task<IHttpActionResult> Columns() {
            var list = PickColumns(pi => pi.HasAttribute<TableColumnAttribute>() &&
                pi.GetCustomAttribute<TableColumnAttribute>().Disabled == false).ToList();
            return await Task.FromResult(list.ParseGenericJson(Request));
        }

        [RouteGet(path: "fields/{argument:int:min(1)}", ArgumentTypes = new[] { typeof(long) })]
        public async Task<IHttpActionResult> Fields(long formType) {
            var type = (EFormType)formType;
            var list = PickColumns((pi) => (pi.GetCustomAttribute<FormFieldAttribute>()?.FormType & type) == type
                || pi.HasAttribute<HavingCollectionAttribute>()).ToList();
            return await Task.FromResult(list.ParseGenericJson(Request));
        }

        #endregion

        #region Post Methods
        [RoutePost(path: "insert-domain")]
        public async Task<IHttpActionResult> Insert([FromBody]IDOBase domain) {

            return await Task.FromResult(domain.InsertSelf().ParseGenericJson(Request));

        }

        [RoutePost(path: "insert")]
        public async Task<IHttpActionResult> InsertAggregate([FromBody]object view) {  
        
            var transformer = AggregateType.Materialize<IAGBase>().GetClientTransformer();
            var aggregate = transformer.ClientToAggregate(view);
            return await Task.FromResult(aggregate.InsertSelf().ParseJsonToClient(Request));
        }

        [RoutePut(path: "edit")]
        public async virtual Task<IHttpActionResult> Edit([FromBody]object view) {

            var transformer = AggregateType.Materialize<IAGBase>().GetClientTransformer();
            var aggregate = transformer.ClientToAggregate(view);
            return (await GenerateDOEngine().GenericUpdateAggregate(aggregate)).ParseJsonToClient(Request);

        }

        [RouteDelete(path: "delete/{argument}", ArgumentTypes = new[] { typeof(long) })]
        public async virtual Task<IHttpActionResult> Delete(long id) {
            var eng = GenerateAGEngine();
            var aggregateResp = await eng.GenericSelectByID(id);
            if (aggregateResp.HasData == false) {
                return aggregateResp.ParseJsonToClient(Request);
            }
            var aggregate = aggregateResp.Single as IAGBase;
            return aggregate.DeleteSelf().ParseJsonToClient(Request);
        }

        #endregion

        #region Special Methods

        private IDictionary<string, object> PickFields() {
            var dict = new Dictionary<string, object>();
            var resourceType = _AggregateType.GetCustomAttribute<ResourceLocatorAttribute>()?.ResourceType;
            var rm = resourceType != null ? new ResourceManager(resourceType) : null;
            foreach (var col in _AggregateType.GetProperties()
               .Where(p => p.HasAttribute<TableColumnAttribute>())
               .OrderBy(prop => prop.GetCustomAttribute<TableColumnAttribute>().Order)) {
                var type = GetEditor(col, out var editor, out var inputType);
                dict[col.Name] =
                    new {
                        name = col.Name,
                        inputType = inputType,
                        title = $"{_AggregateType.GetStringResource(col.Name)}",
                        type = type,
                        editor = editor,
                        maxLength = (Activator.CreateInstance(_AggregateType) as IAGBase).GetMaxLength(col),
                        required = (Activator.CreateInstance(_AggregateType) as IAGBase).IsColumnRequired(col),
                        placeholder = Preformats.FieldPlaceholder.Puts(_AggregateType.GetStringResource(col.Name))
                    };
            }
            return dict;
        }

        private string GetEditor(PropertyInfo col, out IDictionary<string, object> dict, out string inputType) {
            var type = "string";
            inputType = "text";
            dynamic editor = new {
                type = type
            };
            if (col.PropertyType.OfType<DateTime>()) {
                type = "datetime";
                editor = new {
                    type = "datepicker",
                    component = "",
                };
            } else if (col.PropertyType.OfType<bool>()) {
                editor = new {
                    type = "checkbox",
                    config = new {
                        @true = "Var",
                        @false = "Yok"
                    }
                };
                inputType = "radio";
            } else if (col.PropertyType.IsNumericType()) {
                editor = new {
                    type = "number"
                };
                type = "number";
                inputType = "number";
            } else if (col.HasAttribute<HavingCollectionAttribute>() && col.PropertyType.IsCollection()) {
                var aggragateType = col.PropertyType.GetGenericArguments().First();
                var domain = Activator.CreateInstance(aggragateType).As<IAGBase>().MainDomain;
                var eng = Activator.CreateInstance(typeof(AGEngine<>).MakeGenericType(aggragateType)) as IGenericEngine;
                var list = eng.GenericSelectAllSync();
                editor = new {
                    type = "list",
                    config = new {
                        list = list.Items.Select(item => new { value = item.IDProperty, title = item.TextValue })
                    }
                };
            } else if (col.HasAttribute<ComputedFieldAttribute>()) {
                var attr = col.GetCustomAttribute<ComputedFieldAttribute>();
                if (attr.ComputedType == EComputedType.Enum) {
                    var list = Enum.GetValues(attr.ReferencedType).OfType<object>().Select(val =>
                        new { value = Enum.GetName(attr.ReferencedType, val), title = val.GetEnumResource(attr.ReferencedType) });
                    editor = new {
                        type = "list",
                        config = new {
                            list = list
                        }
                    };
                } else if (attr.ComputedType == EComputedType.FormattedString) {
                    if (attr.ReferencedType.Equals(typeof(DateTime))) {
                        type = "datetime";
                        editor = new {
                            type = "datepicker",
                            component = "",
                        };
                    }
                }
            }
            dynamic exp = new ExpandoObject();
            dict = exp;
            if (col.HasAttribute<TableColumnAttribute>(out var tcol)) {
                if (tcol.Order == 0) {
                    dict.Add("editable", false);
                }
                if (tcol.Disabled == true) {
                    inputType = "password";
                }
            }
            foreach (PropertyInfo prop in editor.GetType().GetProperties()) {
                dict[prop.Name] = prop.GetValue(editor);
            }
            return type;
        }

        private IEnumerable<IDictionary<string, object>> PickColumns(Func<PropertyInfo, bool> filter = null) {

            var resourceType = _AggregateType.GetCustomAttribute<ResourceLocatorAttribute>()?.ResourceType;
            var rm = resourceType != null ? new ResourceManager(resourceType) : null;
            foreach (var col in _AggregateType.GetProperties()
                .Where(p => p.HasAttribute<TableColumnAttribute>())
                .Where(p => filter?.Invoke(p) ?? true)
                .OrderBy(prop => prop.GetCustomAttribute<TableColumnAttribute>().Order)) {
                var dataType = GetDataTypeOfEditable(col, out var inputType, out var selectables, out var link);
                var dict = new Dictionary<string, object> {
                    { "name", col.Name },
                    { "defaultValue", null },
                    { "isRequired", (Activator.CreateInstance(_AggregateType) as IAGBase).IsColumnRequired(col) },
                    {
                        "formdata",
                        new {
                            placeholder = Preformats.FieldPlaceholder.Puts(_AggregateType.GetStringResource(col.Name)),
                            title = $"{_AggregateType.GetStringResource(col.Name)}:",
                            isNavigationProperty = false,
                            dataType = dataType,
                            inputType = inputType,
                            link = dataType == "collection" ? link : null,
                            selectables = dataType == "selectable" ? selectables : null
                        }
                    }
                };
                if (filter == null) {
                    dict.Add("label", rm?.GetString(col.Name, CultureInfo.DefaultThreadCurrentCulture) ?? col.Name);
                } else if (filter(col)) {
                    dict.Add("label", rm?.GetString(col.Name, CultureInfo.DefaultThreadCurrentCulture) ?? col.Name);
                } else {
                    continue;
                }
                yield return dict;
            }
        }

        private static string GetDataTypeOfEditable(PropertyInfo col, out string inputType, out List<object> selectables, out string link) {
            selectables = new List<object>();
            link = "";
            var dataType = "text";
            inputType = "";
            if (col.PropertyType.OfType<DateTime>()) {
                dataType = "date";
            } else if (col.PropertyType.OfType<bool>()) {
                dataType = "boolean";
            } else if (col.PropertyType.IsNumericType()) {
                dataType = "number";
            } else if (col.HasAttribute<HavingCollectionAttribute>() && col.PropertyType.IsCollection()) {
                dataType = "collection";
                link = Routes(Activator.CreateInstance(col.PropertyType.GetGenericArguments().First()).As<IAGBase>().MainDomain)[EApiRoutes.List];
            } else if (col.HasAttribute<ComputedFieldAttribute>()) {
                dataType = "computed";
                var attr = col.GetCustomAttribute<ComputedFieldAttribute>();
                if (attr.ComputedType == EComputedType.Enum) {
                    dataType = "selectable";
                    foreach (var val in Enum.GetValues(attr.ReferencedType)) {
                        selectables.Add(new { value = Enum.GetName(attr.ReferencedType, val), text = val.GetEnumResource(attr.ReferencedType) });
                    }
                } else if (attr.ComputedType == EComputedType.FormattedString) {
                    if (attr.ReferencedType.Equals(typeof(DateTime))) {
                        dataType = "datetime";
                    }
                }
            }

            if (dataType == "number" || dataType == "text") {
                var colInputType = col.GetCustomAttribute<TableColumnAttribute>().InputType;
                inputType = string.IsNullOrEmpty(colInputType) ? dataType : colInputType;
                dataType = "input";
            }
            return dataType;
        }

        protected IGenericEngine GenerateDOEngine() {
            return Activator.CreateInstance(typeof(DOEngine<>).MakeGenericType(DomainType)) as IGenericEngine;
        }

        protected IGenericEngine GenerateAGEngine() {
            return Activator.CreateInstance(typeof(AGEngine<>).MakeGenericType(_AggregateType)) as IGenericEngine;
        }

        #endregion

    }

}
