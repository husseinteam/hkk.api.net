﻿using HKK.Core.Api.Controller;
using HKK.Core.Api.CoreModel.Views.Membership;
using HKK.Core.Api.Decorators;
using HKK.Core.Domain.Aggregate;
using HKK.Core.Domain.Data;
using HKK.Core.Domain.Objects;
using HKK.Core.Extensions.Static;
using HKK.Resources.Values;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Xml.Linq;

namespace HKK.Core.Api.Static {
    public static class SRPIValues {

        public static string CurrentRPIKey { get; set; } = "api";
        public static string CurrentRPIVersion { get; set; } = "v1";

        private static string currentDebugHost;
        private static string currentProductionHost;

        public static string CurrentDebugHost {
            get => String.IsNullOrEmpty(currentDebugHost) ? throw new Exception("Current Debug Host Not Set") : currentDebugHost;
            set => currentDebugHost = value;
        }
        public static string CurrentProductionHost {
            get => String.IsNullOrEmpty(currentProductionHost) ? throw new Exception("Current Production Host Not Set") : currentProductionHost;
            set => currentProductionHost = value;
        }

        public static string CurrentRemoteHost {
            get {
                return $"{(SDbParams.IsLocalMachine() ? CurrentDebugHost : CurrentProductionHost) }";
            }
        }

        public static IEnumerable<Type> ReferencedDomains(Type aggregateType) {
            var domains = aggregateType.Assembly.GetTypes().Where(t => !t.IsGenericType && t.Implements<IDOBase>()).ToList();
            var aggregates = aggregateType.Assembly.GetTypes().Where(t => !t.IsGenericType && t.Implements<IAGBase>()).ToList();
            return domains.Where(d => !Activator.CreateInstance(aggregateType).As<IAGBase>().MainDomain.Equals(d)).ToArray();
        }

        public static IEnumerable<ApiMetadata> BaseMetadata() => GenerateMetadata<UserView>();

        public static IEnumerable<ApiMetadata> GenerateMetadata<TMarker>() {

            var controllerTypes = typeof(TMarker).Assembly.GetTypes()
                .Where(t => t.Implements<GenericController>());
            var aggregateTypes = typeof(TMarker).Assembly.GetTypes().Where(t => !t.IsGenericType && t.Implements<IAGBase>()).ToList();
            foreach (var aggregateType in aggregateTypes) {
                var apiInstance = controllerTypes
                    .SingleOrDefault(controller => controller.Materialize<GenericController>().AggregateType.Equals(aggregateType))?
                    .Materialize<GenericController>();
                apiInstance = apiInstance ?? new GenericController(aggregateType);
                var actions = apiInstance.ResolveMethods()
                   .Where(m => m.GetCustomAttributes().Any(ca => ca.GetType().Implements<RelayerAttribute>()));
                var infos = actions.Select(action =>
                    (method: action, attr: action.GetCustomAttribute<RelayerAttribute>()));
                yield return new ApiMetadata {
                    ApiController = apiInstance,
                    RouteInfos = infos
                };

            }

        }

        public static string CurrentPath {
            get {
                return $"{CurrentRPIKey}/{CurrentRPIVersion}";
            }
        }

        internal static string CurrentRemoteAddress {
            get {
                return $"{CurrentRemoteHost}/{CurrentPath}";
            }
        }

        public static string CurrentSchema => SDbParams.IsLocalMachine() ? HKKValues.LocalhostSchemaName : HKKValues.RemoteSchemaName;
    }
}