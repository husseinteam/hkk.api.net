﻿using HKK.Core.Api.CoreModel.Decorators;
using HKK.Core.Api.CoreModel.Models.Membership;
using HKK.Core.Api.CoreModel.Seeders;
using HKK.Core.Api.CoreModel.Views.Membership;
using HKK.Core.Api.Controller;
using HKK.Core.Domain.Aggregate;
using HKK.Core.Domain.Objects;
using HKK.Core.Engines;
using HKK.Core.Extensions.Static;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using System.Web.Http;
using System.Web.Http.Routing;

namespace HKK.Core.Api.Static {
    public static class SRouter {

        public static void Configure<TApplication, TMarkerAggregate>(HttpConfiguration config)
            where TApplication : HttpApplication
            where TMarkerAggregate : AGBase<TMarkerAggregate> {
            config.MapHttpAttributeRoutes();

            RegisterMetadata(config, SRPIValues.BaseMetadata());
            RegisterMetadata(config, SRPIValues.GenerateMetadata<TMarkerAggregate>());
            RegisterMetadata(config, SRPIValues.GenerateMetadata<TApplication>());

            config.Filters.Add(new TahditliAttribute());
            config.Filters.Add(new VazifeAttribute());
            config.Filters.Add(new KaideAttribute());
            var session = SAzaSeeder.GetAdminSession();
            SubscribeSeeder<UserView>(
                (agg) => agg.UserName == session.UserName && agg.UserPassword == session.Password,
                () => new[] { AsyncExtensions.AwaitResult(() => SAzaSeeder.NewUserWith(session)) }
            );
        }

        private static void RegisterMetadata(HttpConfiguration config, IEnumerable<ApiMetadata> mdatas) {
            foreach (var metadata in mdatas) {
                foreach (var (method, attr) in metadata.RouteInfos) {
                    var path = attr.Path;
                    path = path.First() == '/' ? path.TrimStart('/') : path;
                    var action = path;
                    var arguments = path.SlicePartials('{', new[] { ':', '}' });
                    if (arguments.Count() > 0) {
                        path.SlicePartials(':', new[] { '}' }).Each((i, ext) => path = path.Replace($":{ext}", ""));
                        action = path.SubstringTo('/');
                    }
                    register($"{action}/");
                    if (action == "list") {
                        register("");
                    }
                    void register(string _action) {
                        var template = $"{SRPIValues.CurrentPath}/{metadata.ComputedResource}/{_action}";
                        IDictionary<string, object> defaults = new Dictionary<string, object>();
                        arguments.Each((i, arg) => defaults.Add(arg.TrimEnd('?'), RouteParameter.Optional));
                        config.Routes.MapHttpRoute(
                            name: $"ApiRoute-{Guid.NewGuid().ToString("N")}",
                            routeTemplate: template,
                            defaults: defaults,
                            handler: new GenericRouter(config, metadata, (method, attr)),
                            constraints: new {
                                httpMethod = new HttpMethodConstraint(attr.Method)
                            }
                        );
                    }
                }
            }
        }

        public static IEnumerable<IEnumerable<IAGBase>> Seed(Action builder) {
            builder();
            return Seeders.Map((i, seeder) => seeder.Invoke());
        }

        internal static List<Func<IEnumerable<IAGBase>>> Seeders { get; private set; } = new List<Func<IEnumerable<IAGBase>>>();
        public static void SubscribeSeeder<TAggregate>(Expression<Func<TAggregate, bool>> existenceSelector, Func<IEnumerable<IAGBase>> seeder)
            where TAggregate : AGBase<TAggregate> {
            Seeders.Add(() => {
                var existingList = SDataEngine.GenerateAGEngine<TAggregate>().SelectBySync(existenceSelector);
                if (existingList.HasData == false) {
                    return seeder.Invoke().InsertUs().Items.CastToList(typeof(TAggregate)).As<IEnumerable<TAggregate>>();
                } else {
                    return existingList.Items.CastToList(typeof(TAggregate)).As<IEnumerable<TAggregate>>();
                }
            });
        }
        public static void SubscribeSeeder<TAggregate>(Func<IEnumerable<IAGBase>> seeder)
            where TAggregate : AGBase<TAggregate> {
            var seeds = seeder.Invoke();
            Seeders.Add(() => {
                if (SDataEngine.GenerateAGEngine<TAggregate>().SelectCount() != seeds.Count()) {
                    return seeds.Map((i, newSeed) => {
                        newSeed.InsertThat(out var inserted);
                        return inserted;
                    });
                } else {
                    return SDataEngine.GenerateAGEngine<TAggregate>()
                        .SelectAllSync().Items.CastToList(typeof(TAggregate)).As<IEnumerable<TAggregate>>();
                }
            });
        }

    }
}
