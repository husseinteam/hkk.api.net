﻿
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Security.Cryptography;
using System.Threading.Tasks;
using System.Web.Http.Controllers;
using System.Web.Http.Filters;

namespace HKK.Core.Api.ActionFilters {
    public class ETagAttribute : ActionFilterAttribute {

        public override void OnActionExecuting(HttpActionContext actionContext) {
            base.OnActionExecuting(actionContext);
            actionContext.Response.Content = new ETagFilter(actionContext.Response.Content, actionContext.Request.Content);
        }

    }
    public class ETagFilter : HttpContent {

        private MemoryStream IncomingStream = null;
        private HttpContent response;
        private HttpContent request;

        public ETagFilter(HttpContent response, HttpContent request) {
            this.response = response;
            this.request = request;
        }

        private string GetToken(Stream stream) {
            var checksum = new byte[0];
            checksum = MD5.Create().ComputeHash(stream);
            return Convert.ToBase64String(checksum, 0, checksum.Length);
        }
        public void Write(byte[] buffer, int offset, int count) {
            
            
        }

        protected override async Task SerializeToStreamAsync(Stream stream, TransportContext context) {
            if (this.IncomingStream != null) {
                var buffer = new byte[this.IncomingStream.Length];
                await this.IncomingStream.WriteAsync(buffer, 0, buffer.Length);
                var token = GetToken(new MemoryStream(buffer));
                IEnumerable<string> found = new List<string>();

                if (this.request.Headers.TryGetValues("If-None-Match", out found)) {
                    if (token != found.First()) {
                        this.response.Headers.Add("ETag", token);
                        this.IncomingStream.Write(buffer, 0, buffer.Length);
                    } else {
                        this.response.Headers.Add("SuppressContent", "True");
                        this.response.Headers.Add("StatusCode", "304");
                        this.response.Headers.Add("StatusDescription", "Not Modified");
                        this.response.Headers.Add("Content-Length", "0");
                    }
                }
            }
        }

        protected override bool TryComputeLength(out long length) {
            if (this.IncomingStream == null) {
                length = -1;
            } else {
                length = this.IncomingStream.Length;
            }
            return length > -1;
        }
    }
}
