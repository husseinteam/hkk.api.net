﻿using System.IO;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Web.Http.Filters;

namespace HKK.Core.Api.ActionFilters {
    public class ReplaceWhitespaceAttribute : ActionFilterAttribute {

        public override void OnActionExecuted(HttpActionExecutedContext actionExecutedContext) {
            base.OnActionExecuted(actionExecutedContext);
            var response = actionExecutedContext.Response;
            // If it's a sitemap, just return.
            if (actionExecutedContext.Request.RequestUri.PathAndQuery == "/sitemap.xml") return;

            if (response.Content.Headers.ContentType.MediaType != "text/html") return;

            actionExecutedContext.Response.Content = new HelperClass(actionExecutedContext.Response.RequestMessage.Content);
        }

        private class HelperClass : HttpContent {
            private Stream InputStream;

            public HelperClass(HttpContent content) {
                content.ReadAsStreamAsync().ContinueWith(ts => this.InputStream = ts.Result).Wait();
            }

            protected override async Task SerializeToStreamAsync(Stream stream, TransportContext context) {

                var buffer = new byte[stream.Length];
                await this.InputStream.WriteAsync(buffer, 0, buffer.Length);

                var html = Encoding.UTF8.GetString(buffer, 0, buffer.Length);
                var reg = new Regex(@"(?<=\s)\s+(?![^<>]*</pre>)");
                html = reg.Replace(html, string.Empty);
                buffer = Encoding.UTF8.GetBytes(html);
                this.InputStream.Write(buffer, 0, buffer.Length);

            }

            protected override bool TryComputeLength(out long length) {
                if (this.InputStream == null) {
                    length = -1;
                } else {
                    length = this.InputStream.Length;
                }
                return length > -1;
            }
        }
    }
}
