﻿using HKK.Core.Api.Controller;
using HKK.Core.Domain.Aggregate;
using HKK.Core.Domain.Objects;
using HKK.Core.Api.CoreModel.Decorators;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Web.Http;

namespace HKK.Core.Api.Contracts {

    public interface IApiController {

        string ListRoute { get; }
        Type AggregateType { get; }

        Task<IHttpActionResult> Translations();
        Task<IHttpActionResult> Columns();
        Task<IHttpActionResult> Fields(long type);
        Task<IHttpActionResult> GridFields();

        Task<IHttpActionResult> List();
        Task<IHttpActionResult> All([FromUri] Dictionary<string, object> queryString);
        Task<IHttpActionResult> One(long id);

        Task<IHttpActionResult> Insert([FromBody]IDOBase domain);
        Task<IHttpActionResult> InsertAggregate([FromBody]object view);
        Task<IHttpActionResult> Edit([FromBody]object view);
        Task<IHttpActionResult> Delete(long id);

    }

}
