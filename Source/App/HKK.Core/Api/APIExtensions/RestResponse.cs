﻿using System.Net;

namespace HKK.Core.Api.APIExtensions {

    public class RestResponse {

        public string ReasonPhrase { get; internal set; }
        public HttpStatusCode StatusCode { get; internal set; }
        public bool Success { get; internal set; }
        public string Content { get; internal set; }

    }
    public class RestResponse<TResponse> : RestResponse {

        public TResponse ResponseObject { get; internal set; }
    }

}
