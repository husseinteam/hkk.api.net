﻿using HKK.Core.Extensions.Static;
using System;
using System.Net.Http;
using System.Web.Http.Controllers;
using System.Web.Http.Hosting;

namespace HKK.Core.Api.APIExtensions {

    public static class ContextExtensions {

        public static string GetRouteTemplate(this HttpActionContext actionContext) {

            return actionContext.ControllerContext.RouteData.Route.RouteTemplate;

        }
        
    }
}
