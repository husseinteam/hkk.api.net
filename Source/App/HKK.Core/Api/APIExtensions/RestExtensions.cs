﻿using HKK.Core.Extensions.Static;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Net;
using System.Net.Http;
using System.Net.Http.Formatting;
using System.Net.Http.Headers;
using System.Threading;
using System.Threading.Tasks;

namespace HKK.Core.Api.APIExtensions {
    public static class RestExtensions {

        private static HttpClient GenerateHttpClient(this string baseAddress, NameValueCollection headers = null) {

            var client = new HttpClient() {
                BaseAddress = new Uri(baseAddress),
                Timeout = TimeSpan.FromSeconds(10)
            };
            client.DefaultRequestHeaders.Accept.Clear();
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

            headers = headers ?? new NameValueCollection();
            foreach (string header in headers) {
                client.DefaultRequestHeaders.Add(header, headers[header]);
            }
            return client;

        }

        public static async Task<RestResponse<TGETResponse>> RequestRoute<TGETResponse>(this string baseAddress, string route, NameValueCollection headers = null)
            where TGETResponse : class {

            var client = baseAddress.GenerateHttpClient(headers);
            var response = await client.GetAsync(route);

            if (response.IsSuccessStatusCode) {
                var content = await response.Content.ReadAsStringAsync();
                return new RestResponse<TGETResponse>() {
                    ResponseObject = content.ToObject<TGETResponse>(),
                    ReasonPhrase = response.ReasonPhrase,
                    StatusCode = response.StatusCode,
                    Success = response.IsSuccessStatusCode,
                    Content = content
                };
            } else {
                return new RestResponse<TGETResponse>() {
                    ReasonPhrase = response.ReasonPhrase,
                    StatusCode = response.StatusCode,
                    Success = response.IsSuccessStatusCode
                };
            }

        }
        public static async Task<RestResponse<TGETResponse>> RequestRouteModifying<TGETResponse>(this string baseAddress, string route, Func<JObject, TGETResponse> mapper, NameValueCollection headers = null)
            where TGETResponse : class {

            var client = baseAddress.GenerateHttpClient(headers);
            var response = await client.GetAsync(route, HttpCompletionOption.ResponseContentRead);
            if (response.IsSuccessStatusCode) {
                var content = await response.Content.ReadAsStringAsync();
                return new RestResponse<TGETResponse>() {
                    ResponseObject = mapper(JObject.Parse(content)),
                    ReasonPhrase = response.ReasonPhrase,
                    StatusCode = response.StatusCode,
                    Success = response.IsSuccessStatusCode,
                    Content = content
                };
            } else {
                return new RestResponse<TGETResponse>() {
                    ReasonPhrase = response.ReasonPhrase,
                    StatusCode = response.StatusCode,
                    Success = response.IsSuccessStatusCode
                };
            }

        }

        public static async Task<RestResponse<TResponse>> RequestPost<TResponse, TEntity>(this string baseAddress, TEntity entity, string route, NameValueCollection headers = null)
            where TEntity : class {

            var client = baseAddress.GenerateHttpClient(headers);
            var response = await client.PostAsync(route, entity, new JsonMediaTypeFormatter());
            if (response.IsSuccessStatusCode) {
                var content = await response.Content.ReadAsStringAsync();
                return new RestResponse<TResponse>() {
                    ResponseObject = content.ToObject<TResponse>(),
                    ReasonPhrase = response.ReasonPhrase,
                    StatusCode = response.StatusCode,
                    Success = response.IsSuccessStatusCode,
                    Content = content
                };
            } else {
                return new RestResponse<TResponse>() {
                    ReasonPhrase = response.ReasonPhrase,
                    StatusCode = response.StatusCode,
                    Success = response.IsSuccessStatusCode
                };
            }

        }

        public static async Task<RestResponse> RequestDelete(this string baseAddress, string route, NameValueCollection headers = null) {

            var client = baseAddress.GenerateHttpClient(headers);
            var response = await client.DeleteAsync($"{baseAddress}/{route}");
            return new RestResponse() {
                ReasonPhrase = response.ReasonPhrase,
                StatusCode = response.StatusCode,
                Success = response.IsSuccessStatusCode,
                Content = await response.Content.ReadAsStringAsync()
            };

        }

    }
}
