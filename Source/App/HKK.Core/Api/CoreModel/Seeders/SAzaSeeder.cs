﻿using HKK.Core.Api.Controller;
using HKK.Core.Api.CoreModel.Models.Membership;
using HKK.Core.Api.CoreModel.Views.Membership;
using HKK.Core.Engines;
using HKK.Core.Extensions.Static;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HKK.Core.Api.CoreModel.Seeders {

    internal class SAzaSeeder {

        internal static Session GetAdminSession() {
            return new Session {
                UserName = "25871077074",
                Password = "BE671920-FE71-4295-856F-27581994CB5B",
            };
        }

        internal static async Task<UserView> NewUserWith(Session session) {
            var eng = SDataEngine.GenerateAGEngine<UserView>();
            var kunyeResp = (await SDataEngine.GenerateDOEngine<Contact>().InsertIfNotExists(
                new Contact {
                    Phone = 11.RandomDigits(),
                    FirstName = 1.RandomWords(),
                    LastName = 1.RandomWords(),
                    Profession = EProfession.Scholar,
                    PlaceOfBirth = "Sivas",
                    DateOfBirth = 1982.RandomDate("tr-TR")
                }));
            var kunye = kunyeResp.Single;
            var azaResp = (await SDataEngine.GenerateDOEngine<User>().InsertIfNotExists(new User { ContactID = kunye.ID, Password = session.Password, UserName = session.UserName }));

            var aza = azaResp.Single;
            Role emirVazifesi = null;
            var emirVazifesiResp = SDataEngine.GenerateDOEngine<Role>().SelectSingleSync(v => v.Title == "Emir");
            if (emirVazifesiResp.HasData == false) {
                emirVazifesi = new Role {
                    Title = "Emir"
                }.InsertSelf().SingleOrFirst as Role;
            } else {
                emirVazifesi = emirVazifesiResp.Single;
            }

            Role azaVazifesi = null;
            var azaVazifesiResp = SDataEngine.GenerateDOEngine<Role>().SelectSingleSync(v => v.Title == "Aza");
            if (azaVazifesiResp.HasData == false) {
                azaVazifesi = new Role {
                    Title = "Aza"
                }.InsertSelf().SingleOrFirst as Role;
            } else {
                azaVazifesi = azaVazifesiResp.Single;
            }

            var tahditler = new List<ConsentView>();
            foreach (var tarif in new[] { "lists", "inserts", "edits", "deletes" }) {
                Authority kaide = null;
                var kaideResp = SDataEngine.GenerateDOEngine<Authority>().SelectSingleSync(k => k.Ability == tarif);
                if (kaideResp.HasData == false) {
                    kaide = new Authority {
                        Ability = tarif,
                    }.InsertSelf().SingleOrFirst as Authority;
                } else {
                    kaide = kaideResp.Single;
                }
                Consent tahdit = null;
                var tahditResp = SDataEngine.GenerateDOEngine<Consent>().SelectSingleSync(t => t.AuthorityID == kaide.ID);
                if (tahditResp.HasData == false) {
                    tahdit = new Consent {
                        Name = $"{azaVazifesi.Title} {tarif}",
                        UserID = aza.ID,
                        RoleID = azaVazifesi.ID,
                        AuthorityID = kaide.ID
                    }.InsertSelf().SingleOrFirst as Consent;
                } else {
                    tahdit = tahditResp.Single;
                }
                tahditler.Add(tahdit.MapToAggregate(typeof(ConsentView)).As<ConsentView>());
            }
            return new UserView {
                UserID = aza.ID,
                UserContactID = kunye.ID,
                UserPassword = aza.Password,
                UserName = aza.UserName,
                ID = aza.ID,
                ContactPhone = kunye.Phone,
                ContactFirstName = kunye.FirstName,
                ContactPlaceOfBirth = kunye.PlaceOfBirth,
                ContactProfession = kunye.Profession,
                ContactDateOfBirth = kunye.DateOfBirth,
                ConactLastName = kunye.LastName,
                Consents = tahditler
            };
        }

    }

}
