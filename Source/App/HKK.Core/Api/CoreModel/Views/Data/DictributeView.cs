﻿
using System;
using HKK.Core.Domain.Aggregate;
using HKK.Core.Api.CoreModel.Decorators;
using System.Globalization;
using HKK.Core.Engines;
using HKK.Core.Domain.Contracts;
using System.Collections.Generic;
using HKK.Core.Api.CoreModel.Views.Data.Resources;
using HKK.Core.Api.CoreModel.Models.Data;
using HKK.Core.Api.Controller;

namespace HKK.Core.Api.CoreModel.Views.Data {

    [ResourceLocator(typeof(DictributeResource))]
    public class DictributeView : AGBase<DictributeView> {
        
        [IDLocator]
        public long DictributeID { get; set; }

        [TableColumn(Order = 1)]
        public string DictributeKey { get; set; }

        [TableColumn(Order = 2)]
        public string DictributeValue { get; set; }

        [TableColumn(Order = 3)]
        public string DictributeDataType { get; set; }

        public override Type MainDomain => typeof(Dictribute);

        public override string TextValue => $"[{DictributeKey}: <{DictributeValue}>]";

        public override string ResourceName => "dictributes";

        protected override void Map(IAGViewBuilder<DictributeView> builder) {
            builder
                .MapsTo(x => x.SchemaName("HKK").ViewName("DictributeView"))
                .Select<Dictribute>(li =>
                    li
                    .Map(x => x.ID, x => x.DictributeID)
                    .Map(x => x.Key, x => x.DictributeKey)
                    .Map(x => x.Value, x => x.DictributeValue)
                );
        }
    }
}