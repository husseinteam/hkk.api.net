﻿
using System;
using HKK.Core.Domain.Aggregate;
using HKK.Core.Api.CoreModel.Decorators;
using System.Globalization;
using HKK.Core.Engines;
using HKK.Core.Domain.Contracts;
using System.Collections.Generic;
using HKK.Core.Api.CoreModel.Views.Data.Resources;
using HKK.Core.Api.CoreModel.Models.Data;
using HKK.Core.Api.Controller;

namespace HKK.Domain.Data.Views {

    [ResourceLocator(typeof(LogResource))]
    public class LogView : AGBase<LogView> {
        [IDLocator]
        public long LogID { get; set; }

        [TableColumn(Order = 1)]
        public string LogStackTrace { get; set; }

        [TableColumn(Order = 2)]
        public DateTime LogTime { get; set; }

        public override Type MainDomain => typeof(Log);

        public override string TextValue => LogStackTrace;

        public override string ResourceName => "logs";

        protected override void Map(IAGViewBuilder<LogView> builder) {
            builder
                .MapsTo(x => x.SchemaName("HKK").ViewName("LogView"))
                .Select<Log>(li =>
                    li
                    .Map(x => x.ID, x => x.LogID)
                    .Map(x => x.StackTrace, x => x.LogStackTrace)
                    .Map(x => x.Time, x => x.LogTime)
                );
        }
    }
}