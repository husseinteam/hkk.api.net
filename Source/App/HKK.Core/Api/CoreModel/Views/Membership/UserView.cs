﻿using HKK.Core.Api.Controller;
using HKK.Core.Api.CoreModel.Models.Membership;
using HKK.Core.Api.CoreModel.Views.Membership.Resources;
using HKK.Core.Domain.Aggregate;
using HKK.Core.Api.CoreModel.Decorators;
using HKK.Core.Extensions.Static;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Reflection;

namespace HKK.Core.Api.CoreModel.Views.Membership {

    [ResourceLocator(typeof(AzaResource))]
    public class UserView : AGBase<UserView> {

        [IDLocator]
        public long UserID { get; set; }

        [ClientSide("userContactId")]
        public long UserContactID { get; set; }

        [ClientSide("firstName")]
        public string ContactFirstName { get; set; }

        [ClientSide("lastName")]
        public string ConactLastName { get; set; }

        [ClientSide("placeOfBirth")]
        public string ContactPlaceOfBirth { get; set; }

        [ClientSide("phone")]
        public string ContactPhone { get; set; }

        [ClientSide("dateOfBirth")]
        public DateTime ContactDateOfBirth { get; set; }
        [ComputedField(EComputedType.FormattedString, typeof(DateTime))]
        public string KunyeMevlidComputed {
            get => ContactDateOfBirth.GetTranslatedDateString(format: "dd MMMM yyyy");
            set => ContactDateOfBirth = DateTime.Parse(value, CultureInfo.DefaultThreadCurrentUICulture);
        }

        [ClientSide("profession")]
        public EProfession ContactProfession { get; set; }
        [ComputedField(EComputedType.Enum, typeof(EProfession)), FormField(EFormType.Insert | EFormType.Edit)]
        public string KunyeMeslekComputed {
            get => ContactProfession != 0 ? ContactProfession.GetEnumResource() : "";
            set => ContactProfession = value.ToEnum<EProfession>();
        }

        [ClientSide("userName")]
        public string UserName { get; set; }

        [ClientSide("password")]
        public string UserPassword { get; set; }

        [ClientSide("accessToken")]
        public string UserAccessToken { get; set; }

        [ClientSide("activationToken")]
        public string UserActivationToken { get; set; }

        [ClientSide("recoveryToken")]
        public string UserRecoveryToken { get; set; }

        [ClientSide("activationTokenExpiresOn")]
        public DateTime UserActivationTokenExpiresOn { get; set; }

        [ClientSide("accessTokenExpiresOn")]
        public DateTime UserAccessTokenExpiresOn { get; set; }

        [ClientSide("recoveryTokenExpiresOn")]
        public DateTime UserRecoveryTokenExpiresOn { get; set; }

        [ClientSide("consents")]
        [HavingCollection("ConsentUserID")]
        public List<ConsentView> Consents { get; set; } = new List<ConsentView>();

        public override Type MainDomain => typeof(User);

        public override string TextValue => ContactFirstName;

        public override string ResourceName => "users";

        protected override void Map(IAGViewBuilder<UserView> builder) {

            builder
                .MapsTo(x => x.SchemaName("MEM").ViewName("UserView"))
                .Select<User>(li =>
                    li
                    .Map(x => x.ID, x => x.UserID)
                    .Map(x => x.ContactID, x => x.UserContactID)
                    .Map(x => x.UserName, x => x.UserName)
                    .Map(x => x.Password, x => x.UserPassword)
                    .Map(x => x.AccessToken, x => x.UserAccessToken)
                    .Map(x => x.ActivationToken, x => x.UserActivationToken)
                    .Map(x => x.RecoveryToken, x => x.UserRecoveryToken)
                    .Map(x => x.AccessTokenExpiresOn, x => x.UserAccessTokenExpiresOn)
                    .Map(x => x.ActivationTokenExpiresOn, x => x.UserActivationTokenExpiresOn)
                    .Map(x => x.RecoveryTokenExpiresOn, x => x.UserRecoveryTokenExpiresOn)
                ).OuterJoin<Contact>(li =>
                    li
                    .Map(x => x.Phone, x => x.ContactPhone)
                    .Map(x => x.FirstName, x => x.ContactFirstName)
                    .Map(x => x.LastName, x => x.ConactLastName)
                    .Map(x => x.PlaceOfBirth, x => x.ContactPlaceOfBirth)
                    .Map(x => x.Profession, x => x.ContactProfession)
                    .Map(x => x.DateOfBirth, x => x.ContactDateOfBirth)
                ).On<User>((a, b) => a.ID == b.ContactID);

        }

    }

}
