﻿using System;
using HKK.Core.Domain.Aggregate;
using HKK.Core.Api.CoreModel.Models.Membership;
using HKK.Core.Api.CoreModel.Decorators;
using HKK.Core.Api.CoreModel.Views.Membership.Resources;
using HKK.Core.Api.Controller;

namespace HKK.Core.Api.CoreModel.Views.Membership {

    [ResourceLocator(typeof(TahditResource))]
    public class ConsentView : AGBase<ConsentView> {

        [IDLocator]
        public long ConsentID { get; set; }

        public long ConsentUserID { get; set; }

        [ClientSide("name")]
        public string ConsentName { get; set; }

        [ClientSide("roleName")]
        public string RoleName { get; set; }

        [ClientSide("authorityAbility")]
        public string AuthorityAbility { get; set; }

        public override Type MainDomain => typeof(Consent);

        public override string TextValue => ConsentName;

        public override string ResourceName => "consents";

        protected override void Map(IAGViewBuilder<ConsentView> builder) {
            builder
                .MapsTo(x => x.SchemaName("MEM").ViewName("ConsentView"))
                .Select<Consent>(li =>
                    li
                    .Map(x => x.ID, x => x.ConsentID)
                    .Map(x => x.Name, x => x.ConsentName)
                ).OuterJoin<User>(li => 
                    li.Map(x => x.ID, x => x.ConsentUserID)
                ).On<Consent>((a, b) => a.ID == b.UserID)
                .OuterJoin<Role>(li =>
                    li
                    .Map(x => x.Title, x => x.RoleName)
                ).On<Consent>((a, b) => a.ID == b.RoleID)
                .OuterJoin<Authority>(li =>
                    li
                    .Map(x => x.Ability, x => x.AuthorityAbility)
                ).On<Consent>((a, b) => a.ID == b.AuthorityID);
        }
    }
}