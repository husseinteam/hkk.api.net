﻿using System;

namespace HKK.Core.Api.CoreModel.Decorators {
    [System.AttributeUsage(AttributeTargets.Class | AttributeTargets.Enum, Inherited = true, AllowMultiple = false)]
    public sealed class i18nAttribute : Attribute {

        public i18nAttribute(string container, ELang localizations) {
            this.Container = container;
            this.Localizations = localizations;
        }

        public string Container { get; private set; }

        public ELang Localizations { get; private set; }

    }
}
