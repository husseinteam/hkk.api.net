﻿using System;

namespace HKK.Core.Api.CoreModel.Decorators {
    [System.AttributeUsage(AttributeTargets.Property, Inherited = false, AllowMultiple = false)]
    public sealed class FormFieldAttribute : Attribute {

        public FormFieldAttribute(EFormType formType) {
            this.FormType = formType;
        }

        public EFormType FormType { get; private set; }
        public int Order { get; set; }
    }
}
