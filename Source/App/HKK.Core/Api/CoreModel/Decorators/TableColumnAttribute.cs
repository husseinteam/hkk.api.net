﻿using System;

namespace HKK.Core.Api.CoreModel.Decorators {
    [System.AttributeUsage(AttributeTargets.Property, Inherited = false, AllowMultiple = false)]
    public sealed class TableColumnAttribute : Attribute {
        public TableColumnAttribute() {
        }

        public int Order { get; set; }
        public bool Disabled { get; set; }
        public string InputType { get; set; }
    }
}
