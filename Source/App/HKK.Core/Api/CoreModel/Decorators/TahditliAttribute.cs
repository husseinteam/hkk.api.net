﻿using HKK.Core.Api.Contracts;
using HKK.Core.Api.CoreModel.Models.Membership;
using HKK.Core.Domain.Contracts;
using HKK.Core.Engines;
using HKK.Core.Extensions.Static;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Reflection;
using System.Threading;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http.Controllers;
using System.Web.Http.Filters;

namespace HKK.Core.Api.CoreModel.Decorators {
    [AttributeUsage(AttributeTargets.Method, AllowMultiple = false, Inherited = true)]
    public sealed class TahditliAttribute : ActionFilterAttribute {

        private readonly static IDOEngine<User> _AzaEngine = SDataEngine.GenerateDOEngine<User>();

        public override Task OnActionExecutedAsync(HttpActionExecutedContext actionExecutedContext, CancellationToken cancellationToken) {
            var controllerType = actionExecutedContext.ActionContext.ControllerContext.Controller.GetType();
            var actionName = actionExecutedContext.ActionContext.ActionDescriptor.ActionName;
           if (actionExecutedContext.Request.Headers.TryGetValues("x-hds-tahdit", out var headers)) {
                if (!Celse.Cari(headers.FirstOrDefault(), out var aza)) {
                    return FaultResponse(actionExecutedContext, "invalid-token");
                } else {
                    var kaideAttrs = controllerType.GetMethod(actionName).GetCustomAttributes<KaideAttribute>();
                    var vazifeAttr = controllerType.GetMethod(actionName).GetCustomAttribute<VazifeAttribute>();
                    if (kaideAttrs.Count() > 0) {
                        var vresp = SDataEngine.GenerateDOEngine<Role>().SelectSingleSync(x => x.Title == vazifeAttr.Tarif);
                        if (vresp.HasData) {
                            foreach (var kaideTarif in kaideAttrs.Select(ka => ka.Tarif)) {
                                var kresp = SDataEngine.GenerateDOEngine<Authority>().SelectSingleSync(x => x.Ability == kaideTarif);
                                if (kresp.HasData) {
                                    var tahditlerResp = SDataEngine.GenerateDOEngine<Consent>()
                                        .SelectBySync(t => t.AuthorityID == kresp.EntityID && t.RoleID == vresp.EntityID);
                                    if (tahditlerResp.HasData) {
                                        var tahditler = tahditlerResp.Items;
                                        if (!tahditler.Select(t => t.UserID).Contains(aza.ID)) {
                                            return FaultResponse(actionExecutedContext, $"rule-violation<{aza} Not Permitted>");
                                        }
                                    } else {
                                        return FaultResponse(actionExecutedContext, $"server-error<no-tahdit-found>");
                                    }
                                } else {
                                    return FaultResponse(actionExecutedContext, $"server-error<no-kaide-found>");
                                }
                            }
                        } else {
                            return FaultResponse(actionExecutedContext, $"server-error<no-vazife-found>");
                        }
                    }
                    if (DateTime.Now > aza.AccessTokenExpiresOn) {
                        var token = Guid.NewGuid().ToString("N");
                        _AzaEngine.UpdateSync(aza.ID, (modifier) => modifier
                           .Update(a => a.AccessToken).Set(token)
                           .Update(a => a.AccessTokenExpiresOn).Set(DateTime.Now.AddMinutes(11)));
                    }
                    return OKResponse(actionExecutedContext, "token-refreshed");
                }
            } else {
                return FaultResponse(actionExecutedContext, "no-token-provided");
            }
        }

        private Task<HttpResponseMessage> FaultResponse(HttpActionExecutedContext actionContext, string message) {
            actionContext.Response = new HttpResponseMessage {
                StatusCode = HttpStatusCode.Forbidden,
                RequestMessage = actionContext.Request,
                ReasonPhrase = message
            };
            return Task.FromResult(actionContext.Response);
        }

        private Task<HttpResponseMessage> OKResponse(HttpActionExecutedContext actionContext, string message) {
            actionContext.Response.ReasonPhrase = message;
            return Task.FromResult(actionContext.Response);
        }
    }
}