﻿using System;

namespace HKK.Core.Api.CoreModel.Decorators {
    [Flags]
    public enum  ELang : long {

        TR = 1,
        EN = 2,

        //Sunday = 1,
        //Monday = 2,
        //Tuesday = 4,
        //Wednesday = 8,
        //Thursday = 16,
        //Friday = 32,
        //Saturday = 64
    }
}
