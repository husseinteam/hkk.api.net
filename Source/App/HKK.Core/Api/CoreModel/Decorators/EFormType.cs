﻿using System;

namespace HKK.Core.Api.CoreModel.Decorators {
    [Flags]
    public enum  EFormType : long {

        Insert = 1,
        Edit = 2,

        //Sunday = 1,
        //Monday = 2,
        //Tuesday = 4,
        //Wednesday = 8,
        //Thursday = 16,
        //Friday = 32,
        //Saturday = 64
    }
}
