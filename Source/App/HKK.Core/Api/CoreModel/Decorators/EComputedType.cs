﻿namespace HKK.Core.Api.CoreModel.Decorators {
    public enum EComputedType {

        Enum,
        FormattedString
    }
}
