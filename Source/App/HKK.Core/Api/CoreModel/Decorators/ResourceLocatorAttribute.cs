﻿using System;

namespace HKK.Core.Api.CoreModel.Decorators {
    [System.AttributeUsage(AttributeTargets.Class | AttributeTargets.Enum, Inherited = false, AllowMultiple = false)]
    public sealed class ResourceLocatorAttribute : Attribute {

        readonly Type resourceType;

        // This is a positional argument
        public ResourceLocatorAttribute(Type resourceType) {
            this.resourceType = resourceType;
        }

        public Type ResourceType {
            get { return resourceType; }
        }

        // This is a named argument
        public int NamedInt { get; set; }
    }
}
