﻿using System;

namespace HKK.Core.Api.CoreModel.Decorators {
    [System.AttributeUsage(AttributeTargets.Property, Inherited = false, AllowMultiple = false)]
    public sealed class HavingCollectionAttribute : Attribute {

        public HavingCollectionAttribute(string inverseIDProperty) {
            this.InverseIDProperty = inverseIDProperty;
        }

        public string InverseIDProperty { get; private set; }

    }
}