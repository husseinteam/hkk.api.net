﻿using System;
namespace HKK.Core.Api.CoreModel.Decorators {
    [AttributeUsage(AttributeTargets.Property, Inherited = true, AllowMultiple = false)]
    public sealed class ClientSideAttribute : Attribute {

        public ClientSideAttribute(string name) {

            this.Name = name;

        }

        public string Name { get; private set; }
    }

}
