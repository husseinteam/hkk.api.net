﻿using System;

namespace HKK.Core.Api.CoreModel.Decorators {

    [AttributeUsage(AttributeTargets.Property, Inherited = false, AllowMultiple = false)]
    public sealed class IDLocatorAttribute : Attribute {

        public IDLocatorAttribute() {
        }

    }

}
