﻿using HKK.Core.Api.Contracts;
using HKK.Core.Domain.Query;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http.Controllers;
using System.Web.Http.Filters;

namespace HKK.Core.Api.CoreModel.Decorators {
    [AttributeUsage(AttributeTargets.Method, AllowMultiple = true, Inherited = true)]
    public sealed class KaideAttribute : FilterAttribute {

        private readonly string _Tarif;

        internal KaideAttribute() {
            _Tarif = "*";
        }

        public KaideAttribute(string tarif) {
            _Tarif = tarif;
        }

        public string Tarif => _Tarif;

    }
}