﻿using HKK.Core.Api.Contracts;
using HKK.Core.Domain.Query;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http.Controllers;
using System.Web.Http.Filters;

namespace HKK.Core.Api.CoreModel.Decorators {
    [AttributeUsage(AttributeTargets.Method, AllowMultiple = false, Inherited = true)]
    public sealed class VazifeAttribute : FilterAttribute {

        private readonly string _Tarif;

        internal VazifeAttribute() {
            _Tarif = "*";
        }

        public VazifeAttribute(string tarif) {
            _Tarif = tarif;
        }

        public string Tarif => _Tarif;

    }
}