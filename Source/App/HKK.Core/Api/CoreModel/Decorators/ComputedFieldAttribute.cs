﻿using System;

namespace HKK.Core.Api.CoreModel.Decorators {
    [System.AttributeUsage(AttributeTargets.Property, Inherited = false, AllowMultiple = false)]
    public sealed class ComputedFieldAttribute : Attribute {
        
        public ComputedFieldAttribute(EComputedType computedType, Type referencedType) {

            this.ComputedType = computedType;
            this.ReferencedType = referencedType;

        }

        public EComputedType ComputedType { get; private set; }
        public Type ReferencedType { get; private set; }
    }

}
