﻿using HKK.Core.Api.CoreModel.Models.Membership;
using HKK.Core.Domain.Contracts;
using HKK.Core.Engines;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HKK.Core.Api.CoreModel {
    public static class Celse {

        private static IDOEngine<User> azaeng = SDataEngine.GenerateDOEngine<User>();

        public static bool Cari(string anahtar, out User aza) {
            aza = azaeng.SelectSingleSync(a => a.AccessToken == anahtar).Single;
            return aza != null;
        }

    }
}