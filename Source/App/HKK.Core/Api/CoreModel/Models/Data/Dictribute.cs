﻿using HKK.Core.Domain.Objects;
using HKK.Core.Api.CoreModel.Decorators;
using System;
using System.Collections.Generic;

namespace HKK.Core.Api.CoreModel.Models.Data {

    public class Dictribute : DOBase<Dictribute> {

        public string Key { get; set; }

        public string Value { get; set; }

        public string DataType { get; set; }

        public override string TextValue => $"[{Key}: <{Value}>]";

        protected override void Map(IDOTableBuilder<Dictribute> builder) {

            builder.MapsTo(x => { x.SchemaName("HKK").TableName("Dictributes"); });

            builder.For(d => d.Key).IsTypeOf(EDataType.String).HasMaxLength(512).IsRequired();
            builder.For(d => d.Value).IsTypeOf(EDataType.Text).IsRequired();
            builder.For(d => d.DataType).IsTypeOf(EDataType.Text).IsNullable();

            builder.UniqueKey(x => x.Key);
            
        }

    }

}
