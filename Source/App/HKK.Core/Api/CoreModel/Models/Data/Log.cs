﻿using HKK.Core.Domain.Objects;
using HKK.Core.Api.CoreModel.Decorators;
using System;
using System.Collections.Generic;

namespace HKK.Core.Api.CoreModel.Models.Data {

    public class Log : DOBase<Log> {

        public string StackTrace { get; set; }

        public DateTime Time { get; set; }

        public override string TextValue => StackTrace;

        protected override void Map(IDOTableBuilder<Log> builder) {

            builder.MapsTo(x => { x.SchemaName("HKK").TableName("Logs"); });

            builder.For(d => d.StackTrace).IsTypeOf(EDataType.Text).IsRequired();
            builder.For(d => d.Time).IsTypeOf(EDataType.DateTime).IsNullable();
            
        }

    }

}
