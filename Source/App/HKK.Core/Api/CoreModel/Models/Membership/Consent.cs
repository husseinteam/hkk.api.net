﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HKK.Core.Domain.Objects;
using HKK.Core.Api.CoreModel.Decorators;

namespace HKK.Core.Api.CoreModel.Models.Membership {
    public class Consent : DOBase<Consent> {


        public long UserID { get; set; }

        public long AuthorityID { get; set; }

        public long RoleID { get; set; }

        public string Name { get; set; }

        public override string TextValue => Name;

        protected override void Map(IDOTableBuilder<Consent> builder) {
            builder.MapsTo(x => { x.SchemaName("MEM").TableName("Consents"); });

            builder.For(d => d.Name).IsTypeOf(EDataType.String).IsRequired().HasMaxLength(255);

            builder.ForeignKey(a => a.UserID).References<User>(a => a.ID);
            builder.ForeignKey(a => a.AuthorityID).References<Authority>(a => a.ID);

            builder.UniqueKey(a => a.UserID, a => a.AuthorityID, a => a.RoleID);
            builder.UniqueKey(a => a.Name);

        }
    }
}
