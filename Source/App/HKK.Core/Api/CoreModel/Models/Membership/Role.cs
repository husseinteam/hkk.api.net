﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HKK.Core.Domain.Objects;
using HKK.Core.Api.CoreModel.Decorators;

namespace HKK.Core.Api.CoreModel.Models.Membership {
    public class Role : DOBase<Role> {

        public string Title { get; set; }

        public override string TextValue => Title;

        protected override void Map(IDOTableBuilder<Role> builder) {
            builder.MapsTo(x => { x.SchemaName("MEM").TableName("Roles"); });

            builder.For(d => d.Title).IsTypeOf(EDataType.String).IsRequired().HasMaxLength(255);
            
        }
    }
}
