﻿
using HKK.Core.Api.CoreModel.Decorators;

namespace HKK.Core.Api.CoreModel.Models.Membership {

    [i18n("profession", ELang.EN | ELang.TR)]
    public enum EProfession {

        Trader = 1,
        Doctor,
        Scholar,
        HouseWoman

    }

}
