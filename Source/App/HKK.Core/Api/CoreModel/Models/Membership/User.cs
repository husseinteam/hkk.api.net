﻿using HKK.Core.Domain.Objects;
using HKK.Core.Api.CoreModel.Decorators;
using System;
using System.Collections.Generic;

namespace HKK.Core.Api.CoreModel.Models.Membership {

    public class User : DOBase<User> {

        public long ContactID { get; set; }

        public string UserName { get; set; }

        public string Password { get; set; }

        public string AccessToken { get; set; }

        public string ActivationToken { get; set; }

        public string RecoveryToken { get; set; }

        public DateTime ActivationTokenExpiresOn { get; set; }

        public DateTime AccessTokenExpiresOn { get; set; }

        public DateTime RecoveryTokenExpiresOn { get; set; }

        public override string TextValue => UserName.ToString();

        protected override void Map(IDOTableBuilder<User> builder) {

            builder.MapsTo(x => { x.SchemaName("MEM").TableName("Users"); });

            builder.For(d => d.Password).IsTypeOf(EDataType.String).IsRequired().HasMaxLength(64);
            builder.For(d => d.UserName).IsTypeOf(EDataType.String).IsRequired().HasMaxLength(512);

            builder.For(d => d.ActivationToken).IsTypeOf(EDataType.String).HasMaxLength(128);
            builder.For(d => d.AccessToken).IsTypeOf(EDataType.String).HasMaxLength(128);
            builder.For(d => d.RecoveryToken).IsTypeOf(EDataType.String).HasMaxLength(128);

            builder.For(d => d.ActivationTokenExpiresOn).IsTypeOf(EDataType.DateTime);
            builder.For(d => d.AccessTokenExpiresOn).IsTypeOf(EDataType.DateTime);
            builder.For(d => d.RecoveryTokenExpiresOn).IsTypeOf(EDataType.DateTime);

            builder.ForeignKey(d => d.ContactID).References<Contact>(d => d.ID);

            builder.UniqueKey(d => d.ContactID);
            builder.UniqueKey(d => d.UserName);
        }

    }

}
