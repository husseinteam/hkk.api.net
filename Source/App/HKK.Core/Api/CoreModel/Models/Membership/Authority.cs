﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HKK.Core.Domain.Objects;
using HKK.Core.Api.CoreModel.Decorators;

namespace HKK.Core.Api.CoreModel.Models.Membership {
    public class Authority : DOBase<Authority> {

        public string Ability { get; set; }

        public override string TextValue => Ability;

        protected override void Map(IDOTableBuilder<Authority> builder) {
            builder.MapsTo(x => { x.SchemaName("MEM").TableName("Authorities"); });

            builder.For(d => d.Ability).IsTypeOf(EDataType.String).IsRequired().HasMaxLength(255);

            builder.UniqueKey(x => x.Ability);
        }
    }
}
