﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HKK.Core.Domain.Objects;
using HKK.Core.Api.CoreModel.Decorators;
using HKK.Core.Extensions.Static;

namespace HKK.Core.Api.CoreModel.Models.Membership {
    public class Contact : DOBase<Contact> {


        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string PlaceOfBirth { get; set; }

        public string Phone { get; set; }

        public DateTime DateOfBirth { get; set; }

        public EProfession Profession { get; set; }

        public override string TextValue => $"{FirstName} {LastName}";

        protected override void Map(IDOTableBuilder<Contact> builder) {
            builder.MapsTo(x => { x.SchemaName("MEM").TableName("Contacts"); });

            builder.For(d => d.FirstName).IsTypeOf(EDataType.String).IsRequired().HasMaxLength(255);
            builder.For(d => d.LastName).IsTypeOf(EDataType.String).IsRequired().HasMaxLength(64);
            builder.For(d => d.PlaceOfBirth).IsTypeOf(EDataType.String).HasMaxLength(128);
            builder.For(d => d.Phone).IsTypeOf(EDataType.String).HasMaxLength(32);
            builder.For(d => d.DateOfBirth).IsTypeOf(EDataType.Date);
            builder.For(d => d.Profession).IsTypeOf(EDataType.Enum);

        }
    }
}
