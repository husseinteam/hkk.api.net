﻿using HKK.Core.Api.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using HKK.Core.Api.CoreModel.Decorators;
using HKK.Core.Api.Decorators;
using System.Threading.Tasks;
using HKK.Core.Engines;
using HKK.Core.Api.Controller;
using HKK.Core.Extensions.Static;
using System.Web.Http.Cors;
using HKK.Core.Api.CoreModel.Views.Membership;
using HKK.Core.Api.CoreModel.Models.Membership;
using HKK.Core.Api.CoreModel.Seeders;

namespace HKK.Core.Api.CoreModel.Controllers {

    public class AzaController : GenericController {
        public AzaController() : base(typeof(UserView)) {
        }

        private Func<string, string, Session> FalseToken => (message, exception) => {
            return new Session {
                Token = new ApiToken {
                    AccessToken = "",
                    Success = false
                },
                ClientMessage = message,
                ServerException = exception
            };
        };

        public override Type AggregateType => typeof(UserView);

        #region Session Actions

        [RoutePost("sign-up", ArgumentTypes = new[] { typeof(Session) })]
        public async Task<IHttpActionResult> Signon([FromBody]Session session) {

            var eng = SDataEngine.GenerateAGEngine<UserView>();
            var existingResponse = (await eng.SelectSingle(a => a.UserName == session.UserName));
            if (existingResponse.HasData == false) {
                var aza = await SAzaSeeder.NewUserWith(session);
                if (aza != null) {
                    session.Token = new ApiToken();
                    session.ClientMessage = $"signed-up";
                } else {
                    session = FalseToken("critical-sign-up-error", existingResponse.Message);
                }
            } else {
                session.ClientMessage = $"sign-up-recurrence-error";
                session.UserAlreadyExists = true;
            }
            return session.ParseGenericJson(Request);

        }

        [RoutePost("log-out", ArgumentTypes = new[] { typeof(Session) })]
        public async Task<IHttpActionResult> Logout([FromBody]Session session) {

            var eng = SDataEngine.GenerateAGEngine<UserView>();
            var token = session.Token.AccessToken;
            var existingResponse = (await eng.SelectSingle(a => a.UserAccessToken == token));
            if (existingResponse.HasData) {
                var aza = existingResponse.Single;
                if (aza != null) {
                    session.Token.AccessToken = "";
                    session.Token.Success = true;
                    session.ClientMessage = $"goodbye";
                } else {
                    session = FalseToken("invalid-user-credentials", existingResponse.Message);
                }
            } else {
                session = FalseToken(existingResponse.Message, existingResponse.Message);
            }
            return session.ParseGenericJson(Request);
        }
        [RoutePost("session", ArgumentTypes = new[] { typeof(Session) })]
        public async Task<IHttpActionResult> NewSession(Session session) {
            var eng = SDataEngine.GenerateDOEngine<User>();
            var existingResponse = (await eng.SelectSingle(a => a.UserName == session.UserName && a.Password == session.Password));
            session.UserAlreadyExists = existingResponse.HasData;
            if (existingResponse.HasData) {
                var existing = existingResponse.Single;
                if (existing.AccessToken == null || existing.AccessTokenExpiresOn > DateTime.Now) {
                    //expired or not assigned
                    long id = existing.ID;
                    var resp = await eng.Update(id, (modifier) => modifier
                       .Update(a => a.AccessToken).Set(Guid.NewGuid().ToString("N"))
                       .Update(a => a.AccessTokenExpiresOn).Set(DateTime.Now.AddMinutes(11)));
                    session.Token.Success = resp.HasErrors == false;
                    session.Token.AccessToken = resp.Single.AccessToken;
                } else {
                    session.Token.AccessToken = existing.AccessToken;
                    session.Token.Success = true;
                }
                session.ClientMessage = $"token-generated";
            } else {
                session = FalseToken("invalid-user-credentials", existingResponse.Message);
            }
            return session.ParseGenericJson(Request);
        }

        #endregion

    }
}