﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace HKK.Core.Api.Decorators {
    [AttributeUsage(AttributeTargets.Method, Inherited = true, AllowMultiple = false)]
    public class RouteGetAttribute : RelayerAttribute {
        public RouteGetAttribute(string path) : base(path) {
        }

        public override HttpMethod Method => HttpMethod.Get;
    }
}
