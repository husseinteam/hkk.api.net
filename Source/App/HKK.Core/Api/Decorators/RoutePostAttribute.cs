﻿
using HKK.Core.Domain.Aggregate;
using HKK.Core.Domain.Objects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace HKK.Core.Api.Decorators {
    [AttributeUsage(AttributeTargets.Method, Inherited = true, AllowMultiple = false)]
    public class RoutePostAttribute : RelayerAttribute {
        public RoutePostAttribute(string path) : base(path) {
            ArgumentTypes = new[] { typeof(IAGBase) };
        }
        
        public override HttpMethod Method => HttpMethod.Post;
        
    }
}
