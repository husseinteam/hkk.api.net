﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace HKK.Core.Api.Decorators {
    [AttributeUsage(AttributeTargets.Method, Inherited = true, AllowMultiple = false)]
    public class RouteDeleteAttribute : RelayerAttribute {
        public RouteDeleteAttribute(string path) : base(path) {
        }

        public override HttpMethod Method => HttpMethod.Delete;
    }
}
