﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace HKK.Core.Api.Decorators {
    public abstract class RelayerAttribute : Attribute {

        readonly string path;

        public RelayerAttribute(string path) {
            this.path = path;
            ArgumentTypes = Type.EmptyTypes;
        }

        public string Path {
            get { return path; }
        }

        public Type[] ArgumentTypes { get; set; }
        public abstract HttpMethod Method { get; }
    }
}
