﻿
using HKK.Core.Domain.Aggregate;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace HKK.Core.Api.Decorators {
    [AttributeUsage(AttributeTargets.Method, Inherited = true, AllowMultiple = false)]
    public class RoutePutAttribute : RelayerAttribute {
        public RoutePutAttribute(string path) : base(path) {
            ArgumentTypes = new[] { typeof(IAGBase) };
        }

        public override HttpMethod Method => HttpMethod.Put;
    }
}
