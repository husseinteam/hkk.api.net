﻿using HKK.Core.Api.Controller;
using HKK.Core.Domain.Objects;
using HKK.Core.Engines;
using HKK.Core.Extensions.Static;
using Newtonsoft.Json;
using System;
using System.Linq;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using System.Web.Http;
using HKK.Core.Api.CoreModel.Models.Membership;
using HKK.Core.Api.CoreModel.Views.Membership;
using System.Globalization;
using HKK.Core.Domain.Contracts;
using HKK.Core.Api.CoreModel.Seeders;
using HKK.Core.Api.CoreModel.Controllers;
using NUnit.Framework;

namespace HKK.Core.UnitTest {
    
    public abstract class DataEngineTestBase {

        public abstract void Bootstrap();
        public abstract void RebuildDomain();

        protected async Task<TResponse> ParseResponse<TResponse>(IHttpActionResult result) {
            var response = await result.ExecuteAsync(new CancellationToken());
            var responseString = await response.Content.ReadAsStringAsync();
            return JsonConvert.DeserializeObject<TResponse>(responseString);
        }

        protected async Task MappedDomainFetched() {
            var session = new Session() { UserName = "25871077074", Password = "3132" };
            var eng = SDataEngine.GenerateAGEngine<UserView>();
            var azaResponse = (await eng.SelectSingle(a => a.UserName == session.UserName && a.UserPassword == session.Password));
            Assert.IsTrue(azaResponse.HasData);
            var azaView = azaResponse.Single;
            Assert.IsNotNull(azaView);
            var kunye = azaView.GetMappedDomain<Contact>();
            var aza = azaView.GetMappedDomain<User>();
            Assert.IsNotNull(kunye);
            Assert.IsNotNull(aza);
            Assert.AreNotEqual(0, kunye.ID);
            Assert.AreNotEqual(0, aza.ID);
        }



        protected async Task ControllerInsertsView() {
            var eng = SDataEngine.GenerateAGEngine<UserView>();
            var session = new Session() { UserName = "25871077074", Password = "3132" };

            var azaResponse = (await eng.SelectSingle(a => a.UserName == session.UserName && a.UserPassword == session.Password));
            Assert.IsTrue(azaResponse.HasData, azaResponse.Message);
            var azaView = azaResponse.Single;
            Assert.IsNotNull(azaView);

            var api = new AzaController();
            api.ControllerContext.Configuration = new HttpConfiguration();
            api.Request = new HttpRequestMessage();
            var aggResponse = await ParseResponse<HDSResponse>(await api.InsertAggregate(azaView));
            Assert.IsTrue(aggResponse.HasData, aggResponse.Message);
        }


        protected async Task ControllerDeletesView() {
            var eng = SDataEngine.GenerateAGEngine<UserView>();
            var azaView = new UserView {
                UserName= $"4043{7.RandomDigits()}",
                UserPassword = "3132",
                ContactFirstName = $"Dilek-{1.RandomWords()}",
                ConactLastName = $"{2.RandomWords()}",
                ContactPlaceOfBirth = 3.RandomWords(),
                ContactProfession = EProfession.HouseWoman,
                ContactDateOfBirth = DateTime.Parse("31.10.1981", CultureInfo.GetCultureInfo("tr-TR"))
            };
            var api = new AzaController();
            api.ControllerContext.Configuration = new HttpConfiguration();
            api.Request = new HttpRequestMessage();
            var aggResponse = await ParseResponse<HDSResponse>(await api.InsertAggregate(azaView));
            Assert.IsTrue(aggResponse.HasData, aggResponse.Message);
            var deleteResponse = await ParseResponse<HDSResponse>(await api.Delete(aggResponse.EntityID));
            Assert.IsTrue(deleteResponse.HasData, deleteResponse.Message);
        }


        protected async Task ControllerEditsView() {
            var session = new Session() { UserName = "25871077074", Password = "3132" };
            var eng = SDataEngine.GenerateAGEngine<UserView>();
            var azaResponse = (await eng.SelectSingle(a => a.UserName == session.UserName && a.UserPassword == session.Password));
            Assert.IsTrue(azaResponse.HasData, azaResponse.Message);
            var azaView = azaResponse.Single;
            Assert.IsNotNull(azaView);

            var api = new AzaController();
            api.ControllerContext.Configuration = new HttpConfiguration();
            api.Request = new HttpRequestMessage();
            azaView.ContactFirstName = 2.RandomWords();
            var aggResponse = await ParseResponse<HDSResponse>(await api.Edit(azaView));
            Assert.IsTrue(aggResponse.HasData, aggResponse.Message);
            var updatedResponse = (await eng.SelectSingle(a => a.UserName == "25871077074"));
            Assert.IsTrue(updatedResponse.HasData, updatedResponse.Message);
            Assert.AreEqual(azaView.ContactFirstName, updatedResponse.Single.ContactFirstName);
            var allResponse = (await eng.SelectAll());
            Assert.IsTrue(allResponse.HasData, allResponse.Message);
            Assert.AreEqual(1, allResponse.Items.Count(av => av.ContactFirstName == azaView.ContactFirstName), azaView.ContactFirstName);

        }



        protected async Task ColumnRequiredOrNot() {
            var session = new Session() { UserName = "25871077074", Password = "3132" };
            var eng = SDataEngine.GenerateAGEngine<UserView>();
            var azaResponse = (await eng.SelectSingle(a => a.UserName == session.UserName && a.UserPassword == session.Password));
            Assert.IsTrue(azaResponse.HasData);
            var azaView = azaResponse.Single;
            Assert.IsNotNull(azaView);
            Assert.IsTrue(azaView.IsColumnRequired(typeof(UserView).ResolveProperties().Single(p => p.Name == "AzaTCKN")));
        }


        protected async Task DomainReferenceKeysFetched() {
            var session = new Session() { UserName = "25871077074", Password = "3132" };
            var eng = SDataEngine.GenerateAGEngine<UserView>();
            var azaResponse = (await eng.SelectSingle(a => a.UserName == session.UserName && a.UserPassword == session.Password));
            Assert.IsTrue(azaResponse.HasData);
            var azaView = azaResponse.Single;
            Assert.IsNotNull(azaView);
            var aza = azaView.GetMappedDomain<User>();
            var kunyeReference = aza.GetRelationKeys().FirstOrDefault();
            Assert.IsNotNull(kunyeReference);
            Assert.AreEqual("KunyeID", kunyeReference.Name);
        }


        protected async Task OneFetched() {
            var session = new Session() { UserName = "25871077074", Password = "3132" };
            var eng = SDataEngine.GenerateDOEngine<User>();
            var azaResponse = (await eng.SelectSingle(aza => aza.UserName == session.UserName && aza.Password == session.Password));
            Assert.IsTrue(azaResponse.HasData);
            Assert.IsNotNull(azaResponse.Single);
        }

        
        public async Task OneFetchedByID() {
            var session = new Session() { UserName = "25871077074", Password = "3132" };
            var aza = (await SDataEngine.GenerateDOEngine<User>().SelectSingle(a => a.UserName == session.UserName && a.Password == session.Password))
                .Single;
            var eng = SDataEngine.GenerateAGEngine<UserView>();
            var existing = (await eng.SelectByID(aza.ID)).Single;
            Assert.IsNotNull(existing);
        }


        protected async Task OneOfManyFetchedBy() {
            var session = new Session() { UserName = "25871077074", Password = "3132" };
            var eng = SDataEngine.GenerateDOEngine<User>();
            var existing = (await eng.SelectSingle(aza => aza.UserName == session.UserName && aza.Password == session.Password)).SingleOrFirst;
            Assert.IsNotNull(existing);
        }


        protected async Task OneInserts() {
            var session = SAzaSeeder.GetAdminSession();
            await SAzaSeeder.NewUserWith(SAzaSeeder.GetAdminSession());
        }


        protected async Task BuildsAll() {
            RebuildDomain();
            var mudur = await SAzaSeeder.NewUserWith(SAzaSeeder.GetAdminSession());
            //foreach (var i in Enumerable.Range(0, 100)) {
            //    var aza = await SAzaSeeder.NewUserWith(11.RandomDigits(), 4.RandomDigits());
            //    Assert.AreNotEqual(aza.ID, 0);
            //}
        }


        protected async Task OneUpdated() {
            var session = new Session() { UserName = "25871077074", Password = "3132", Token = new ApiToken() { AccessToken = Guid.NewGuid().ToString("N") } };

            var eng = SDataEngine.GenerateDOEngine<User>();
            var response = (await eng.SelectSingle(aza => aza.UserName == session.UserName && aza.Password == session.Password));
            Assert.IsNotNull(response.Single);

            var updated = await eng.Update(response.EntityID, (modifier) => modifier
                .Update(a => a.AccessToken).Set(session.Token.AccessToken)
                .Update(a => a.AccessTokenExpiresOn).Set(DateTime.Now.AddMinutes(11))
                .UpdateThen<Contact, String>(obj => obj.FirstName).Set("Hüseyn").FromWhere<User>((a, b) => a.ID == b.ContactID)
                );
            Assert.IsTrue(updated.HasData);
            Assert.AreEqual(session.Token.AccessToken, updated.Single.AccessToken);
            Assert.AreEqual("Hüseyn", SDataEngine.GenerateDOEngine<Contact>()
                .SelectSingleByIDSync(updated.Single.KunyeID).Single.Isim);
        }


        protected async Task OneUpsertedAggregate() {
            var session = new Session() { UserName = "25871077074", Password = "3132" };
            var eng = SDataEngine.GenerateAGEngine<UserView>();
            var azaResponse = (await eng.SelectSingle(a => a.UserName == session.UserName && a.UserPassword == session.Password));
            Assert.IsTrue(azaResponse.HasData, azaResponse.Message);
            var azaView = azaResponse.Single;
            Assert.IsNotNull(azaView);
            azaView.UserActivationToken = "42342342980348309248023492";
            var dmlAgg = await SDataEngine.GenerateDOEngine<User>().UpdateAggregate(azaView);
            Assert.IsTrue(dmlAgg.HasData, dmlAgg.Message);
            var token =
                (await eng.SelectSingle(a => a.UserName == session.UserName && a.UserPassword == session.Password)).Single.UserActivationToken;
            Assert.AreEqual(azaView.UserActivationToken, token);
        }
        
    }

}
