﻿
using HKK.Core.Domain.Data;
using HKK.Core.Domain.Enums;
using HKK.Core.Domain.Objects;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Reflection;
using System.Text;

namespace HKK.Core.Domain.Query {

    public interface IQueryBuilder {

        IDataEngine Engine { get; }

        IQueryBuilder AddField(MemberInfo prop);
        IQueryBuilder RemoveField<TEntity>(MemberInfo prop)
            where TEntity : DOBase<TEntity>;
        IQueryBuilder AndConstraint(MemberInfo prop, object value, ExpressionType nodeType);
        IQueryBuilder OrConstraint(MemberInfo prop, object value, ExpressionType nodeType);
        IQueryBuilder OrderByField(MemberInfo prop, EOrderBy orderBy);

        StringBuilder BuildQuery();
        List<MemberInfo> Fields { get; }
    }

}