﻿

using HKK.Core.Domain.Data;
using HKK.Core.Domain.Enums;
using HKK.Core.Domain.Objects;
using HKK.Core.Extensions.Static;

using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Reflection;
using System.Text;

namespace HKK.Core.Domain.Query {

    class QueryBuilder : IQueryBuilder {

        public IDataEngine Engine { get; private set; } = SDbParams.DataTools.GenerateEngine();
        private StringBuilder _QueryString;
        private StringBuilder _FieldsString;
        private List<(MemberInfo prop, ExpressionType nodeType)> _AndConstraints =
            new List<(MemberInfo prop, ExpressionType nodeType)>();
        private List<(MemberInfo prop, ExpressionType nodeType)> _OrConstraints =
            new List<(MemberInfo prop, ExpressionType nodeType)>();
        private StringBuilder _OrderByString;
        private string paramKey = "";
        private string nameFormatter = "";

        public IDOSchemaBuilder SchemaBuilder { get; set; }

        private string FieldNameFromProperty(MemberInfo prop) {

            return "{0}, ".Puts(FieldNameFromPropertyPlain(prop));

        }
        private string FieldNameFromPropertyPlain(MemberInfo prop) {

            return "{0}.{1}".Puts(this.SchemaBuilder.GetFormatted(), nameFormatter.Puts(prop.Name));

        }

        public List<MemberInfo> Fields { get; private set; }

        public QueryBuilder(IDOSchemaBuilder schemaBuilder) {

            _QueryString = new StringBuilder("SELECT #fields# FROM #table# #where# #orderby#");
            _FieldsString = new StringBuilder("*");
            _OrderByString = new StringBuilder("ORDER BY ");
            Fields = new List<MemberInfo>();
            this.SchemaBuilder = schemaBuilder;
            switch (SDbParams.CurrentServerType) {
                case EServerType.MySql:
                    paramKey = "?";
                    nameFormatter = "`{0}`";
                    break;
                case EServerType.MSSql:
                    paramKey = "@";
                    nameFormatter = "[{0}]";
                    break;
                default:
                    throw new ArgumentException("Unsupported Server Type");
            }

        }

        public IQueryBuilder AddField(MemberInfo prop) {

            if (_FieldsString.ToString().Equals("*")) {
                _FieldsString.Clear();
                Fields.Clear();
            }
            if (!Fields.Contains(prop)) {
                _FieldsString.Append(FieldNameFromProperty(prop));
                Fields.Add(prop);
            }
            return this;

        }

        public IQueryBuilder RemoveField<TEntity>(MemberInfo prop)
            where TEntity : DOBase<TEntity> {

            if (Fields.Count == 0) {
                Fields.AddRange(Activator.CreateInstance<TEntity>().GetFields(EResolveBy.Assigned));
            }
            if (Fields.Contains(prop)) {
                _FieldsString.Replace(FieldNameFromProperty(prop), "");
                Fields.Remove(prop);
            }
            if (_FieldsString.ToString().Equals("")) {
                _FieldsString.Append("*");
            }
            return this;

        }

        public IQueryBuilder AndConstraint(MemberInfo prop, object value, ExpressionType nodeType) {
            _AndConstraints.Add((prop: prop, nodeType: nodeType));
            Engine.AddWithValue($"{paramKey}{prop.Name}", value);
            return this;

        }

        public IQueryBuilder OrConstraint(MemberInfo prop, object value, ExpressionType nodeType) {
            _OrConstraints.Add((prop: prop, nodeType: nodeType));
            Engine.AddWithValue($"{paramKey}{prop.Name}", value);
            return this;
        }

        public IQueryBuilder OrderByField(MemberInfo prop, EOrderBy orderBy) {

            _OrderByString.AppendFormat("{0} {1}, ", FieldNameFromPropertyPlain(prop), orderBy.GetDescription());
            return this;

        }

        public StringBuilder BuildQuery() {
            var constraintString = new StringBuilder("WHERE 1=1 ");
            foreach (var ac in _AndConstraints) {
                constraintString.AppendFormat(" AND {0}{2}{3}{1}", "{0}.{1}".Puts(this.SchemaBuilder.GetFormatted(),
                    nameFormatter.Puts(ac.prop.Name)), ac.prop.Name, SDbParams.DataTools.SqlExpression(ac.nodeType), paramKey);
            }
            foreach (var oc in _OrConstraints) {
                constraintString.AppendFormat(" OR {0}{2}{3}{1}", "{0}.{1}".Puts(this.SchemaBuilder.GetFormatted(),
                    nameFormatter.Puts(oc.prop.Name)), oc.prop.Name, SDbParams.DataTools.SqlExpression(oc.nodeType), paramKey);
            }
            _QueryString.Replace("#fields#", _FieldsString.ToString().TrimEnd(' ', ','))
                .Replace("#table#", this.SchemaBuilder.GetFormatted())
                .Replace("#where#", constraintString.ToString() == "WHERE 1=1 " ? "" : constraintString.ToString().TrimEnd(' ', ','))
                .Replace("#orderby#", _OrderByString.ToString() == "ORDER BY " ? "" : _OrderByString.ToString().TrimEnd(' ', ','));
            return _QueryString;
        }
    }

}