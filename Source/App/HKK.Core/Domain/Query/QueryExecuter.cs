﻿using HKK.Core.Domain.Data;
using HKK.Core.Domain.Enums;
using HKK.Core.Domain.Objects;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace HKK.Core.Domain.Query {

    public class QueryExecuter<TEntity> : IQueryExecuter<TEntity>
         where TEntity : DOBase<TEntity> {

        internal Exception Fault { get; private set; }
        private SelectList<TEntity> _SelectList;

        public QueryExecuter(SelectList<TEntity> selectList) {
            _SelectList = selectList;
            if (_SelectList.AllColumns) {
                _SelectList.QueryBuilder.Fields.AddRange(
                    Activator.CreateInstance<TEntity>().GetFields(EResolveBy.All));
            }
        }

        public async Task<IEnumerable<IExecutedQuery<TEntity>>> GenerateExecutedQueryList(Action noneback = null) {
            var list = new List<IExecutedQuery<TEntity>>();
            var commandText = _SelectList.QueryBuilder.BuildQuery().ToString();
            using (var engine = _SelectList.QueryBuilder.Engine) {
                using (var command = engine.AlterCommand(commandText)) {
                    using (var reader = await engine.ReadCommand()) {
                        var c = 0;
                        if (reader?.Read() == true) {
                            list.Add(new ExecutedQuery<TEntity>(reader,
                                    _SelectList.QueryBuilder.Fields, c++));
                            while (reader?.Read() == true) {
                                list.Add(new ExecutedQuery<TEntity>(reader,
                                    _SelectList.QueryBuilder.Fields, c++));
                            }
                        } else {
                            noneback?.Invoke();
                        }
                    }
                }
                Fault = engine.Fault;
            }

            return list.ToArray();
        }

        public async Task GenerateSingleExecutedQuery(Action<ExecutedQuery<TEntity>> doneback, Action noneback = null) {
            var commandText = _SelectList.QueryBuilder.BuildQuery().ToString();
            using (var engine = _SelectList.QueryBuilder.Engine) {
                engine.AlterCommand(commandText);
                using (var reader = await engine.ReadCommand()) {
                    if (reader?.Read() == true) {
                        doneback(new ExecutedQuery<TEntity>(reader,
                            _SelectList.QueryBuilder.Fields, 0));
                    } else {
                        noneback?.Invoke();
                    }
                }
                Fault = engine.Fault;
            }

        }
    }

}