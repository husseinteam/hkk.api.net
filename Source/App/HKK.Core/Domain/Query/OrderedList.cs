﻿using HKK.Core.Domain.Enums;
using HKK.Core.Domain.Objects;
using HKK.Core.Extensions.Static;
using System;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace HKK.Core.Domain.Query {

    public class OrderedList<TEntity> : IOrderedList<TEntity>
        where TEntity : DOBase<TEntity> {

        private SelectList<TEntity> _SelectList;

        public OrderedList(SelectList<TEntity> selectList) {
            this._SelectList = selectList;
        }

        public IOrderedList<TEntity> By<TProp>(Expression<Func<TEntity, TProp>> selector, EOrderBy orderBy) {

            var param = selector.Parameters[0] as ParameterExpression;
            var prop = selector.ResolveMember();
            this._SelectList.QueryBuilder.OrderByField(prop, orderBy);
            return this;

        }

        public async Task<Exception> ExecuteOne(Action<IExecutedQuery<TEntity>> cursor, Action fallback = null) {
            return await this._SelectList.ExecuteOne(cursor, fallback);
        }

        public async Task<Exception> ExecuteMany(Action<IExecutedQuery<TEntity>> cursor, Action noneback = null) {
            return await this._SelectList.ExecuteMany(cursor, noneback);
        }

        public Exception ExecuteOneSync(Action<IExecutedQuery<TEntity>> cursor, Action fallback = null) {
            return AsyncExtensions.AwaitResult(() => this._SelectList.ExecuteOne(cursor, fallback));

        }
        public Exception ExecuteManySync(Action<IExecutedQuery<TEntity>> cursor, Action noneback = null) {
            return AsyncExtensions.AwaitResult(() => this._SelectList.ExecuteMany(cursor, noneback));
        }

    }

}