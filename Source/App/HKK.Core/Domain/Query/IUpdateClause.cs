﻿using HKK.Core.Domain.Aggregate;
using HKK.Core.Domain.Objects;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Reflection;

namespace HKK.Core.Domain.Query {

    public interface IUpdateClause {

        IUpdateSetter<TModel> UpdateThen<TModel, TProp>(Expression<Func<TModel, TProp>> selector)
            where TModel : DOBase<TModel>;
        HDSResponse PersistUpdate();
    }

    public interface IUpdateClause<TEntity> : IUpdateClause
        where TEntity : DOBase<TEntity> {

        IUpdateSetter<TEntity> Update<TProp>(Expression<Func<TEntity, TProp>> selector);
        HDSResponse UpsertAggregate(IAGBase aggregate);

        IUpdateClause<TReferencingEntity> FromWhere<TReferencingEntity>(Expression<Func<TEntity, TReferencingEntity, bool>> selector)
            where TReferencingEntity : DOBase<TReferencingEntity>;
    }

}