﻿using HKK.Core.Domain.Aggregate;
using HKK.Core.Domain.Data;
using HKK.Core.Domain.Objects;
using HKK.Core.Extensions.Static;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Text;

namespace HKK.Core.Domain.Query {
    internal class UpdateClause<TEntity> : IUpdateClause<TEntity>
        where TEntity : DOBase<TEntity> {

        private IDataEngine _Engine;

        private IExecutedQuery executedQuery;
        private StringBuilder _UpdateString;
        private StringBuilder _ReferralString;

        public UpdateClause(IExecutedQuery executedQuery, string referralString = "", IDataEngine engine = null) {
            this.executedQuery = executedQuery;
            _ReferralString = new StringBuilder(referralString);
            _Engine = engine ?? SDbParams.DataTools.GenerateEngine();
        }

        public HDSResponse PersistUpdate() {
            SetUpdateCommandText();
            _Engine.AlterCommand(_UpdateString.ToString());
            var updated = this.executedQuery.ResolvedObject as IDOBase;
            updated.UpdatedAt = DateTime.Now;
            foreach (var kv in this.executedQuery.UpdatedValues) {
                try {
                    kv.Key.SetValue(updated, kv.Value);
                } catch { }
                var foreign = updated.GetKeys().SingleOrDefault(k => k.foreign.PropertyEquals(kv.Key));
                var referencing = updated.GetKeys().SingleOrDefault(k => k.referencing.PropertyEquals(kv.Key));
                _Engine.AddWithValue($"{SDbParams.ParamKey}{(referencing.foreign ?? kv.Key).ParentType().Breath<IDOBase>().SchemaBuilder.GetTableName()}_id",
                    referencing.foreign?.GetValue(updated) ?? DBNull.Value);
                _Engine.AddWithValue($"{SDbParams.ParamKey}{(foreign.referencing ?? kv.Key).ParentType().Breath<IDOBase>().SchemaBuilder.GetTableName()}_id",
                    foreign.foreign?.GetValue(updated) ?? DBNull.Value);
                if (kv.Key.Name != "ID" || kv.Key.Name != "InsertedAt" || kv.Key.Name != "UpdatedAt" || kv.Key.Name != "DeletedAt") {
                    _Engine.AddWithValue($"{SDbParams.ParamKey}{kv.Key.Name}", kv.Value);
                }
            }
            _Engine.AddWithValue($"{SDbParams.ParamKey}{typeof(TEntity).Breath<IDOBase>().SchemaBuilder.GetTableName()}_id", updated.ID);
            _Engine.AddWithValue($"{SDbParams.ParamKey}InsertedAt", updated.InsertedAt);
            _Engine.AddWithValue($"{SDbParams.ParamKey}UpdatedAt", updated.UpdatedAt);
            _Engine.AddWithValue($"{SDbParams.ParamKey}DeletedAt", updated.DeletedAt);

            try {
                _Engine.ExecuteCommandSync(out var rowc);
                return new HDSResponse(updated) { LogSource = SDbParams.LogSource() };
            } catch (Exception ex) {
                return new HDSResponse(ex) { LogSource = SDbParams.LogSource() };
            }
        }


        public HDSResponse UpsertAggregate(IAGBase aggregate) {
            Func<Type, Type> extractor = (t) => t.GetGenericArguments().FirstOrDefault() ?? t;
            var mappings = aggregate.Mappings.GroupBy(map => extractor(map.domain.ReflectedType));
            foreach (var group in mappings) {
                var domainType = extractor(group.Key);
                this.executedQuery.ExecuteMethod("Refer", new[] { domainType });
                foreach (var (dom, agg, _) in group) {
                    if (agg.IsAssigned(aggregate)) {
                        this.executedQuery.ExecuteMethod("AddUpdateParameter", new[] { domainType }, dom, agg.GetValue(aggregate));
                    }
                }
            }
            foreach (var group in mappings) {
                var domainType = extractor(group.Key);
                var clause = typeof(UpdateClause<>).MakeGenericType(domainType)
                       .GetConstructors().First()
                       .Invoke(new object[] { this.executedQuery, "", _Engine }) as IUpdateClause;
                var dml = clause.PersistUpdate();
                if (!dml.HasData) {
                    throw dml.Fault;
                }
            }
            return new HDSResponse(aggregate) { LogSource = SDbParams.LogSource() };
        }

        public IUpdateSetter<TModel> UpdateThen<TModel, TProp>(Expression<Func<TModel, TProp>> selector)
            where TModel : DOBase<TModel> {

            this.executedQuery.Refer<TModel>();
            return new UpdateSetter<TModel>(this.executedQuery, selector.ResolveMember());

        }

        public IUpdateSetter<TEntity> Update<TProp>(Expression<Func<TEntity, TProp>> selector) {

            return new UpdateSetter<TEntity>(this.executedQuery, selector.ResolveMember());

        }

        public IUpdateClause<TReferencingEntity> FromWhere<TReferencingEntity>(Expression<Func<TEntity, TReferencingEntity, bool>> selector)
            where TReferencingEntity : DOBase<TReferencingEntity> {
            if (selector is LambdaExpression) {
                var ps = (selector as LambdaExpression).Parameters;
                var pEntity = Activator.CreateInstance(ps[0].Type) as TEntity;
                var pReference = Activator.CreateInstance(ps[1].Type) as TReferencingEntity;
                var operand = SDbParams.DataTools.SqlExpression(((selector as LambdaExpression).Body as BinaryExpression).NodeType);

                var bexp = (selector as LambdaExpression).Body as BinaryExpression;
                var ePropName = SDbParams.NameFormatter.Puts(bexp.Left.ExposeMember().Name);
                var rPropName = SDbParams.NameFormatter.Puts(bexp.Right.ExposeMember().Name);
                this._ReferralString.AppendFormat("{0} AND ",
                    $"{pEntity.SchemaBuilder.GetFormatted()}.{ePropName} {operand} {pReference.SchemaBuilder.GetFormatted()}.{rPropName}");
                this.executedQuery.Refer<TEntity>();
            } else {
                throw new ArgumentException("Not a lambda expresion");
            }
            return new UpdateClause<TReferencingEntity>(this.executedQuery, this._ReferralString.ToString().TrimTrailing(" AND "), _Engine);
        }

        private void SetUpdateCommandText() {
            var referralString = "";
            switch (SDbParams.CurrentServerType) {
                case EServerType.MySql:
                    referralString = $" AND {this._ReferralString.ToString().TrimTrailing(" AND ")}".TrimTrailing(" AND ");
                    break;
                case EServerType.MSSql:
                    var entityName = typeof(TEntity).Breath<TEntity>().SchemaBuilder.GetFormatted();
                    var referredName = this.executedQuery.EntityTypes.First().Breath<TEntity>().SchemaBuilder.GetFormatted();
                    referralString = $"FROM {entityName} INNER JOIN {referredName} ON {this._ReferralString.ToString().TrimTrailing(" AND ")}";
                    break;
                default:
                    throw new NotSupportedException($"ServerKey: {SDbParams.CurrentServerType.ToString()} Not Supported");
            }

            var whereBuilder = new StringBuilder("WHERE 1=1");
            foreach (var entityType in this.executedQuery.EntityTypes) {
                var schemaBuilder = entityType.Breath<IDOBase>().SchemaBuilder;
                whereBuilder.AppendFormat(" AND {0}.ID={1}{2}_id", schemaBuilder.GetFormatted(), SDbParams.ParamKey, schemaBuilder.GetTableName());
            }

            _UpdateString = new StringBuilder("UPDATE{0}#table# SET #sets# #where# #from#"
                .Puts(SDbParams.CurrentServerType == EServerType.MySql ? " IGNORE " : " "));
            _UpdateString
                .Replace("#table#", this.executedQuery.EntityTypes
                    .Select(e => e.Breath<IDOBase>()).Select(et => et.SchemaBuilder.GetFormatted()).Aggregate((a, b) => $"{a}, {b}"))
                .Replace("#sets#", this.executedQuery.SetClause)
                .Replace("#where#", whereBuilder.ToString());
            _UpdateString.Replace("#from#", this._ReferralString.Length > 0 ? referralString : "");

        }

    }
}