﻿using HKK.Core.Domain.Objects;
using System;
using System.Collections.Generic;
using System.Reflection;

namespace HKK.Core.Domain.Query {

    public interface IExecutedQuery {

        int Index { get; }
        object ResolvedObject { get; }
        string SetClause { get; }
        IExecutedQuery Refer<TFromEntity>()
            where TFromEntity : DOBase<TFromEntity>;
        IEnumerable<Type> EntityTypes { get; }
        IEnumerable<object> Entities { get; }
        IEnumerable<KeyValuePair<MemberInfo, object>> UpdatedValues { get; }

        void AddUpdateParameter<TReferenceEntity>(MemberInfo prop, object value)
            where TReferenceEntity : DOBase<TReferenceEntity>;
    }

    public interface IExecutedQuery<TEntity> : IExecutedQuery
        where TEntity : DOBase<TEntity> {

        TEntity ResolvedEntity { get; }
        IUpdateClause<TEntity> UpdateClause();
        HDSResponse Delete();


    }

}