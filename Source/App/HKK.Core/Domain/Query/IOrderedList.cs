﻿using HKK.Core.Domain.Enums;
using HKK.Core.Domain.Objects;
using System;
using System.Linq.Expressions;

namespace HKK.Core.Domain.Query {

    public interface IOrderedList<TEntity> : ICursorExecuter<TEntity>
        where TEntity : DOBase<TEntity> {

        IOrderedList<TEntity> By<TProp>(Expression<Func<TEntity, TProp>> selector, EOrderBy orderBy);

    }

}
