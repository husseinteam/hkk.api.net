﻿using HKK.Core.Domain.Objects;
using System;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace HKK.Core.Domain.Query {
    public interface IConfinedList<TEntity> : ICursorExecuter<TEntity>
        where TEntity : DOBase<TEntity> {

        IConfinedList<TEntity> And(Expression<Func<TEntity, bool>> confineClause);
        IConfinedList<TEntity> Or(Expression<Func<TEntity, bool>> confineClause);
        IOrderedList<TEntity> Order();
    }
}