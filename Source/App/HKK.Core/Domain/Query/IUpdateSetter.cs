﻿
using HKK.Core.Domain.Objects;

namespace HKK.Core.Domain.Query {

    public interface IUpdateSetter<TEntity>
        where TEntity : DOBase<TEntity> {

        IUpdateClause<TEntity> Set<TProp>(TProp value);

    }

}