﻿
using HKK.Core.Domain.Objects;
using System;
using System.Threading.Tasks;

namespace HKK.Core.Domain.Query {

    public interface ICursorExecuter<TEntity>
         where TEntity : DOBase<TEntity> {

        Task<Exception> ExecuteOne(Action<IExecutedQuery<TEntity>> cursor, Action noneback = null);
        Task<Exception> ExecuteMany(Action<IExecutedQuery<TEntity>> cursor, Action noneback = null);
        Exception ExecuteOneSync(Action<IExecutedQuery<TEntity>> cursor, Action noneback = null);
        Exception ExecuteManySync(Action<IExecutedQuery<TEntity>> cursor, Action noneback = null);

    }

}