﻿
using HKK.Core.Domain.Data;
using HKK.Core.Domain.Enums;
using HKK.Core.Domain.Objects;
using HKK.Core.Extensions.Static;

using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Reflection;
using System.Text;

namespace HKK.Core.Domain.Query {

    public class ExecutedQuery<TEntity> : IExecutedQuery<TEntity>
        where TEntity : DOBase<TEntity> {

        internal Dictionary<Type, IEnumerable<MemberInfo>> EntityFields = new Dictionary<Type, IEnumerable<MemberInfo>>();
        internal List<Tuple<Type, MemberInfo, object>> UpdateParameters => _UpdateParameters;

        private IDataReader reader;
        private TEntity entity;
        private readonly List<Tuple<Type, MemberInfo, object>> _UpdateParameters = new List<Tuple<Type, MemberInfo, object>>();

        public TEntity ResolvedEntity => entity;
        public int Index { get; private set; }
        public object ResolvedObject => ResolvedEntity;
        public IEnumerable<Type> EntityTypes => EntityFields.Keys;
        public IEnumerable<Object> Entities => EntityFields.Keys.Select(t => Activator.CreateInstance(t));

        public string SetClause => UpdateParameters.Where(up => up.Item2.Name != "ID").Select(up => "{1}.{0}={2}{0}".Puts(up.Item2.Name,
            (Activator.CreateInstance(up.Item1) as IDOBase).SchemaBuilder.GetFormatted(), SDbParams.ParamKey)).
        Aggregate((prev, next) => $"{prev}, {next}");

        public IEnumerable<KeyValuePair<MemberInfo, object>> UpdatedValues => UpdateParameters.Select(up =>
                    new KeyValuePair<MemberInfo, object>(up.Item2, Convert.ChangeType(up.Item3, up.Item2.GetMemberType())));

        public ExecutedQuery(IDataReader reader, IList<MemberInfo> fields, int index) {
            this.reader = reader;
            this.Index = Index;
            ParseEntity(fields);
            this.EntityFields[typeof(TEntity)] = fields;
        }

        public IExecutedQuery Refer<TFromEntity>()
            where TFromEntity : DOBase<TFromEntity> {
            this.EntityFields[typeof(TFromEntity)] = Activator.CreateInstance<TFromEntity>().GetFields(EResolveBy.Assigned);
            return this;
        }

        private void ParseEntity(IEnumerable<MemberInfo> entityFields) {

            entity = Activator.CreateInstance<TEntity>();
            foreach (var ef in entityFields) {
                if (this.reader.HasColumn(ef.Name)) {
                    var value = this.reader[ef.Name];
                    if (value == null || value == DBNull.Value) {

                    } else if (ef.GetMemberType().IsEnum) {
                        ef.SetValue(entity, Convert.ChangeType(Enum.ToObject(ef.GetMemberType(), value), ef.GetMemberType()));
                    } else if (ef.GetMemberType().GetInterfaces().Any(inf => inf.Equals(typeof(IConvertible)))) {
                        ef.SetValue(entity, Convert.ChangeType(value, ef.GetMemberType()));
                    } else {
                        ef.SetValue(entity, value);
                    }
                }
            }

        }

        public IUpdateClause<TEntity> UpdateClause() {

            return new UpdateClause<TEntity>(this);

        }

        public void AddUpdateParameter<TReferenceEntity>(MemberInfo prop, object value)
            where TReferenceEntity : DOBase<TReferenceEntity> {
            UpdateParameters.Add(Tuple.Create(typeof(TReferenceEntity), prop, value));
        }

        public HDSResponse Delete() {
            var deleteClause = new StringBuilder("DELETE FROM #table# WHERE #selector#");
            var pkli = (entity as IDOBase).GetFields(EResolveBy.PKs);
            var selectors = pkli.Select(p => "{0}={1}{0}".Puts(p.Name, SDbParams.ParamKey))
                .Aggregate((p, n) => "{0} AND {1}".Puts(p, n));
            deleteClause
                .Replace("#table#", (entity as IDOBase).SchemaBuilder.GetFormatted())
                .Replace("#selector#", selectors);
            using (var engine = SDbParams.DataTools.GenerateEngine().AlterCommand(deleteClause.ToString())) {
                this.entity.DeletedAt = DateTime.Now;
                foreach (var id in pkli) {
                    engine.AddWithValue($"{SDbParams.ParamKey}{id.Name}", id.GetValue(this.entity));
                }
                var response = new HDSResponse(entity) {
                    LogSource = SDbParams.LogSource()
                };
                try {
                    engine.ExecuteCommandSync(out var rowc);
                } catch (Exception ex) {
                    response.ReplaceFault(ex);
                }
                return response;
            }
        }
    }
}