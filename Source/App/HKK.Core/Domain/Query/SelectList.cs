﻿using HKK.Core.Domain.Objects;
using HKK.Core.Extensions.Static;
using System;
using System.Linq.Expressions;
using System.Reflection;
using System.Threading.Tasks;

namespace HKK.Core.Domain.Query {

    public class SelectList<TEntity> : ISelectList<TEntity>
        where TEntity : DOBase<TEntity> {

        private readonly IQueryBuilder _QueryBuilder;
        private IConfinedList<TEntity> _ConfinedList;
        private IOrderedList<TEntity> _OrderedList;
        private bool allColumns;

        internal QueryExecuter<TEntity> QueryExecuter { get; private set; }


        internal IQueryBuilder QueryBuilder {
            get {
                return _QueryBuilder;
            }
        }

        public bool AllColumns {
            get {
                return allColumns;
            }
        }

        public SelectList(bool allColumns = false) {
            this.allColumns = allColumns;
            this._QueryBuilder = new QueryBuilder((Activator.CreateInstance<TEntity>() as IDOBase).SchemaBuilder);
        }

        public ISelectList<TEntity> Add<TProp>(Expression<Func<TEntity, TProp>> columnSelector) {

            var prop = columnSelector.ResolveMember();
            this.QueryBuilder.AddField(prop);
            return this;

        }

        public ISelectList<TEntity> Remove<TProp>(Expression<Func<TEntity, TProp>> columnSelector) {

            var prop = columnSelector.ResolveMember();
            this.QueryBuilder.RemoveField<TEntity>(prop);
            return this;

        }

        public IConfinedList<TEntity> Where(Expression<Func<TEntity, bool>> columnSelector) {

            return (_ConfinedList = _ConfinedList ?? new ConfinedList<TEntity>(this).And(columnSelector));

        }

        public IConfinedList<TEntity> WhereDynamic(MemberInfo targetProp, object targetValue) {
            QueryBuilder.AndConstraint(targetProp, targetValue, ExpressionType.Equal);
            return (_ConfinedList = _ConfinedList ?? new ConfinedList<TEntity>(this));
        }

        public IOrderedList<TEntity> Order() {

            return (_OrderedList = _OrderedList ?? new OrderedList<TEntity>(this));

        }

        public async Task<Exception> ExecuteOne(Action<IExecutedQuery<TEntity>> cursor, Action noneback = null) {

            QueryExecuter = QueryExecuter ?? new QueryExecuter<TEntity>(this);
            await QueryExecuter.GenerateSingleExecutedQuery(cursor, noneback);
            return QueryExecuter.Fault;

        }

        public async Task<Exception> ExecuteMany(Action<IExecutedQuery<TEntity>> cursor, Action noneback = null) {

            QueryExecuter = QueryExecuter ?? new QueryExecuter<TEntity>(this);
            foreach (var eq in await QueryExecuter.GenerateExecutedQueryList(noneback)) {
                cursor(eq);
            }
            return QueryExecuter.Fault;

        }

        public Exception ExecuteOneSync(Action<IExecutedQuery<TEntity>> cursor, Action noneback = null) {
            QueryExecuter = QueryExecuter ?? new QueryExecuter<TEntity>(this);
            AsyncExtensions.AwaitResult(() => QueryExecuter.GenerateSingleExecutedQuery(cursor, noneback));
            return QueryExecuter.Fault;
        }
        public Exception ExecuteManySync(Action<IExecutedQuery<TEntity>> cursor, Action noneback = null) {
            QueryExecuter = QueryExecuter ?? new QueryExecuter<TEntity>(this);
            AsyncExtensions.AwaitResult(async () => {
                foreach (var eq in await QueryExecuter.GenerateExecutedQueryList(noneback)) {
                    cursor(eq);
                }
            });
            return QueryExecuter.Fault;
        }
    }

}