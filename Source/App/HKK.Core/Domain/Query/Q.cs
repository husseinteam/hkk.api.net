﻿using HKK.Core.Domain.Objects;

namespace HKK.Core.Domain.Query {

    public static class Q<TEntity> 
        where TEntity : DOBase<TEntity> {

        public static ISelectList<TEntity> SelectAllColumns(bool off = false) {

            return new SelectList<TEntity>(!off);

        }

    }

}
