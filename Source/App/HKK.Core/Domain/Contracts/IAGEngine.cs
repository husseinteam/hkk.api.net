﻿using HKK.Core.Domain.Aggregate;
using HKK.Core.Domain.Objects;
using System;
using System.Linq.Expressions;
using System.Reflection;
using System.Threading.Tasks;

namespace HKK.Core.Domain.Contracts {

    public interface IAGEngine<TAggregate>
            where TAggregate : AGBase<TAggregate> {
        Task<HDSResponse> SelectAll(Func<TAggregate, bool> filter = null);
        Task<HDSResponse> SelectBy(params Expression<Func<TAggregate, bool>>[] selectors);
        Task<HDSResponse> SelectSingle(Expression<Func<TAggregate, bool>> selector);
        Task<HDSResponse> SelectByID(long id);
        Task<HDSResponse> SelectByOne(Expression<Func<TAggregate, bool>> selector);

        long SelectCount();
        HDSResponse SelectAllSync(Func<TAggregate, bool> filter = null);
        HDSResponse SelectBySync(params Expression<Func<TAggregate, bool>>[] selectors);
        HDSResponse SelectByOneSync(Expression<Func<TAggregate, bool>> selector);
        HDSResponse SelectSingleSync(Expression<Func<TAggregate, bool>> selector);
        HDSResponse SelectByIDSync(long id);
    }

}
