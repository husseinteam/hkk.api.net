﻿using HKK.Core.Domain.Aggregate;
using System;
using System.Threading.Tasks;
using System.Reflection;

namespace HKK.Core.Domain.Contracts {
    public interface IGenericEngine {
        Task<IHDSResponse> GenericSelectAll(Func<dynamic, bool> filter = null);
        Task<IHDSResponse> GenericSelectByID(long id);

        IHDSResponse GenericSelectAllSync();
        IHDSResponse GenericSelectByIDSync(long id);
        Task<IHDSResponse> GenericDelete(long id);
        Task<IHDSResponse> GenericUpdateAggregate(IAGBase aggregate);
        IHDSResponse GenericSelectByComparison(MemberInfo targetProp, object targetValue);
        IHDSResponse GenericSelectAllByReferenceSync(long id, string referencedColumn);
    }

}
