﻿using HKK.Core.Domain.Aggregate;
using System;
using System.Collections.Generic;

namespace HKK.Core.Domain.Contracts {

    public interface IHDSResponse {

        IHDSResponse ReplaceItems(Func<object, object> replacer);
        IHDSResponse ReplaceSingle(Func<object, object> replacer);
        IHDSResponse ReplaceFault(Exception fault);

        Exception Fault { get; }
        string Message { get; }
        long EntityID { get; set; }
        bool HasData { get; }
        bool HasErrors { get; }

        bool ContainsSingleOut<T>(out T item)
            where T : class;

        List<dynamic> Items { get; }
        dynamic Single { get; }
        dynamic SingleOrFirst { get; }

        Type TypeOfItem();

    }

}
