﻿using HKK.Core.Domain.Aggregate;
using HKK.Core.Domain.Objects;
using HKK.Core.Domain.Query;
using System;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace HKK.Core.Domain.Contracts {
    public interface IDOEngine<TEntity>
        where TEntity : DOBase<TEntity> {

        Task<HDSResponse> InsertIfNotExists(TEntity entity);
        Task<IHDSResponse> Update(long id, Func<IUpdateClause<TEntity>, IUpdateClause<TEntity>> updater);
        Task<IHDSResponse> UpdateAggregate(IAGBase aggregate);
        Task<HDSResponse> Delete<TKey>(TKey key);

        Task<HDSResponse> SelectAll(Func<TEntity, bool> filter = null);
        Task<HDSResponse> SelectSingleByID<TKey>(TKey key);
        Task<HDSResponse> SelectLastRecord<TKey>(Expression<Func<TEntity, TKey>> keySelector);
        Task<HDSResponse> SelectSingle(Expression<Func<TEntity, bool>> selector);

        HDSResponse SelectAllSync();
        HDSResponse SelectBySync(params Expression<Func<TEntity, bool>>[] selectors);
        HDSResponse SelectSingleSync(Expression<Func<TEntity, bool>> selector);
        HDSResponse SelectLastRecordSync<TKey>(Expression<Func<TEntity, TKey>> keySelector);
        HDSResponse SelectSingleByIDSync<TKey>(TKey key);

        HDSResponse InsertSync(TEntity entity);
        HDSResponse UpdateSync(long id, Func<IUpdateClause<TEntity>, IUpdateClause<TEntity>> updater);
        IHDSResponse UpdateAggregateSync(IAGBase aggregate);
        HDSResponse DeleteSync<TKey>(TKey key);

    }

}
