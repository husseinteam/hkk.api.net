﻿using System.ComponentModel;

namespace HKK.Core.Domain.Enums {
    public enum EOrderBy {
        [Description("DESC")]
        DESC,
        [Description("ASC")]
        ASC
    }
}
