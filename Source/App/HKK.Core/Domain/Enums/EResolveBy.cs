﻿namespace HKK.Core.Domain.Enums {
    public enum EResolveBy {
        Assigned,
        All,
        Required,
        PKs,
        UQs
    }
}
