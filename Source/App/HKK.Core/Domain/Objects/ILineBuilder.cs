﻿
namespace HKK.Core.Domain.Objects {

    public interface ILineBuilder {

        string Build();

    }

}
