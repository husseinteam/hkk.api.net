﻿using HKK.Core.Domain.Data;
using HKK.Core.Extensions.Static;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;

namespace HKK.Core.Domain.Objects {

    internal class DOTableBuilder<TDomain> : DOConstraintBuilder<TDomain>, IDOTableBuilder<TDomain>
        where TDomain : DOBase<TDomain> {

        public IDOSchemaBuilder SchemaBuilder {
            get {
                return base._SchemaBuilder;
            }
        }

        private List<MemberInfo> requiredFields = new List<MemberInfo>();
        private List<ILineBuilder> uniqueKeyBuilders = new List<ILineBuilder>();
        private List<ILineBuilder> primaryKeyBuilders = new List<ILineBuilder>();

        public IEnumerable<MemberInfo> RequiredFields() {
            return ConstraintPropBuilders.Where(pb => (pb as DOPropBuilder).isRequired).Select(pb => (pb as DOPropBuilder).Prop).ToArray();
        }

        public IEnumerable<DOPropBuilder> PropBuilders() {
            return ConstraintPropBuilders.Select(pb => (pb as DOPropBuilder)).ToArray();
        }

        public IEnumerable<MemberInfo> PKFields() {
            var li = new List<MemberInfo>();
            foreach (var pkb in primaryKeyBuilders) {
                li.AddRange((pkb as DOPrimaryKeyBuilder).PrimaryKeyProps);
            }
            return li.ToArray();
        }

        public IEnumerable<MemberInfo> RelationKeys() {
            foreach (var rel in RelationBuilders) {
                foreach (var key in (rel as DORelationBuilder).ForeignKeys
                    .Where(k => typeof(TDomain).IsAssignableFrom(k.ReflectedType))) {
                    yield return key;
                }
            }
        }

        public IEnumerable<DOPropBuilder> UniqueKeys() {
            foreach (var rel in uniqueKeyBuilders) {
                foreach (var key in (rel as DOUniqueKeyBuilder).UniqueKeyProps) {
                    yield return key;
                }
            }
        }

        public IEnumerable<MemberInfo> ReferencingKeys() {
            foreach (var rel in RelationBuilders) {
                foreach (var key in (rel as DORelationBuilder).ReferencedKeys) {
                    yield return key;
                }
            }
        }

        public DOPropBuilder For<TProp>(Expression<Func<TDomain, TProp>> selector) {

            var field = selector.ResolveMember();
            base.ConstraintFields.Add(field);
            var propBuilder = new DOPropBuilder(field);
            ConstraintPropBuilders.Add(propBuilder);
            return propBuilder;

        }

        public void UniqueKey<TProp>(params Expression<Func<TDomain, TProp>>[] selectors) {

            var builders = new List<ILineBuilder>();
            selectors.Select((Expression<Func<TDomain, TProp>> s) => s.ResolveMember()).ToList().ForEach(s => {
                if (!base.ConstraintFields.Contains(s)) {
                    builders.Add(new DOPropBuilder(s).IsTypeOf(typeof(TProp).GetDataType()).IsRequired());
                    base.ConstraintFields.Add(s);
                }
            });
            ConstraintPropBuilders.AddRange(builders);
            var uniqueKeyBuilder = new DOUniqueKeyBuilder(SchemaBuilder, selectors.Select(s => s.ResolveMember())
                .Where(prop => ConstraintPropBuilders.SingleOrDefault(b => (b as DOPropBuilder).Prop.Equals(prop)) != null)
                .Select(prop => ConstraintPropBuilders.Single(b => (b as DOPropBuilder).Prop.Equals(prop)) as DOPropBuilder)
                );
            uniqueKeyBuilders.Add(uniqueKeyBuilder);

        }

        public void PrimaryKey<TProp>(params Expression<Func<TDomain, TProp>>[] selectors) {

            var primaryKeyBuilder = new DOPrimaryKeyBuilder(SchemaBuilder, selectors.Select(s => s.ResolveMember()));
            primaryKeyBuilders.Add(primaryKeyBuilder);

        }

        public void MapsTo(Action<IDOSchemaBuilder> schematizer) {

            schematizer(this.SchemaBuilder);

        }

        public void BuildTable() {

            var tableAndSchema = this.SchemaBuilder.Build();
            var columns = this.ConstraintPropBuilders.Select(pb => pb.Build()).Aggregate((prev, next) => "{0}, {1}".Puts(prev, next));
            var script = "CREATE TABLE {0}( {1} );".Puts(tableAndSchema, columns);
            using (var engine = SDbParams.DataTools.GenerateEngine().AlterCommand(script)) {
                engine.SwitchLoggingOff().ExecuteCommandSync(out var rowc);
                if (primaryKeyBuilders.Count > 0 && SDbParams.CurrentServerType != EServerType.MySql) {
                    foreach (var builder in primaryKeyBuilders) {
                        var pkscript = "ALTER TABLE {0} ADD {1};".Puts(tableAndSchema, builder.Build());
                        engine.AlterCommand(pkscript).ExecuteCommandSync(out var rowc2);
                    }
                }
            }
        }

        public void BuildConstraints() {

            var tableAndSchema = this.SchemaBuilder.Build();
            var builders = RelationBuilders.Union(uniqueKeyBuilders);
            using (var engine = SDbParams.DataTools.GenerateEngine()) {
                foreach (var builder in builders) {
                    var builded = builder.Build();
                    if (!string.IsNullOrEmpty(builded)) {
                        var script = "ALTER TABLE {0} ADD {1};".Puts(tableAndSchema, builded);
                        engine.SwitchLoggingOff().AlterCommand(script).ExecuteCommandSync(out var rowc);
                    }
                }
            }

        }
        public IDOTableBuilder<TDomain> CutOff() {
            var tableAndSchema = this.SchemaBuilder.Build();
            var script = "";
            switch (SDbParams.CurrentServerType) {
                case EServerType.MySql:
                    script = @"
SET FOREIGN_KEY_CHECKS = 0;
DROP TABLE IF EXISTS {0};
SET FOREIGN_KEY_CHECKS = 1;
".Puts(tableAndSchema);
                    break;
                case EServerType.MSSql:
                    script = @"
if exists (select * from sysobjects where name='{0}' and xtype='U')
begin
DECLARE @sql varchar(800)
DECLARE alter_cursor CURSOR
	FOR SELECT 
	'ALTER TABLE ' +  OBJECT_SCHEMA_NAME(parent_object_id) +
	'.[' + OBJECT_NAME(parent_object_id) + 
	'] DROP CONSTRAINT ' + name
FROM sys.foreign_keys
WHERE referenced_object_id = object_id('{1}')
OPEN alter_cursor
FETCH NEXT FROM alter_cursor INTO @sql
WHILE @@FETCH_STATUS = 0  
BEGIN  
	EXEC (@sql)
	FETCH NEXT FROM alter_cursor INTO @sql
END

CLOSE alter_cursor
DEALLOCATE alter_cursor

DROP TABLE {1}
end
".Puts(this.SchemaBuilder.GetTableName(), tableAndSchema);
                    break;
                default:
                    throw new ArgumentException("Unsupported Server Type");
            }

            using (var engine = SDbParams.DataTools.GenerateEngine().AlterCommand(script)) {
                engine.ExecuteCommandSync(out var rowc);
            }
            return this;

        }
    }

}