﻿

using HKK.Core.Domain.Data;
using HKK.Core.Engines;
using HKK.Core.Extensions.Static;

using System;
using System.Collections.Generic;
using System.Linq;

namespace HKK.Core.Domain.Objects {

    public class DOSchemaBuilder : IDOSchemaBuilder {

        private string schemaName;
        private string tableName;
        private static Dictionary<string, int> AliasIndex { get; } = new Dictionary<string, int>();

        private static Lazy<Dictionary<string, IList<string>>> TableAliasDict { get; } = new Lazy<Dictionary<string, IList<string>>>(() => generateTableAliases());

        private static Dictionary<string, IList<string>> generateTableAliases() {
            var dict = new Dictionary<string, IList<string>>();
            SDataEngine.GeneratedDomains.ForEach(e => {
                var name = $"hkk{Guid.NewGuid().ToString("N")}";
                dict[e.SchemaBuilder.GetTableName()] = new List<string>();
                dict[e.SchemaBuilder.GetTableName()].Add(SDbParams.NameFormatter.Puts(name));
                AliasIndex[e.SchemaBuilder.GetTableName()] = 0;
            });
            return dict;
        }

        public string FormatsBy(string format) {
            return format.Puts(this.schemaName, this.tableName);
        }

        public IDOSchemaBuilder SchemaName(string schemaName) {

            this.schemaName = SDbParams.CurrentServerType == EServerType.MySql ? schemaName.ToLower() : schemaName;
            return this;

        }

        public IDOSchemaBuilder TableName(string tableName) {

            this.tableName = SDbParams.CurrentServerType == EServerType.MySql ? tableName.ToLower() : tableName;
            return this;

        }
        public string GetTableName() {
            return this.tableName;
        }

        public string GetFormatted() {
            switch (SDbParams.CurrentServerType) {
                case EServerType.MySql:
                    return "`{0}_{1}`".Puts(this.schemaName, this.tableName);
                case EServerType.MSSql:
                    return "[{0}].[{1}]".Puts(this.schemaName, this.tableName);
                default:
                    throw new NotImplementedException("Unsupported keyType");
            }
        }

        public string GetAlias() {
            return TableAliasDict.Value[tableName].ElementAt(AliasIndex[tableName]);
        }

        public void RefreshAlias() {
            var name = $"hkk{Guid.NewGuid().ToString("N")}";
            TableAliasDict.Value[tableName].Add(SDbParams.NameFormatter.Puts(name));
            AliasIndex[tableName] = AliasIndex[tableName] + 1;
        }

        public string Build() {

            switch (SDbParams.CurrentServerType) {
                case EServerType.MySql:
                    return GetFormatted();
                case EServerType.MSSql:
                    var script = @"
IF NOT EXISTS (SELECT schema_name 
    FROM INFORMATION_SCHEMA.SCHEMATA 
    WHERE schema_name = '{0}' )
BEGIN
    EXEC sp_executesql N'CREATE SCHEMA {0};';
END";
                    using (var engine = SDbParams.DataTools.GenerateEngine().AlterCommand(script.Puts(schemaName))) {
                        engine.SwitchLoggingOff().ExecuteCommandSync(out var rowc);
                    }
                    return GetFormatted();
                default:
                    throw new NotImplementedException("Unsupported keyType");
            }

        }

    }

}