﻿using HKK.Core.Api.CoreModel.Models.Data;
using HKK.Core.Domain.Aggregate;
using HKK.Core.Domain.Contracts;
using HKK.Core.Engines;
using HKK.Core.Extensions.Static;
using Newtonsoft.Json;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace HKK.Core.Domain.Objects {

    public class HDSResponse : IHDSResponse {

        public HDSResponse(IEnumerable items) {
            foreach (var item in items) {
                Items.Add(item);
            }
        }

        public HDSResponse(dynamic single) {
            Single = single;
        }

        public HDSResponse(Exception fault) {
            Fault = fault;
        }

        [JsonIgnore]
        public long EntityID {
            get {
                if (SingleOrFirst != null) {
                    if (typeof(IDOBase).IsAssignableFrom(SingleOrFirst.GetType())) {
                        return (SingleOrFirst as IDOBase).ID;
                    } else if (typeof(IAGBase).IsAssignableFrom(SingleOrFirst.GetType())) {
                        return (SingleOrFirst as IAGBase).ID;
                    } else {
                        throw new ArgumentException(
                            $"EntityID.get called on {SingleOrFirst.GetType().ToString()} which should have been of DOBase<> or AGBase<>");
                    }
                } else {
                    return 0;
                }
            }
            set {
                if (SingleOrFirst != null) {
                    if (typeof(IDOBase).IsAssignableFrom(SingleOrFirst.GetType())) {
                        (SingleOrFirst as IDOBase).ID = value;
                    } else if (typeof(IAGBase).IsAssignableFrom(SingleOrFirst.GetType())) {
                        (SingleOrFirst as IAGBase).ID = value;
                    } else {
                        throw new ArgumentException(
                            $"EntityID.set({value}) called on {SingleOrFirst.GetType().Name} which doesnt subclass DOBase<> or AGBase<>");
                    }
                }
            }
        }

        public bool ContainsSingleOut<T>(out T item)
            where T : class {
            item = HasData ? this.Single as T : null;
            return HasData;
        }

        public Type TypeOfItem() {
            object item = SingleOrFirst;
            return item.GetType();
        }

        [JsonIgnore]
        public Exception Fault { get; internal set; }

        public bool HasErrors => Fault != null;

        public bool HasData => SingleOrFirst != null && HasErrors == false;

        [JsonIgnore]
        public string LogSource { private get; set; }
       
        public IHDSResponse ReplaceFault(Exception fault) {
            this.Fault = fault;
            return this;
        }

        public IHDSResponse ReplaceItems(Func<object, object> replacer) {
            for (int i = 0; i < Items.Count(); i++) {
                Items[i] = replacer(Items.ElementAt(i));
            }
            return this;
        }

        public IHDSResponse ReplaceSingle(Func<object, object> replacer) {
            if (Single != null) {
                Single = replacer(Single);
            }
            return this;
        }

        public List<dynamic> Items { get; internal set; } = new List<dynamic>();
        public dynamic Single { get; internal set; }

        [JsonIgnore]
        public dynamic SingleOrFirst {
            get => Single != null ? Single : Items?.FirstOrDefault();
        }

        public string Message { get => $"{Fault?.Message ?? LogSource}"; }
    }
}
