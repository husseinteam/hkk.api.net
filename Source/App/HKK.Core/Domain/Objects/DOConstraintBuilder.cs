﻿using HKK.Core.Extensions.Static;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;

namespace HKK.Core.Domain.Objects {

    internal class DOConstraintBuilder<TEntity> : IDOConstraintBuilder<TEntity>
        where TEntity : DOBase<TEntity> {

        public IList<MemberInfo> ConstraintFields { get; private set; } = new List<MemberInfo>();

        protected List<ILineBuilder> ConstraintPropBuilders { get; private set; } = new List<ILineBuilder>();
        protected IDOSchemaBuilder _SchemaBuilder { get; private set; } = new DOSchemaBuilder();
        protected List<ILineBuilder> RelationBuilders { get; private set; } = new List<ILineBuilder>();


        public DORelationBuilder ForeignKey<TKey>(params Expression<Func<TEntity, TKey>>[] selectors) {

            selectors.Select(s => s.ResolveMember()).ToList().ForEach(s => {
                if (!ConstraintFields.Contains(s)) {
                    var prop = new DOPropBuilder(s).IsTypeOf(typeof(TKey).GetDataType());
                    if (Nullable.GetUnderlyingType(typeof(TKey)) != null) {
                        prop.IsNullable();
                    }
                    ConstraintPropBuilders.Add(prop);
                    ConstraintFields.Add(s);
                }
            });
            var relationBuilder = new DORelationBuilder(_SchemaBuilder, selectors.Select(s => s.ResolveMember()));
            RelationBuilders.Add(relationBuilder);
            return relationBuilder;

        }

    }

}