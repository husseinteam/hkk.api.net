﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Reflection;

namespace HKK.Core.Domain.Objects {

    public interface IDOTableBuilder<TEntity> : IDOConstraintBuilder<TEntity>
        where TEntity : DOBase<TEntity> {

        DOPropBuilder For<TProp>(Expression<Func<TEntity, TProp>> selector);
        void UniqueKey<TProp>(params Expression<Func<TEntity, TProp>>[] selectors);
        void PrimaryKey<TProp>(params Expression<Func<TEntity, TProp>>[] selectors);

        void MapsTo(Action<IDOSchemaBuilder> schematizer);

        IDOSchemaBuilder SchemaBuilder { get; }
        IList<MemberInfo> ConstraintFields { get; }
        IEnumerable<MemberInfo> RequiredFields();
        IEnumerable<MemberInfo> PKFields();
        IEnumerable<DOPropBuilder> PropBuilders();

        IEnumerable<MemberInfo> RelationKeys();
        IEnumerable<MemberInfo> ReferencingKeys();
        IEnumerable<DOPropBuilder> UniqueKeys();

        void BuildTable();
        void BuildConstraints();
        IDOTableBuilder<TEntity> CutOff();

    }

}