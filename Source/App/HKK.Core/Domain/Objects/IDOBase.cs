﻿
using HKK.Core.Domain.Aggregate;
using HKK.Core.Domain.Contracts;
using HKK.Core.Domain.Enums;
using System;
using System.Collections.Generic;
using System.Reflection;

namespace HKK.Core.Domain.Objects {

    public interface IDOBase {

        void BuildTable();

        IDOBase BuildConstraints();

        int? GetMaxLengthOf(MemberInfo key);

        IEnumerable<MemberInfo> GetFields(EResolveBy by);

        IEnumerable<DOPropBuilder> GetBuilders();

        long ID { get; set; }

        DateTime InsertedAt { get; set; }

        DateTime UpdatedAt { get; set; }

        DateTime DeletedAt { get; set; }

        IDOBase Drop();

        object MapToAggregate(Type aggregateType);

        IHDSResponse InsertSelf();

        IHDSResponse DeleteSelf();

        IDOSchemaBuilder SchemaBuilder { get; }

        IEnumerable<MemberInfo> GetRelationKeys();

        IEnumerable<DOPropBuilder> GetUniqueKeyBuilders();

        IEnumerable<(MemberInfo foreign, MemberInfo referencing)> GetKeys();

    }

}