﻿
namespace HKK.Core.Domain.Objects {

    public interface IDOSchemaBuilder : ILineBuilder, IFormatter {

        IDOSchemaBuilder SchemaName(string schemaName);
        IDOSchemaBuilder TableName(string tableName);
        string GetTableName();
        string GetAlias();
        void RefreshAlias();
    }

}