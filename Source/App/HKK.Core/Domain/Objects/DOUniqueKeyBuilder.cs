﻿
using HKK.Core.Domain.Data;
using HKK.Core.Extensions.Static;

using System;
using System.Collections.Generic;
using System.Linq;

namespace HKK.Core.Domain.Objects {
    internal class DOUniqueKeyBuilder : ILineBuilder {

        internal IEnumerable<DOPropBuilder> UniqueKeyProps { get; private set; }
        private IDOSchemaBuilder hostSchemaBuilder;

        public DOUniqueKeyBuilder(IDOSchemaBuilder hostSchemaBuilder, IEnumerable<DOPropBuilder> uniqueKeyProps) {
            this.hostSchemaBuilder = hostSchemaBuilder;
            this.UniqueKeyProps = uniqueKeyProps;
        }

        public string Build() {
            if (this.UniqueKeyProps.Count() == 0) {
                return String.Empty;
            }
            string uniqueString() {
                var uniqueListString = "";
                if (SDbParams.CurrentServerType == EServerType.MSSql) {
                    uniqueListString = this.UniqueKeyProps.Select(p => SDbParams.NameFormatter.Puts(p.Prop.Name)).Aggregate((prev, next) => "{0}, {1}".Puts(prev, next));
                } else {
                    foreach (var uqProp in this.UniqueKeyProps) {
                        var name = uqProp.Prop.Name;
                        if (uqProp.Prop.Name.ToLower() == "key") {
                            name = SDbParams.NameFormatter.Puts(uqProp.Prop.Name);
                        }
                        var keyLength = uqProp.MaxLength > 0 ? uqProp.MaxLength : 255;
                        uniqueListString += $", {name}({keyLength})";
                    }
                }
                return uniqueListString.TrimStart(',').TrimStart(' ');
            }
            switch (SDbParams.CurrentServerType) {
                case EServerType.MySql:
                    return "CONSTRAINT UQ_{0}_{1} UNIQUE ({2})".Puts(
                        this.hostSchemaBuilder.GetTableName(),
                        this.UniqueKeyProps.Select(p => p.Prop.Name).Aggregate((prev, next) => "{0}_{1}".Puts(prev, next)), uniqueString());
                case EServerType.MSSql:
                    return "CONSTRAINT UQ_{0}_{1} UNIQUE ({2})".Puts(
                        this.hostSchemaBuilder.GetTableName(),
                        this.UniqueKeyProps.Select(p => p.Prop.Name).Aggregate((prev, next) => "{0}_{1}".Puts(prev, next)), uniqueString()
                    );
                default:
                    throw new NotImplementedException("Unsupported option");
            }

        }
    }
}