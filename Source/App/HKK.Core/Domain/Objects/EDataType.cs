﻿
using HKK.Core.Domain.Data;
using HKK.Core.Extensions.Static;
using System;
using System.Collections.Generic;

namespace HKK.Core.Domain.Objects {

    public enum EDataType {
        Text,
        String,
        Int,
        Bool,
        Enum,
        DateTime,
        Date,
        UniqueIdentifier,
        BigInt,
        Spatial,
        Money,
        BinaryMax
    }

    public static class EDataTypeExtensions {

        internal static EDataType GetDataType(this Type self) {

            if (self.Equals(typeof(String))) {
                return EDataType.String;
            } else if (self.Equals(typeof(bool))) {
                return EDataType.Bool;
            } else if (self.Equals(typeof(DateTime))) {
                return EDataType.DateTime;
            } else if (self.IsEnum) {
                return EDataType.Enum;
            } else if (self.Equals(typeof(int?)) || self.Equals(typeof(int))) {
                return EDataType.Int;
            } else if (self.Equals(typeof(long?)) || self.Equals(typeof(long))) {
                return EDataType.BigInt;
            } else if (self.Equals(typeof(decimal))) {
                return EDataType.Spatial;
            } else if (self.Equals(typeof(double)) || self.Equals(typeof(float))) {
                return EDataType.Money;
            } else if (self.Equals(typeof(byte[]))) {
                return EDataType.BinaryMax;
            } else {
                return EDataType.String;
            }

        }

        public static string Parse(this EDataType self, EServerType sType, int maxLength) {
            var responseString = "";
            switch (sType) {
                case EServerType.MySql:
                    switch (self) {
                        case EDataType.String:
                            responseString = maxLength == 0 ? "NVARCHAR(64)" : "NVARCHAR({0})".Puts(maxLength);
                            break;
                        case EDataType.Int:
                        case EDataType.Enum:
                            responseString = "INT" + (maxLength == 0 ? "(11)" : "({0})".Puts(maxLength));
                            break;
                        case EDataType.BigInt:
                            responseString = "BIGINT" + (maxLength == 0 ? "" : "({0})".Puts(maxLength));
                            break;
                        case EDataType.Spatial:
                            responseString = "DECIMAL(9,6)";
                            break;
                        case EDataType.Bool:
                            responseString = "BOOLEAN";
                            break;
                        case EDataType.DateTime:
                            responseString = "DATETIME";
                            break;
                        case EDataType.Date:
                            responseString = "DATE";
                            break;
                        case EDataType.UniqueIdentifier:
                            responseString = "CHAR(36)";
                            break;
                        case EDataType.Text:
                            responseString = "TEXT";
                            break;
                        case EDataType.Money:
                            responseString = "NUMERIC(15,2)";
                            break;
                        case EDataType.BinaryMax:
                            responseString = "LONGBLOB";
                            break;
                        default:
                            throw new KeyNotFoundException("invalid EDataType");
                    }
                    break;
                case EServerType.MSSql:
                    switch (self) {
                        case EDataType.String:
                            responseString = "NVARCHAR" + (maxLength == 0 ? "(MAX)" : "({0})".Puts(maxLength));
                            break;
                        case EDataType.Int:
                        case EDataType.Enum:
                            responseString = "INT";
                            break;
                        case EDataType.BigInt:
                            responseString = "BIGINT";
                            break;
                        case EDataType.Spatial:
                            responseString = "DECIMAL(9,6)";
                            break;
                        case EDataType.Bool:
                            responseString = "Bit";
                            break;
                        case EDataType.DateTime:
                            responseString = "DATETIME2(7)";
                            break;
                        case EDataType.Date:
                            responseString = "DATE";
                            break;
                        case EDataType.UniqueIdentifier:
                            responseString = "UNIQUEIDENTIFIER";
                            break;
                        case EDataType.Text:
                            responseString = "NVARCHAR(MAX)";
                            break;
                        case EDataType.Money:
                            responseString = "SMALLMONEY";
                            break;
                        case EDataType.BinaryMax:
                            responseString = "VARBINARY(MAX)";
                            break;
                        default:
                            throw new KeyNotFoundException("invalid EDataType");
                    }
                    break;
                default:
                    throw new NotImplementedException("Unsupported option");
            }
            return responseString;

        }

    }
}