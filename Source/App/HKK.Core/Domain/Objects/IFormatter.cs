﻿namespace HKK.Core.Domain.Objects {
    public interface IFormatter : ILineBuilder {

        string GetFormatted();

    }
}
