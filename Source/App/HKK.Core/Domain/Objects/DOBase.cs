﻿
using HKK.Core.Domain.Aggregate;
using HKK.Core.Domain.Contracts;
using HKK.Core.Domain.Data;
using HKK.Core.Domain.Enums;
using HKK.Core.Engines;
using HKK.Core.Api.CoreModel.Decorators;
using HKK.Core.Extensions.Static;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace HKK.Core.Domain.Objects {

    public abstract class DOBase<TEntity> : BindableBase, IDOBase
        where TEntity : DOBase<TEntity> {

        protected abstract void Map(IDOTableBuilder<TEntity> builder);

        public abstract string TextValue { get; }

        private static IDOEngine<TEntity> _Engine { get; } = SDataEngine.GenerateDOEngine<TEntity>();
        private IDOTableBuilder<TEntity> _DOBuilder;

        protected IDOTableBuilder<TEntity> TableBuilder {
            get {
                if (_DOBuilder == null) {
                    _DOBuilder = SDbParams.TableBuilder<TEntity>();

                    _DOBuilder.For(d => d.ID).IsTypeOf(EDataType.BigInt).IsIdentity();
                    _DOBuilder.For(d => d.InsertedAt).IsTypeOf(EDataType.DateTime).IsNullable();
                    _DOBuilder.For(d => d.UpdatedAt).IsTypeOf(EDataType.DateTime).IsNullable();
                    _DOBuilder.For(d => d.DeletedAt).IsTypeOf(EDataType.DateTime).IsNullable();

                    _DOBuilder.PrimaryKey(d => d.ID);

                    Map(TableBuilder);
                }
                return _DOBuilder;
            }
        }

        public int? GetMaxLengthOf(MemberInfo key) {
            return TableBuilder.PropBuilders().SingleOrDefault(pb => pb.Prop.Name.Equals(key.Name))?.MaxLength;
        }

        public IEnumerable<(MemberInfo foreign, MemberInfo referencing)> GetKeys() {
            foreach (var item in TableBuilder.RelationKeys()
                .Zip(TableBuilder.ReferencingKeys(), (rlk, rfk) => (foreign: rlk, referencing: rfk))) {
                yield return item;
            }
        }

        public IEnumerable<MemberInfo> GetReferencingKeys() {
            return TableBuilder.ReferencingKeys();
        }

        public IEnumerable<MemberInfo> GetRelationKeys() {
            return TableBuilder.RelationKeys();
        }

        public IEnumerable<DOPropBuilder> GetUniqueKeyBuilders() {
            return TableBuilder.UniqueKeys();
        }

        public IDOSchemaBuilder SchemaBuilder {
            get {
                return TableBuilder.SchemaBuilder;
            }
        }

        [IDLocator]
        public long ID { get; set; }

        public DateTime InsertedAt { get; set; }

        public DateTime UpdatedAt { get; set; }

        public DateTime DeletedAt { get; set; }

        public IDOBase BuildConstraints() {

            TableBuilder.BuildConstraints();
            return this;

        }

        public void BuildTable() {

            TableBuilder.BuildTable();

        }

        public IEnumerable<DOPropBuilder> GetBuilders() {
            return TableBuilder.PropBuilders();
        }

        public IEnumerable<MemberInfo> GetFields(EResolveBy by) {

            switch (by) {
                case EResolveBy.All:
                    return TableBuilder.ConstraintFields;
                case EResolveBy.Assigned:
                    return TableBuilder.ConstraintFields.Where(f => f.IsAssigned(this));
                case EResolveBy.Required:
                    return TableBuilder.RequiredFields();
                case EResolveBy.PKs:
                    return TableBuilder.PKFields();
                case EResolveBy.UQs:
                    return TableBuilder.UniqueKeys().Select(uqb => uqb.Prop);
                default:
                    throw new NotImplementedException("Unsupported option");
            }

        }

        public IDOBase Drop() {
            TableBuilder.CutOff();
            return this;
        }

        public void Absorb(TEntity other) {

            foreach (var field in GetFields(EResolveBy.All)) {
                if (field.IsAssigned(other)) {
                    field.SetValue(this, field.GetValue(other));
                }
            }

        }

        public override string ToString() {
            return this.TextValue;
        }

        public object MapToAggregate(Type aggregateType) {
            var agg = Activator.CreateInstance(aggregateType).As<IAGBase>();
            foreach (var domainProp in typeof(TEntity).ResolveProperties()) {
                var mapping = agg.Mappings.SingleOrDefault(mpp => mpp.domain.PropertyEquals(domainProp));
                mapping.aggregate.SetValue(agg, mapping.domain.GetValue(this as TEntity));
            }
            return agg;
        }

        public IHDSResponse InsertSelf() {
            var e = this as TEntity;
            return _Engine.InsertSync(e) as IHDSResponse;
        }

        public IHDSResponse DeleteSelf() {
            return _Engine.DeleteSync(ID) as IHDSResponse;
        }

    }

}
