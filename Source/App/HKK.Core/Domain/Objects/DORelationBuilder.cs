﻿
using HKK.Core.Domain.Data;
using HKK.Core.Extensions.Static;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;

namespace HKK.Core.Domain.Objects {

    public class DORelationBuilder : ILineBuilder {

        internal IEnumerable<MemberInfo> ReferencedKeys { get; private set; }
        internal IEnumerable<MemberInfo> ForeignKeys { get; private set; }

        private IDOSchemaBuilder hostSchemaBuilder;
        private IDOSchemaBuilder referencedSchemaBuilder;

        private object _Domain;

        public DORelationBuilder(IDOSchemaBuilder hostSchemaBuilder, IEnumerable<MemberInfo> piForeignKeys) {

            this.hostSchemaBuilder = hostSchemaBuilder;
            this.ForeignKeys = piForeignKeys;

        }

        public void References<TDomain>(params Expression<Func<TDomain, long>>[] selectors)
            where TDomain : DOBase<TDomain> {

            _Domain = _Domain ?? Activator.CreateInstance<TDomain>();
            this.referencedSchemaBuilder = _Domain.As<TDomain>().SchemaBuilder;
            this.ReferencedKeys = selectors.Select(s => s.ResolveMember());

        }

        public void ReferencesSelf<TDomain>(params Expression<Func<TDomain, long>>[] selectors)
            where TDomain : DOBase<TDomain> {

            _Domain = _Domain ?? Activator.CreateInstance<TDomain>();
            this.referencedSchemaBuilder = this.hostSchemaBuilder;
            this.ReferencedKeys = selectors.Select(s => s.ResolveMember());

        }

        public string Build() {
            switch (SDbParams.CurrentServerType) {
                case EServerType.MySql:
                case EServerType.MSSql:
                    return "CONSTRAINT FK_{0}_{1} FOREIGN KEY({2}) REFERENCES {3}({4}) ON DELETE NO ACTION ON UPDATE NO ACTION".Puts(
                        this.hostSchemaBuilder.GetTableName(),
                        this.referencedSchemaBuilder.GetTableName(),
                        this.ForeignKeys.Select(p => p.Name).Aggregate((prev, next) => "{0},{1}".Puts(prev, next)),
                        this.referencedSchemaBuilder.GetFormatted(),
                        this.ReferencedKeys.Select(p => p.Name).Aggregate((prev, next) => "{0},{1}".Puts(prev, next))
                        );
                default:
                    throw new NotImplementedException("Unsupported option");
            }
        }

    }

}