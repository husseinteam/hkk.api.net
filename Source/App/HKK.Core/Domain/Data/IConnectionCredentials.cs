﻿namespace HKK.Core.Domain.Data {

    public interface IConnectionCredentials {

        string ConnectionString();

    }

}
