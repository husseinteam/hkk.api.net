﻿using HKK.Core.Extensions.Static;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;

namespace HKK.Core.Domain.Data {

    internal class DataTools : IDataTools {

        public IDataEngine GenerateEngine() {
            return new DataEngine();
        }

        public string SqlExpression(ExpressionType nodeType) {

            switch (SDbParams.CurrentServerType) {
                case EServerType.MySql:
                    switch (nodeType) {
                        case ExpressionType.Equal: return "=";
                        case ExpressionType.NotEqual: return "!=";
                        case ExpressionType.GreaterThanOrEqual: return ">=";
                        case ExpressionType.GreaterThan: return ">";
                        case ExpressionType.LessThanOrEqual: return "<=";
                        case ExpressionType.LessThan: return "<";
                        default:
                            throw new NotImplementedException("Unsupported option: {0}".Puts(nodeType));
                    }
                case EServerType.MSSql:
                    switch (nodeType) {
                        case ExpressionType.Equal: return "=";
                        case ExpressionType.NotEqual: return "!=";
                        case ExpressionType.GreaterThanOrEqual: return ">=";
                        case ExpressionType.GreaterThan: return ">";
                        case ExpressionType.LessThanOrEqual: return "<=";
                        case ExpressionType.LessThan: return "<";
                        default:
                            throw new NotImplementedException("Unsupported option: {0}".Puts(nodeType));
                    }
                default:
                    throw new NotImplementedException("Unsupported option");
            }

        }
        
        public string ParseFields(IEnumerable<MemberInfo> fields) {
            return fields.Select(pr => SDbParams.NameFormatter.Puts(pr.Name)).Aggregate((prev, next) => prev + ", " + next);
        }

        public string ParseParams(IEnumerable<MemberInfo> fields) {
            switch (SDbParams.CurrentServerType) {
                case EServerType.MSSql:
                    return fields.Select(pr => "@" + pr.Name).Aggregate((prev, next) => prev + ", " + next);
                case EServerType.MySql:
                    return fields.Select(pr => "?" + pr.Name).Aggregate((prev, next) => prev + ", " + next);
                default:
                    throw new NotImplementedException("Unsupported option");
            }
        }

    }

}
