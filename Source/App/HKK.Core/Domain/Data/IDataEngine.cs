﻿
using System;
using System.Data;
using System.Data.Common;
using System.Threading.Tasks;

namespace HKK.Core.Domain.Data {

    public interface IDataEngine : IDisposable {
        
        IDataEngine IdentifyCommand(string tableName);
        IDataEngine AlterCommand(string script);

        IDataEngine AddWithValue(string name, object value);
        IDataEngine AddParameter(string name, Type type);

        Task<long> ExecuteCommand();
        Task<DbDataReader> ReadCommand();

        IDataEngine ExecuteScalarSync(out long result);
        IDataEngine ExecuteCommandSync(out long rowc);
        DbDataReader ReadCommandSync();

        IDataEngine SwitchLoggingOff();
        IDataEngine SwitchLoggingOn();
        Exception Fault { get; }

    }

}