﻿using HKK.Core.Api.CoreModel.Models.Data;
using HKK.Core.Engines;
using HKK.Core.Extensions.Static;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Threading.Tasks;

namespace HKK.Core.Domain.Data {

    internal class DataEngine : IDataEngine {

        public DataEngine() {
            KeepConnectified();
        }

        private Exception _Fault;
        public Exception Fault {
            get => _Fault;
            private set {
                _Fault = value;
                if (this._LogFaults) {
                    SDataEngine.GenerateDOEngine<Log>().InsertSync(new Log {
                        StackTrace = SDbParams.LogSource().Puts(FaultMessage(value)),
                        Time = DateTime.Now
                    });
                } else {
                    throw _Fault;
                }
            }
        }

        private string FaultMessage(Exception ex) {
            return $"{ex?.Message ?? ex?.StackTrace ?? "Error Has Neither Message Nor Stack Trace"}";
        }

        private IDbTransaction TransactionObject { get; set; }
        private IDbConnection ConnectionObject => Command.Connection;

        private IDbCommand _Command;
        private bool _LogFaults = true;

        private IDbCommand Command {
            get {
                if (_Command == null) {
                    switch (SDbParams.CurrentServerType) {
                        case EServerType.MySql:
                            var mysqlConn = new MySqlConnection(SDbParams.ConnectionString());
                            _Command = mysqlConn.CreateCommand();
                            break;
                        case EServerType.MSSql:
                            var msSqlConn = new SqlConnection(SDbParams.ConnectionString());
                            _Command = msSqlConn.CreateCommand();
                            break;
                        default:
                            throw new NotImplementedException("Unsupported option");
                    }
                }
                return _Command;
            }
        }

        public IDataEngine IdentifyCommand(string tableName) {
            switch (SDbParams.CurrentServerType) {
                case EServerType.MySql:
                    Command.CommandText = "SELECT LAST_INSERT_ID()";
                    return this;
                case EServerType.MSSql:
                    Command.CommandText = "SELECT IDENT_CURRENT('{0}')".Puts(tableName);
                    return this;
                default:
                    throw new NotImplementedException("Unsupported option");
            }
        }

        public IDataEngine AlterCommand(string script) {
            Command.CommandText = script;
            return this;
        }

        public IDataEngine AddWithValue(string name, object value) {

            switch (SDbParams.CurrentServerType) {
                case EServerType.MySql:
                    var mysqlcommand = Command as MySqlCommand;
                    if (mysqlcommand.Parameters.Contains(name)) {
                        if (value.IsAssigned()) {
                            mysqlcommand.Parameters[name].Value = value;
                        }
                        return this;
                    }
                    if (value.GetType().Equals(typeof(DateTime))) {
                        var prm = new MySqlParameter {
                            ParameterName = name,
                            MySqlDbType = MySqlDbType.DateTime
                        };
                        prm.Value = value;
                        mysqlcommand.Parameters.Add(prm);
                    } else if (value.GetType().Equals(typeof(Guid))) {
                        var prm = new MySqlParameter {
                            ParameterName = name,
                            MySqlDbType = MySqlDbType.Guid
                        };
                        prm.Value = value;
                        mysqlcommand.Parameters.Add(prm);
                    } else {
                        mysqlcommand.Parameters.AddWithValue(name, value);
                    }
                    return this;
                case EServerType.MSSql:
                    var sqlcommand = Command as SqlCommand;
                    if (sqlcommand.Parameters.Contains(name)) {
                        if (value.IsAssigned()) {
                            sqlcommand.Parameters[name].Value = value;
                        }
                        return this;
                    }
                    if (value.GetType().Equals(typeof(DateTime))) {
                        var prm = new SqlParameter {
                            ParameterName = name,
                            SqlDbType = SqlDbType.DateTime2
                        };
                        prm.Value = value;
                        sqlcommand.Parameters.Add(prm);
                    } else if (value.GetType().Equals(typeof(Guid))) {
                        var prm = new SqlParameter {
                            ParameterName = name,
                            SqlDbType = SqlDbType.UniqueIdentifier
                        };
                        prm.Value = value;
                        sqlcommand.Parameters.Add(prm);
                    } else {
                        sqlcommand.Parameters.AddWithValue(name, value);
                    }
                    return this;
                default:
                    throw new NotImplementedException("Unsupported option");
            }

        }

        public IDataEngine AddParameter(string name, Type type) {

            switch (SDbParams.CurrentServerType) {
                case EServerType.MySql:
                    var mysqlcommand = Command as MySqlCommand;
                    if (type.Equals(typeof(DateTime))) {
                        var prm = new MySqlParameter {
                            ParameterName = name,
                            MySqlDbType = MySqlDbType.DateTime
                        };
                        mysqlcommand.Parameters.Add(prm);
                    } else if (type.Equals(typeof(Guid))) {
                        var prm = new MySqlParameter {
                            ParameterName = name,
                            MySqlDbType = MySqlDbType.Guid
                        };
                        mysqlcommand.Parameters.Add(prm);
                    } else {
                        var prm = new MySqlParameter {
                            ParameterName = name
                        };
                        mysqlcommand.Parameters.Add(prm);
                    }
                    return this;
                case EServerType.MSSql:
                    var sqlcommand = Command as SqlCommand;
                    if (type.Equals(typeof(DateTime))) {
                        var prm = new SqlParameter {
                            ParameterName = name,
                            SqlDbType = SqlDbType.DateTime2
                        };
                        sqlcommand.Parameters.Add(prm);
                    } else if (type.Equals(typeof(Guid))) {
                        var prm = new SqlParameter {
                            ParameterName = name,
                            SqlDbType = SqlDbType.UniqueIdentifier
                        };
                        sqlcommand.Parameters.Add(prm);
                    } else {
                        var prm = new SqlParameter {
                            ParameterName = name,
                        };
                        sqlcommand.Parameters.Add(prm);
                    }
                    return this;
                default:
                    throw new NotImplementedException("Unsupported option");
            }

        }

        public void Dispose() {
            Command.Parameters.Clear();
            if (ConnectionObject.State != ConnectionState.Closed) {
                ConnectionObject.Close();
            }
            this._LogFaults = false;
        }

        public async Task<long> ExecuteCommand() {
            KeepConnectified();
            switch (SDbParams.CurrentServerType) {
                case EServerType.MySql:
                    try {
                        return (await (Command as MySqlCommand).ExecuteNonQueryAsync()).As<long>();
                    } catch (Exception ex) {
                        Fault = ex;
                    }
                    break;
                case EServerType.MSSql:
                    try {
                        return (await (Command as SqlCommand).ExecuteNonQueryAsync()).As<long>();
                    } catch (Exception ex) {
                        Fault = ex;
                    }
                    break;
                default:
                    throw new ArgumentException($"Server Key: {SDbParams.CurrentServerType} Not Implemented");
            }
            return -1;
        }

        public IDataEngine ExecuteCommandSync(out long rowc) {
            KeepConnectified();
            rowc = -1;
            switch (SDbParams.CurrentServerType) {
                case EServerType.MySql:
                    try {
                        rowc = (Command as MySqlCommand).ExecuteNonQuery().As<long>();
                    } catch (Exception ex) {
                        Fault = ex;
                    }
                    break;
                case EServerType.MSSql:
                    try {
                        rowc = (Command as SqlCommand).ExecuteNonQuery().As<long>();
                    } catch (Exception ex) {
                        Fault = ex;
                    }
                    break;
                default:
                    throw new ArgumentException($"Server Key: {SDbParams.CurrentServerType} Not Implemented");
            }
            return this;
        }

        public IDataEngine SwitchLoggingOff() {
            this._LogFaults = false;
            return this;
        }

        public IDataEngine SwitchLoggingOn() {
            this._LogFaults = true;
            return this;
        }

        public IDataEngine ExecuteScalarSync(out long result) {
            KeepConnectified();
            switch (SDbParams.CurrentServerType) {
                case EServerType.MySql:
                    result = (Command as MySqlCommand).ExecuteScalar().ToInt32();
                    break;
                case EServerType.MSSql:
                    result = (Command as SqlCommand).ExecuteScalar().ToInt32();
                    break;
                default:
                    throw new ArgumentException($"Server Key: {SDbParams.CurrentServerType} Not Implemented");
            }
            return this;
        }

        public async Task<DbDataReader> ReadCommand() {
            KeepConnectified();
            switch (SDbParams.CurrentServerType) {
                case EServerType.MySql:
                    try {
                        return await (Command as MySqlCommand).ExecuteReaderAsync(CommandBehavior.CloseConnection);
                    } catch (MySqlException ex) {
                        Fault = ex;
                    }
                    break;
                case EServerType.MSSql:
                    try {
                        return await (Command as SqlCommand).ExecuteReaderAsync(CommandBehavior.CloseConnection);
                    } catch (SqlException ex) {
                        Fault = ex;
                    }
                    break;
                default:
                    throw new ArgumentException($"Server Key: {SDbParams.CurrentServerType} Not Implemented");
            }
            return null;
        }

        public DbDataReader ReadCommandSync() {
            KeepConnectified();
            switch (SDbParams.CurrentServerType) {
                case EServerType.MySql:
                    try {
                        return (Command as MySqlCommand).ExecuteReader(CommandBehavior.CloseConnection);
                    } catch (MySqlException ex) {
                        Fault = ex;
                    }
                    break;
                case EServerType.MSSql:
                    try {
                        return (Command as SqlCommand).ExecuteReader(CommandBehavior.CloseConnection);
                    } catch (SqlException ex) {
                        Fault = ex;
                    }
                    break;
                default:
                    throw new ArgumentException($"Server Key: {SDbParams.CurrentServerType} Not Implemented");
            }
            return null;
        }

        private void BeginTransaction() {
            KeepConnectified();
            TransactionObject = ConnectionObject.BeginTransaction(IsolationLevel.Serializable);
            Command.Transaction = TransactionObject;
        }

        private void KeepConnectified() {
            if (ConnectionObject.State == ConnectionState.Closed) {
                ConnectionObject.Open();
            }
        }

    }

}
