﻿using HKK.Core.Domain.Enums;
using HKK.Core.Domain.Objects;
using HKK.Core.Extensions.Static;
using System;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.IO;
using System.Runtime.CompilerServices;

namespace HKK.Core.Domain.Data {
    public static class SDbParams {

        public static bool IsLocalMachine() {
            return new StackTrace().GetReferencingAssembly().IsDebugBuild();
        }


        internal static IDataTools DataTools { get; } = new DataTools();
        internal static IDOTableBuilder<TEntity> TableBuilder<TEntity>() where TEntity : DOBase<TEntity> {
            return new DOTableBuilder<TEntity>();
        }

        public static string ConnectionString() {

            if (IsLocalMachine()) {
                return DebugConnectionString[CurrentServerType];

            } else {
                return ReleaseConnectionString[CurrentServerType];
            }

        }

        private static EServerType _CurrentServerType = EServerType.Unset;

        public static EServerType CurrentServerType {
            get {
                return _CurrentServerType;
            }
        }


        public static string ParamKey {
            get {
                switch (CurrentServerType) {
                    case EServerType.MySql:
                        return "?";
                    case EServerType.MSSql:
                        return "@";
                    default:
                        throw new NotSupportedException($"ServerKey: {CurrentServerType.ToString()} Not Supported");
                }
            }
        }

        public static string NameFormatter {
            get {
                switch (CurrentServerType) {
                    case EServerType.MySql:
                        return "`{0}`";
                    case EServerType.MSSql:
                        return "[{0}]";
                    default:
                        throw new NotSupportedException($"ServerKey: {CurrentServerType.ToString()} Not Supported");
                }
            }
        }

        private static Dictionary<EServerType, String> DebugConnectionString = new Dictionary<EServerType, string>();
        private static Dictionary<EServerType, String> ReleaseConnectionString = new Dictionary<EServerType, string>();

        public static void SetConnectionString(string connectionString, EConnectionStringMode mode, EServerType serverType = EServerType.Unset) {
            _CurrentServerType = serverType;
            switch (serverType) {
                case EServerType.MySql:
                    connectionString += ";Allow User Variables=True";
                    break;
                case EServerType.MSSql:
                    break;
                default:
                    break;
            }
            switch (mode) {
                case EConnectionStringMode.Debug:
                    DebugConnectionString[serverType] = connectionString;
                    break;
                case EConnectionStringMode.Release:
                    ReleaseConnectionString[serverType] = connectionString;
                    break;
                default:
                    break;
            }
        }

        public static string LogSource([CallerFilePath] string file = "",
                        [CallerMemberName] string member = "",
                        [CallerLineNumber] int line = 0) {
            return $"{Path.GetFileNameWithoutExtension(file)}.{member}(line:{line}): {{0}}";
        }

    }
}
