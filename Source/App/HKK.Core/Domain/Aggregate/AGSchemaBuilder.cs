﻿
using HKK.Core.Domain.Data;
using HKK.Core.Domain.Objects;
using HKK.Core.Extensions.Static;

using System;

namespace HKK.Core.Domain.Aggregate {

    public class AGSchemaBuilder : IAGSchemaBuilder {

        private string schemaName;
        private string viewName;

        public string FormatsBy(string format) {
            return format.Puts(this.schemaName, this.viewName);
        }

        public IAGSchemaBuilder SchemaName(string schemaName) {

            this.schemaName = SDbParams.CurrentServerType == EServerType.MySql ? schemaName.ToLower() : schemaName;
            return this;

        }

        public IAGSchemaBuilder ViewName(string viewName) {

            this.viewName = SDbParams.CurrentServerType == EServerType.MySql ? viewName.ToLower() : viewName;
            return this;

        }
        public string GetViewName() {
            return this.viewName;
        }

        public string GetFormatted() {
            switch (SDbParams.CurrentServerType) {
                case EServerType.MySql:
                    return "`{0}_{1}`".Puts(this.schemaName, this.viewName);
                case EServerType.MSSql:
                    return "[{0}].[{1}]".Puts(this.schemaName, this.viewName);
                default:
                    throw new NotImplementedException("Unsupported keyType");
            }
        }

        public string Build() {

            switch (SDbParams.CurrentServerType) {
                case EServerType.MySql:
                    return GetFormatted();
                case EServerType.MSSql:
                    var script = @"
IF NOT EXISTS (SELECT schema_name 
    FROM INFORMATION_SCHEMA.SCHEMATA 
    WHERE schema_name = '{0}' )
BEGIN
    EXEC sp_executesql N'CREATE SCHEMA {0};'
END";
                    using (var engine = SDbParams.DataTools.GenerateEngine().AlterCommand(script.Puts(schemaName))) {
                        engine.SwitchLoggingOff().ExecuteCommandSync(out var rowc);
                    }
                    return GetFormatted();
                default:
                    throw new NotImplementedException("Unsupported keyType");
            }

        }

    }


}