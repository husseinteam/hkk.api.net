﻿
using System;
using System.Linq.Expressions;

namespace HKK.Core.Domain.Aggregate {

    public interface IAggragator<T> 
        where T : AGBase<T> {

        IAggragator<T> SetDistinctColumn<TProp>(Expression<Func<T, TProp>> column);

        ISelectedAggragator<T> SelectColumns<TProp>(
            Expression<Func<T, TProp>> column);
        ISelectedAggragator<T> SelectAllColumns();
        long SelectCount();
    }

}