﻿
using HKK.Core.Domain.Data;
using HKK.Core.Domain.Objects;
using HKK.Core.Extensions.Static;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Reflection;
using System.Text;

namespace HKK.Core.Domain.Aggregate {

    internal class AGSelectList<TEntity, TAggregate> : IAGSelectList<TEntity, TAggregate>
        where TEntity : DOBase<TEntity>
        where TAggregate : AGBase<TAggregate> {
        
        public AGSelectList() {
            SelectedExpressions = new List<Tuple<Expression, Expression, Type>>();
        }

        internal List<Tuple<Expression, Expression, Type>> SelectedExpressions { get; private set; }

        public IAGSelectList<TEntity, TAggregate> Map<TProp>(Expression<Func<TEntity, TProp>> columnSelector, Expression<Func<TAggregate, TProp>> aliasSelector, Type bindedAggregate = null) {

            SelectedExpressions.Add(
                Tuple.Create(columnSelector.Body, aliasSelector.Body, bindedAggregate));
            return this;

        }


        public override string ToString() {
            var str = new StringBuilder();
            foreach (var (columnSelector, aggSelector, _) in SelectedExpressions) {
                if (columnSelector as MemberExpression != null) {
                    var prop = columnSelector.ExposeMember();
                    var entity = Activator.CreateInstance(typeof(TEntity)) as IDOBase;
                    str.AppendFormat("{0}.{1} AS {2}, ", entity.SchemaBuilder.GetAlias(),
                        SDbParams.NameFormatter.Puts(prop.Name), aggSelector.ExposeMember().Name);
                } else {
                    var compositString = new StringBuilder();
                    var entity = Activator.CreateInstance(typeof(TEntity)) as IDOBase;
                    Action<MemberInfo> caseMember = (pr) => {
                        compositString.AppendFormat(" {0}.{1} ",
                            entity.SchemaBuilder.GetAlias(), SDbParams.NameFormatter.Puts(pr.Name));
                    };
                    Action<object> caseConstant = (value) => {
                        compositString.AppendFormat("+ '{0}' +", value);
                    };

                    if (columnSelector is BinaryExpression binary) {
                        var left = binary.Left;
                        if (left is BinaryExpression lbinary) {
                            var left1 = lbinary.Left;
                            var left2 = lbinary.Right;
                            left1.ExtractComposit(caseMember, caseConstant);
                            left2.ExtractComposit(caseMember, caseConstant);
                        } else {
                            left.ExtractComposit(caseMember, caseConstant);
                        }
                        columnSelector.ExtractComposit(caseMember, caseConstant);
                    } else if (columnSelector.NodeType == ExpressionType.Call) {
                        throw new Exception("You cannot specify method call in a aggregate mapping. Use a computed mapping instead.");
                    } else if (columnSelector is UnaryExpression unary) {
                        unary.ExtractComposit(caseMember, caseConstant);
                    }

                    str.AppendFormat("{0} AS {1}, ", compositString, aggSelector.ExposeMember().Name);
                }
            }
            return str.ToString().TrimEnd(' ', ',');
        }

    }

}