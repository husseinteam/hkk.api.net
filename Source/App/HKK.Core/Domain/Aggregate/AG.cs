﻿namespace HKK.Core.Domain.Aggregate {
    public static class AG<TEntity>
        where TEntity : AGBase<TEntity> {

        public static IAggragator<TEntity> Views() {

            return new Aggragator<TEntity>();

        }

    }
}
