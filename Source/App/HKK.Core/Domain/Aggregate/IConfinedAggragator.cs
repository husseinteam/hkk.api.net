﻿using System;
using System.Threading.Tasks;

namespace HKK.Core.Domain.Aggregate {

    public interface IConfinedAggragator<T>
        where T : AGBase<T> {

        Task ExecuteList(Action<T> cursor, Action noneback, Action<Exception> failback = null);
        Task ExecuteSingle(Action<T> cursor, Action noneback, Action<Exception> failback = null);
        void ExecuteListSync(Action<T> cursor, Action noneback, Action<Exception> failback = null);
        void ExecuteSingleSync(Action<T> cursor, Action noneback, Action<Exception> failback = null);
    }

}