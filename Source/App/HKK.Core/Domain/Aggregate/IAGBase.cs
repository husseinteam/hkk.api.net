﻿
using HKK.Core.Api.Controller;
using HKK.Core.Domain.Contracts;
using HKK.Core.Domain.Objects;
using System;
using System.Collections.Generic;
using System.Reflection;

namespace HKK.Core.Domain.Aggregate {

    public interface IAGBase : IEquatable<IAGBase> {

        long ID { get; set; }
        IAGBase Drop();
        void BuildView();
        IAGSchemaBuilder GetSchemaBuilder();
        TDomain GetMappedDomain<TDomain>()
            where TDomain : DOBase<TDomain>;
        IDOBase GetMappedDomainByType(Type domainType);
        bool IsColumnRequired(PropertyInfo property);

        IEnumerable<(MemberInfo domain, MemberInfo aggregate, Type binder)> Mappings { get; }
        IEnumerable<(MemberInfo domain, MemberInfo aggregate, Type binder)> SortedAggregates();

        Type MainDomain { get; }
        string TextValue { get; }
        string ResourceName { get; }

        long GetMaxLength(MemberInfo key);
        IHDSResponse InsertThat(out IAGBase that);
        IHDSResponse InsertSelf();
        IHDSResponse DeleteSelf();
        IAGBase CopyFromAggregate(IAGBase aggregate);
        IAGBase CopyFromDomain(IDOBase domain);
        IClientTransformer GetClientTransformer();

    }

}