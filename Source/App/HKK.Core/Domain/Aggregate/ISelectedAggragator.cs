﻿
using System;
using System.Linq.Expressions;
using System.Reflection;

namespace HKK.Core.Domain.Aggregate {

    public interface ISelectedAggragator<T> : IConfinedAggragator<T>
        where T : AGBase<T> {

        IWheredAggragator<T> Where(Expression<Func<T, bool>> selector);
        IWheredAggragator<T> WhereDynamic(MemberInfo aggregateProp, object targetValue);
    }

}