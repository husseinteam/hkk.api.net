﻿using HKK.Core.Domain.Data;
using HKK.Core.Domain.Enums;
using HKK.Core.Domain.Objects;
using HKK.Core.Engines;
using HKK.Core.Api.CoreModel.Decorators;
using HKK.Core.Extensions.Static;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;

namespace HKK.Core.Domain.Aggregate {

    public class AGViewBuilder<TAggregate> : IAGViewBuilder<TAggregate>, IAGMappedViewBuilder<TAggregate>, IAGSelectedViewBuilder<TAggregate>
        where TAggregate : AGBase<TAggregate> {

        public AGViewBuilder() {
            SchemaBuilder = new AGSchemaBuilder();
            QueryString = new StringBuilder("SELECT #selectors# FROM #table# #joinlist#");
        }

        internal void AppendJoinString(string join) {
            QueryString.Replace("#joinlist#", "{0} #joinlist#".Puts(join));
        }

        private bool JoinedOnce = false;
        private static List<string> AliasesRegistry { get; } = new List<string>();
        public IAGSchemaBuilder SchemaBuilder { get; private set; }
        public StringBuilder QueryString { get; private set; }
        internal List<(MemberInfo domain, MemberInfo aggregate, Type binder)> Mappings { get; private set; }
            = new List<(MemberInfo domain, MemberInfo aggregate, Type binder)>();

        public IAGMappedViewBuilder<TAggregate> MapsTo(Action<IAGSchemaBuilder> schematizer) {

            schematizer(this.SchemaBuilder);
            return this;

        }

        internal IDOBase GetMappedDomain(Type domainType, TAggregate agg) {
            var obj = Activator.CreateInstance(domainType) as IDOBase;
            foreach (var (domain, aggregate, _) in Mappings) {
                var value = aggregate.GetValue(agg);
                if (domain.ParentType().Equals(domainType)) {
                    domain?.SetValue(obj, value);
                }
                foreach (var keys in obj.GetKeys()) {
                    if (keys.referencing.PropertyEquals(domain)) {
                        keys.foreign.SetValue(obj, value);
                    }
                }
            }
            return obj;
        }

        internal bool IsColumnRequired(PropertyInfo property) {
            var (domain, aggregate, _) = Mappings.SingleOrDefault(mp => mp.Item2.Name == property.Name);
            if (domain != null) {
                return (Activator.CreateInstance(domain.ReflectedType) as IDOBase).GetFields(EResolveBy.Required)
                    .Any(f => f.Name == domain.Name);
            } else {
                return false;
            }
        }

        internal int GetMaxLength(MemberInfo key) {
            var (domain, aggregate, _) = Mappings.SingleOrDefault(mp => mp.Item2.Name == key.Name);
            if (domain != null) {
                return (Activator.CreateInstance(domain.ReflectedType) as IDOBase).GetMaxLengthOf(domain) ?? 0;
            } else {
                return 0;
            }
        }

        public IAGSelectedViewBuilder<TAggregate> Select<TDomain>(Action<IAGSelectList<TDomain, TAggregate>> selectCursor = null)
            where TDomain : DOBase<TDomain> {
            var entity = Activator.CreateInstance<TDomain>();
            ApplyCursor(selectCursor);
            QueryString.Replace("#table#", "{0} as {1}".Puts(entity.SchemaBuilder.GetFormatted(), entity.SchemaBuilder.GetAlias()));
            return this;
        }

        public IJoinBuilder<TDomain, TAggregate> InnerJoin<TDomain>(Action<IAGSelectList<TDomain, TAggregate>> selectCursor = null)
            where TDomain : DOBase<TDomain> {
            if (JoinedOnce) {
                RefreshAliasesRegistry<TDomain>();
            }
            ApplyCursor(selectCursor);
            JoinedOnce = true;
            return new JoinBuilder<TDomain, TAggregate>(this);
        }

        public IJoinBuilder<TDomain, TAggregate> OuterJoin<TDomain>(Action<IAGSelectList<TDomain, TAggregate>> selectCursor = null)
            where TDomain : DOBase<TDomain> {
            if (JoinedOnce) {
                RefreshAliasesRegistry<TDomain>();
            }
            ApplyCursor(selectCursor);
            JoinedOnce = true;
            return new JoinBuilder<TDomain, TAggregate>(this, true);
        }

        private void ApplyCursor<TDomain>(Action<IAGSelectList<TDomain, TAggregate>> selectCursor) where TDomain : DOBase<TDomain> {

            var entity = Activator.CreateInstance<TDomain>() as IDOBase;
            IAGSelectList<TDomain, TAggregate> selectList = new AGSelectList<TDomain, TAggregate>();
            selectCursor(selectList);
            QueryString.Replace("#selectors#", "{0}, #selectors#".Puts(selectList));
            var mappings = (selectList as AGSelectList<TDomain, TAggregate>).SelectedExpressions
                .Select(col => (domain: col.Item1.ExposeMember(), aggregate: col.Item2.ExposeMember(), binder: col.Item3));
            Mappings.AddRange(mappings);

        }

        private static void RefreshAliasesRegistry<TDomain>() where TDomain : DOBase<TDomain> {
            var entity = Activator.CreateInstance<TDomain>();
            if (AliasesRegistry.Contains(entity.SchemaBuilder.GetAlias())) {
                entity.SchemaBuilder.RefreshAlias();
            }
            AliasesRegistry.Append(entity.SchemaBuilder.GetAlias());
        }
    }

}