﻿
using HKK.Core.Domain.Objects;
using System;
using System.Linq.Expressions;

namespace HKK.Core.Domain.Aggregate {

    public interface IJoinBuilder<TEntity, T> 
        where TEntity : DOBase<TEntity>
        where T : AGBase<T> {
        
        IAGSelectedViewBuilder<T> On<TOther>(Expression<Func<TEntity, TOther, bool>> selector)
            where TOther : DOBase<TOther>;

    }

}