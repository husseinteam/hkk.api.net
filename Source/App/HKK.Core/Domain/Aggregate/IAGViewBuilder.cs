﻿using HKK.Core.Domain.Objects;
using System;
using System.Text;

namespace HKK.Core.Domain.Aggregate {

    public interface IAGViewBuilder<T> where T : AGBase<T> {

        IAGMappedViewBuilder<T> MapsTo(Action<IAGSchemaBuilder> schematizer);
        IAGSchemaBuilder SchemaBuilder { get; }
        StringBuilder QueryString { get; }

    }

}