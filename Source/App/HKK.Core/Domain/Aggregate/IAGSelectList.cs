﻿using HKK.Core.Domain.Objects;
using System;
using System.Linq.Expressions;

namespace HKK.Core.Domain.Aggregate {

    public interface IAGSelectList<TEntity, T>
        where TEntity : DOBase<TEntity>
        where T : AGBase<T> {

        IAGSelectList<TEntity, T> Map<TProp>(Expression<Func<TEntity, TProp>> columnSelector, Expression<Func<T, TProp>> aliasSelector, Type bindedAggregate = null);
    }

}