﻿
namespace HKK.Core.Domain.Objects {

    public interface IAGSchemaBuilder : ILineBuilder, IFormatter {

        IAGSchemaBuilder SchemaName(string schemaName);
        IAGSchemaBuilder ViewName(string viewName);
        string GetViewName();

    }

}