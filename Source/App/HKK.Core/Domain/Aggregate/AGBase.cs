﻿
using HKK.Core.Domain.Data;
using HKK.Core.Domain.Objects;
using HKK.Core.Api.CoreModel.Decorators;
using HKK.Core.Extensions.Static;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text.RegularExpressions;
using HKK.Core.Domain.Contracts;
using HKK.Core.Domain.DML;
using HKK.Core.Engines;
using HKK.Core.Engines.Core;
using System.Collections;
using HKK.Core.Domain.Enums;
using HKK.Core.Api.Controller;

namespace HKK.Core.Domain.Aggregate {

    public abstract class AGBase<TAggregate> : IAGBase
        where TAggregate : AGBase<TAggregate> {

        private static IAGViewBuilder<TAggregate> _Builder;

        private IAGViewBuilder<TAggregate> GetBuilder() {
            if (_Builder == null) {
                _Builder = new AGViewBuilder<TAggregate>();
                Map(_Builder);
            }
            return _Builder;
        }

        protected abstract void Map(IAGViewBuilder<TAggregate> builder);
        public abstract Type MainDomain { get; }
        public abstract string TextValue { get; }
        public abstract string ResourceName { get; }

        public IClientTransformer GetClientTransformer() {
            return new ClientTransformer().SetHandler((model, direction) => {
                var props = typeof(TAggregate).ResolveProperties().Where(
                    p => p.HasAttribute<ClientSideAttribute>() || p.HasAttribute<IDLocatorAttribute>());
                dynamic obj = new Dictionary<string, object>();
                foreach (var prop in props) {
                    var propName = prop.HasAttribute<ClientSideAttribute>(out var clientSide) ? clientSide.Name : "id";
                    switch (direction) {
                        case ETransform.SetClient:
                            if (prop.HasAttribute<HavingCollectionAttribute>() == false) {
                                obj[propName] = prop.GetValue(model);
                            } else {
                                var collection = prop.GetValue(model);
                                obj[propName] = collection.As<IEnumerable>().Transform<IAGBase>(
                                    item => item.GetClientTransformer().AggregateToClient(item));
                            }
                            break;
                        case ETransform.SetAggregate:
                            if (prop.HasAttribute<HavingCollectionAttribute>() == false) {
                                prop.SetValue(this, model.GetPropValue(clientSide.Name));
                            } else {
                                var collection = new List<IAGBase>();
                                Type itemType = prop.GetValue(this).GenericArguments().FirstOrDefault();
                                var transformer = itemType.Materialize<IAGBase>().GetClientTransformer();
                                foreach (var item in model.GetPropValue(propName).As<IEnumerable>()) {
                                    collection.Add(transformer.ClientToAggregate(item));
                                }
                                prop.SetValue(this, collection.CastToList(itemType));
                            }
                            break;
                    }
                }
                return direction == ETransform.SetClient ? obj : model;
            });
        }

        [TableColumn(Order = 0)]
        public long ID {
            get {
                var props = typeof(TAggregate).GetProperties(BindingFlags.Instance | BindingFlags.Public)
                        .Where(p => p.GetCustomAttribute<IDLocatorAttribute>() != null);
                if (props.Count() > 1) {
                    throw new InvalidOperationException("Multiple IDPropertyLocator Specified");
                } else if (props.Count() == 0) {
                    throw new InvalidOperationException("None of Multiple IDPropertyLocator Specified");
                }
                var prop = props.First();
                return Convert.ToInt64(prop.GetValue(this));
            }
            set {
                var props = typeof(TAggregate).GetProperties(BindingFlags.Instance | BindingFlags.Public)
                                        .Where(p => p.GetCustomAttribute<IDLocatorAttribute>() != null);
                if (props.Count() > 1) {
                    throw new InvalidOperationException("Multiple IDPropertyLocator Specified");
                } else if (props.Count() == 0) {
                    throw new InvalidOperationException("None of Multiple IDPropertyLocator Specified");
                }
                var prop = props.First();
                prop.SetValue(this, value);
            }
        }

        public TDomain GetMappedDomain<TDomain>()
            where TDomain : DOBase<TDomain> {
            return (GetBuilder() as AGViewBuilder<TAggregate>).GetMappedDomain(typeof(TDomain), this as TAggregate) as TDomain;
        }

        public IDOBase GetMappedDomainByType(Type domainType) {
            if (domainType.IsSubclassOf(typeof(DOBase<>).MakeGenericType(domainType))) {
                return (GetBuilder() as AGViewBuilder<TAggregate>).GetMappedDomain(domainType, this as TAggregate);
            } else {
                throw new ArgumentException($"{domainType.ToString()} Should Have Been Assignable From DOBase<>");
            }
        }

        public bool IsColumnRequired(PropertyInfo property) {
            return (GetBuilder() as AGViewBuilder<TAggregate>).IsColumnRequired(property);
        }

        public long GetMaxLength(MemberInfo key) {
            return (GetBuilder() as AGViewBuilder<TAggregate>).GetMaxLength(key);
        }

        public IAGSchemaBuilder GetSchemaBuilder() {
            return GetBuilder().SchemaBuilder;
        }

        public IAGBase Drop() {
            using (var engine = SDbParams.DataTools.GenerateEngine()) {
                var commandText = "";
                switch (SDbParams.CurrentServerType) {
                    case Data.EServerType.MySql:
                        commandText = "DROP VIEW IF EXISTS {1}";
                        break;
                    case Data.EServerType.MSSql:
                        commandText = "IF EXISTS(select * FROM sys.views where name = '{0}') DROP VIEW {1}";
                        break;
                    default:
                        throw new ArgumentException("Unsupported Server Type");
                }
                commandText = commandText.Puts(GetBuilder().SchemaBuilder.GetViewName(),
                        GetBuilder().SchemaBuilder.GetFormatted());
                engine.AlterCommand(commandText).ExecuteCommandSync(out var rowc);
            }
            return this;

        }

        public void BuildView() {
            var viewAndSchema = GetBuilder().SchemaBuilder.Build();
            var selectStatement = GetBuilder().QueryString
                .Replace(", #selectors#", "")
                .Replace(" #joinlist#", "");
            var commandText = "";
            switch (SDbParams.CurrentServerType) {
                case Data.EServerType.MySql:
                    commandText = "CREATE VIEW {0} AS {1}".Puts(viewAndSchema, selectStatement);
                    break;
                case Data.EServerType.MSSql:
                    var countStatement = Regex.Replace(selectStatement.ToString(), @"(SELECT )(.*)( FROM.*)", "$1COUNT(*)$3");
                    var createViewStatement = selectStatement.Insert(selectStatement.ToString().ToUpper().IndexOf("SELECT") + 7, "TOP ({0})".Puts(countStatement));
                    commandText = "CREATE VIEW {0} WITH SCHEMABINDING AS {1}".Puts(viewAndSchema, createViewStatement);
                    break;
                default:
                    throw new ArgumentException("Unsupported Server Type");
            }

            using (var engine = SDbParams.DataTools.GenerateEngine()) {
                engine.SwitchLoggingOff().AlterCommand(commandText).ExecuteCommandSync(out var rowc);
            }
        }

        public override string ToString() {
            return this.TextValue;
        }

        public IHDSResponse InsertThat(out IAGBase that) {
            var resp = this.InsertSelf();
            that = this as TAggregate;
            return resp;
        }

        public IAGBase CopyFromAggregate(IAGBase aggregate) {
            IEnumerable<(MemberInfo, MemberInfo)> counterparts() {
                foreach (var mpp in Mappings.Where(map => !map.aggregate.HasAttribute<IDLocatorAttribute>())) {
                    foreach (var mppagg in aggregate.Mappings) {
                        if (mppagg.domain.Name == mpp.domain.Name) {
                            yield return (mpp.aggregate, mppagg.aggregate);
                        }
                    }
                }
            }
            foreach (var (prop, propagg) in counterparts()) {
                prop.SetValue(this, propagg.GetValue(aggregate));
            }
            return this;
        }

        public IAGBase CopyFromDomain(IDOBase domain) {
            IEnumerable<(MemberInfo, MemberInfo)> counterparts() {
                foreach (var mpp in Mappings) {
                    if (mpp.domain.DeclaringType.GetGenericArguments().FirstOrDefault()?.Name == domain.GetType().Name) {
                        yield return (mpp.aggregate, mpp.domain);
                    }
                }
            }
            foreach (var (prop, propdm) in counterparts()) {
                prop.SetValue(this, propdm.GetValue(domain));
            }
            return this;
        }

        public IHDSResponse InsertSelf() {
            foreach (var (_, colagg, binder) in SortedAggregates().Reverse()) {
                var referencedAggregate = binder.Breath<IAGBase>();
                referencedAggregate.CopyFromAggregate(this);
                var response = referencedAggregate.InsertSelf();
                if (response.EntityID > 0 && colagg.DeclaringType.Equals(typeof(TAggregate))) {
                    colagg.SetValue(this, response.EntityID);
                }
            }

            var domain = this.GetMappedDomainByType(this.MainDomain);
            var domainResp = domain.InsertSelf();
            if (domainResp.HasData) {
                if (domain.ID > 0) {
                    InsertSubCollection(domain.ID);
                }
                return new HDSResponse(this.CopyFromDomain(domain));
            } else {
                return domainResp;
            }
        }

        public IEnumerable<(MemberInfo domain, MemberInfo aggregate, Type binder)> SortedAggregates() {
            var boundList = Mappings.Where(map => map.binder != null);
            foreach (var it in boundList) {
                foreach (var iit in it.binder.Breath<IAGBase>().SortedAggregates()) {
                    yield return iit;
                }
                yield return it;
            }
        }

        private void InsertSubCollection(long referenceID) {
            foreach (var prop in ReflectionExtensions.ResolveProperties(typeof(TAggregate))) {
                if (prop.HasAttribute<HavingCollectionAttribute>(out var attr)) {
                    var collection = prop.GetValue(this);
                    var elementType = collection.GetType().GetGenericArguments().FirstOrDefault();
                    if (typeof(IAGBase).IsAssignableFrom(elementType)) {
                        var targetProp = ReflectionExtensions.ResolveProperties(elementType)
                            .SingleOrDefault(p => p.Name == attr.InverseIDProperty);
                        var list = new List<IAGBase>();
                        foreach (IAGBase agg in collection.As<IEnumerable>()) {
                            targetProp?.SetValue(agg, referenceID);
                            if (agg.InsertThat(out var inserted).HasData) {
                                list.Add(inserted);
                            }
                        }
                        prop.SetValue(this, list.CastToList(elementType));
                    }
                }
            }
        }

        public IHDSResponse DeleteSelf() {
            foreach (var prop in ReflectionExtensions.ResolveProperties(typeof(TAggregate))) {
                if (prop.HasAttribute<HavingCollectionAttribute>(out var attr)) {
                    var collection = prop.GetValue(this);
                    var elementType = collection.GetType().GetGenericArguments().FirstOrDefault();
                    IGenericEngine elementEng = null;
                    if (typeof(IAGBase).IsAssignableFrom(elementType)) {
                        elementEng = Activator.CreateInstance(typeof(AGEngine<>).MakeGenericType(elementType)) as IGenericEngine;
                        var stronglyElement = Activator.CreateInstance(elementType).As<IAGBase>();
                        var view = Activator.CreateInstance(elementType).As<IAGBase>();
                        var targetProp = view.GetType().GetProperty(attr.InverseIDProperty);
                        IHDSResponse listedCollectionResponse = elementEng.GenericSelectByComparison(targetProp, this.ID);
                        if (listedCollectionResponse.HasData == true) {
                            foreach (IAGBase item in listedCollectionResponse.Items) {
                                var dmn = item.GetMappedDomainByType(item.MainDomain);
                                var dmnDeleteResp = dmn.DeleteSelf();
                                if (dmnDeleteResp.HasData == false) {
                                    return dmnDeleteResp;
                                }
                            }
                        }
                    } else {
                        elementEng = Activator.CreateInstance(typeof(DOEngine<>).MakeGenericType(elementType)) as IGenericEngine;
                        var stronglyElement = Activator.CreateInstance(elementType).As<IDOBase>();
                        var targetProp = elementType.GetProperty(attr.InverseIDProperty);
                        IHDSResponse listedCollectionResponse = elementEng.GenericSelectByComparison(targetProp, this.ID);
                        if (listedCollectionResponse.HasData == true) {
                            foreach (IDOBase item in listedCollectionResponse.Items) {
                                var dmnDeleteResp = item.DeleteSelf();
                                if (dmnDeleteResp.HasData == false) {
                                    return dmnDeleteResp;
                                }
                            }
                        }
                    }
                }
            }
            var domain = this.GetMappedDomainByType(this.MainDomain);
            var deleteResponse = domain.DeleteSelf();
            if (deleteResponse.HasData == true) {
                foreach (var (coldomain, colagg, binder) in SortedAggregates().Reverse()) {
                    var rdomain = this.GetMappedDomainByType(coldomain.ParentType());
                    var dmlResponse = rdomain.DeleteSelf();
                    if (dmlResponse.HasData == false) {
                        return dmlResponse;
                    }
                }
            }
            return new HDSResponse(this) {
                LogSource = SDbParams.LogSource()
            };
        }

        [JsonIgnore]
        public IEnumerable<(MemberInfo domain, MemberInfo aggregate, Type binder)> Mappings {
            get {
                return (GetBuilder() as AGViewBuilder<TAggregate>).Mappings.ToArray();
            }
        }

        public bool Equals(IAGBase other) {
            return this.MainDomain.Equals(other.MainDomain)
                && this.ResourceName == other.ResourceName
                || (this.ID != 0 && this.ID == other.ID);
        }

    }



    public static class AGBaseExtensions {
        public static IHDSResponse InsertUs<TAggregate>(this IEnumerable<TAggregate> self, Action<int, IHDSResponse> cursor = null)
            where TAggregate : class, IAGBase {
            var list = new List<dynamic>();
            self.EachIf((i, item) => {
                var response = item.InsertSelf();
                if (response.HasData) {
                    list.Add(response.Single);
                    cursor?.Invoke(i, response);
                } else if (response.HasErrors) {
                    cursor?.Invoke(i, response);
                    return false;
                }
                return true;
            });
            return new HDSResponse(list);
        }
    }

}