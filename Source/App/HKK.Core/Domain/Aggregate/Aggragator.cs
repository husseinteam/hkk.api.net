﻿
using HKK.Core.Domain.Data;
using HKK.Core.Api.CoreModel.Decorators;
using HKK.Core.Extensions.Static;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace HKK.Core.Domain.Aggregate {

    internal class Aggragator<T> : IAggragator<T>, ISelectedAggragator<T>, IWheredAggragator<T>, IConfinedAggragator<T>
        where T : AGBase<T> {

        private List<Expression> _SelectList = new List<Expression>();
        private Expression _DistinctColumn;
        private Queue<KeyValuePair<ExpressionType, BinaryExpression>> _WhereQueue =
            new Queue<KeyValuePair<ExpressionType, BinaryExpression>>();
        private Queue<(string name, object value)> Selectors = new Queue<(string name, object value)>();
        public Aggragator() {

            DataTools = SDbParams.DataTools;

        }

        private IDataTools DataTools;

        public ISelectedAggragator<T> SelectColumns<TProp>(
             Expression<Func<T, TProp>> column) {

            _SelectList.Add(column.Body);
            return this;

        }

        public ISelectedAggragator<T> SelectAllColumns() {

            _SelectList.Clear();
            return this;

        }

        public long SelectCount() {
            var entitySchemaBuilder = Activator.CreateInstance(typeof(T)).As<IAGBase>().GetSchemaBuilder();
            var commandText = $"SELECT COUNT(*) FROM {entitySchemaBuilder.GetFormatted()};";
            using (var engine = DataTools.GenerateEngine().AlterCommand(commandText)) {
                Parameterize(engine);
                engine.ExecuteScalarSync(out var c);
                return c;
            }
        }

        public IAggragator<T> SetDistinctColumn<TProp>(Expression<Func<T, TProp>> column) {

            _DistinctColumn = column.Body;
            return this;

        }

        public IWheredAggragator<T> Where(Expression<Func<T, bool>> selector) {

            _WhereQueue.Enqueue(new KeyValuePair<ExpressionType, BinaryExpression>(ExpressionType.And, selector.Body as BinaryExpression));
            return this;

        }

        public IWheredAggragator<T> WhereDynamic(MemberInfo aggregateProp, object targetValue) {
            Expression<Func<object, bool>> body =
                (obj) => aggregateProp.GetValue(obj).ToString().ToLowerInvariant() == targetValue.ToString().ToLowerInvariant();
            _WhereQueue.Enqueue(new KeyValuePair<ExpressionType, BinaryExpression>(ExpressionType.And, body.Body as BinaryExpression));
            return this;
        }

        public IConfinedAggragator<T> AndAlso(Expression<Func<T, bool>> selector) {

            _WhereQueue.Enqueue(new KeyValuePair<ExpressionType, BinaryExpression>(ExpressionType.AndAlso, selector.Body as BinaryExpression));
            return this;

        }

        public IConfinedAggragator<T> OrElse(Expression<Func<T, bool>> selector) {

            _WhereQueue.Enqueue(new KeyValuePair<ExpressionType, BinaryExpression>(ExpressionType.OrElse, selector.Body as BinaryExpression));
            return this;

        }

        public async Task ExecuteList(Action<T> cursor, Action noneback, Action<Exception> failback = null) {

            try {
                var commandText = this.ToString();
                using (var engine = DataTools.GenerateEngine().AlterCommand(commandText)) {
                    Parameterize(engine);
                    using (var reader = await engine.ReadCommand()) {
                        var exists = false;
                        while (reader?.Read() == true) {
                            exists = true;
                            var et = ExtractFrom(reader);
                            cursor(et);
                        }
                        if (exists == false) {
                            noneback();
                        }
                    }
                }
            } catch (Exception ex) {
                failback?.Invoke(ex);
            }

        }

        public void ExecuteListSync(Action<T> cursor, Action noneback, Action<Exception> failback = null) {

            try {
                var commandText = this.ToString();
                using (var engine = DataTools.GenerateEngine().AlterCommand(commandText)) {
                    Parameterize(engine);
                    using (var reader = engine.ReadCommandSync()) {
                        var exists = false;
                        while (reader?.Read() == true) {
                            exists = true;
                            var et = ExtractFrom(reader);
                            cursor(et);
                        }
                        if (exists == false) {
                            noneback();
                        }
                    }
                }
            } catch (Exception ex) {
                failback?.Invoke(ex);
            }

        }

        public async Task ExecuteSingle(Action<T> cursor, Action noneback, Action<Exception> failback = null) {

            try {
                var commandText = this.ToString();
                using (var engine = DataTools.GenerateEngine().AlterCommand(commandText)) {
                    Parameterize(engine);
                    using (var reader = await engine.ReadCommand()) {
                        if (reader?.Read() == true) {
                            var et = ExtractFrom(reader);
                            cursor(et);
                        } else {
                            noneback();
                        }
                    }
                }
            } catch (Exception ex) {
                failback?.Invoke(ex);
            }

        }

        public void ExecuteSingleSync(Action<T> cursor, Action noneback, Action<Exception> failback = null) {

            try {
                var commandText = this.ToString();
                using (var engine = DataTools.GenerateEngine().AlterCommand(commandText)) {
                    Parameterize(engine);
                    using (var reader = engine.ReadCommandSync()) {
                        if (reader?.Read() == true) {
                            var et = ExtractFrom(reader);
                            cursor(et);
                        } else {
                            noneback();
                        }
                    }
                }
            } catch (Exception ex) {
                failback?.Invoke(ex);
            }

        }

        private void Parameterize(IDataEngine engine) {
            while (Selectors.Count > 0) {
                var mpp = Selectors.Dequeue();
                engine.AddWithValue(mpp.name, mpp.value);
            }
        }

        public override string ToString() {

            var entitySchemaBuilder = Activator.CreateInstance(typeof(T)).As<IAGBase>().GetSchemaBuilder();
            var selectList = "{0}.*".Puts(entitySchemaBuilder.GetFormatted());
            var viewAndSchema = entitySchemaBuilder.GetFormatted();
            if (_SelectList.Count > 0) {
                if (_DistinctColumn == null) {
                    selectList = _SelectList.Select(se => "{0}.[{1}]".Puts(
                        viewAndSchema, se.ExposeMember().Name))
                        .Aggregate((p, n) => p + ", " + n);
                } else {
                    selectList = _SelectList.Where(se => se.ExposeMember().Name != _DistinctColumn.ExposeMember().Name)
                        .Select(se => "{0}.[{1}]".Puts(viewAndSchema,
                            se.ExposeMember().Name))
                        .Aggregate((p, n) => p + ", " + n);
                    selectList.Insert(0, "DISTINCT {0}.[{1}], ".Puts(viewAndSchema,
                            _DistinctColumn.ExposeMember().Name));
                }
            } else {
                if (_DistinctColumn != null) {
                    selectList = selectList.Insert(0, "DISTINCT {0}.[{1}], ".Puts(viewAndSchema, _DistinctColumn.ExposeMember().Name));
                }
            }
            var str = new StringBuilder("SELECT {0} FROM {1} WHERE 1=1".Puts(selectList, viewAndSchema));

            Action<BinaryExpression, StringBuilder> reducer = null;
            reducer = (be, sb) => {
                var name = "";
                if (be.Left is BinaryExpression lbinary) {
                    reducer(lbinary, sb);
                    str.AppendFormat(" {0} ", be.NodeTypeString() == "&&" ? "AND" : "OR");
                } else if (be.Left is UnaryExpression lunary) {
                    name = lunary.ExposeMember().Name;
                    str.AppendFormat("{0}.{1}", entitySchemaBuilder.GetFormatted(),
                        SDbParams.NameFormatter.Puts(name));
                    str.AppendFormat(" {0} ", be.NodeTypeString());
                } else if (be.Left is MemberExpression lmember) {
                    var prop = lmember.ExposeMember();
                    if (lmember.ExposeMember().Name == "IDProperty") {
                        var props = typeof(T).GetProperties(BindingFlags.Instance | BindingFlags.Public)
                        .Where(p => p.GetCustomAttribute<IDLocatorAttribute>() != null);
                        if (props.Count() > 1) {
                            throw new InvalidOperationException("Multiple IDPropertyLocator Specified");
                        } else if (props.Count() == 0) {
                            throw new InvalidOperationException("None of Multiple IDPropertyLocator Specified");
                        }
                        prop = props.First();
                    }
                    name = prop.Name;
                    str.AppendFormat("{0}.{1} {2} ",
                        entitySchemaBuilder.GetFormatted(),
                        SDbParams.NameFormatter.Puts(name),
                        be.NodeTypeString());
                    var result = lmember.Resultify();
                    str.AppendFormat("{1}{0}{1}", result, lmember.ExposeType().IsNumericType() ? "" : (result.IsAssigned() ? "'" : ""));
                } else if (be.Left is MethodCallExpression mcall) {
                    Expression root = mcall;
                    while (root is MethodCallExpression rootmc && rootmc.Object != null) {
                        root = rootmc.Object;
                    }
                    var member = (root as MethodCallExpression).Arguments.First();
                    var viewScheme = Activator.CreateInstance(member.ExposeType()).As<IAGBase>().GetSchemaBuilder();
                    name = member.Resultify().As<MemberInfo>()?.Name;
                    str.AppendFormat("LOWER({0}.{1}) {2} ",
                        viewScheme.GetFormatted(), SDbParams.NameFormatter.Puts(name), be.NodeTypeString());
                }
                if (be.Right is BinaryExpression rbinary) {
                    reducer(rbinary, sb);
                } else {
                    var result = be.Right.Resultify();
                    str.AppendFormat("{1}{0}", name, SDbParams.ParamKey);
                    Selectors.Enqueue((name: name, value: result));
                }
            };

            while (_WhereQueue.Count > 0) {
                var toq = _WhereQueue.Dequeue();
                var operand = "AND";
                if (toq.Key == ExpressionType.OrElse) {
                    operand = "OR";
                }
                str.AppendFormat(" {0} ", operand);
                reducer(toq.Value, str);
            }
            return str.ToString();

        }

        private static T ExtractFrom(System.Data.IDataReader reader) {
            var et = Activator.CreateInstance<T>() as T;
            foreach (var p in typeof(T).GetProperties()) {
                if (reader.HasColumn(p.Name)) {
                    if (!p.HasAttribute<ComputedFieldAttribute>()) {
                        var value = reader[p.Name];
                        if (!value.GetType().Equals(typeof(System.DBNull))) {
                            if (p.PropertyType.IsEnum) {
                                p.SetValue(et, Enum.Parse(p.PropertyType, value.ToString()));
                            } else if (p.PropertyType.IsConvertible()) {
                                p.SetValue(et, Convert.ChangeType(value, p.PropertyType));
                            } else {
                                p.SetValue(et, reader[p.Name]);
                            }
                        }
                    }
                }
            }

            return et;
        }

    }

}