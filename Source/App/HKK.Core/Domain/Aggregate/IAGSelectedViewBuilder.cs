﻿
using HKK.Core.Domain.Objects;
using System;

namespace HKK.Core.Domain.Aggregate {

    public interface IAGSelectedViewBuilder<T> 
        where T : AGBase<T> {

        IJoinBuilder<TEntity, T> InnerJoin<TEntity>(Action<IAGSelectList<TEntity, T>> selectCursor = null)
            where TEntity : DOBase<TEntity>;

        IJoinBuilder<TEntity, T> OuterJoin<TEntity>(Action<IAGSelectList<TEntity, T>> selectCursor = null)
            where TEntity : DOBase<TEntity>;

    }

}