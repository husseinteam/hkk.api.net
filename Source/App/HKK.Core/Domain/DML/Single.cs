﻿using HKK.Core.Domain.Data;
using HKK.Core.Domain.Enums;
using HKK.Core.Domain.Objects;
using HKK.Core.Extensions.Static;

using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace HKK.Core.Domain.DML {
    public class Single<TEntity> : ISingle<TEntity>
        where TEntity : DOBase<TEntity> {

        private List<KeyValuePair<Type, object>> _Dict = new List<KeyValuePair<Type, object>>();
        private bool AutoIdentity { get; set; }
        protected readonly TEntity entity;

        public Single(TEntity entity, List<KeyValuePair<Type, object>> dict = null) {
            this.entity = entity;
            if (dict != null) {
                _Dict.InsertRange(0, dict);
            }
            _Dict.Add(new KeyValuePair<Type, object>(typeof(TEntity), this));
            AutoIdentity = true;
        }

        public ISibling<TEntity> Sibling() {

            return new Sibling<TEntity>(InsertIfNotExistsSync() as TEntity);

        }

        public void ExecuteMany(Action<IExecutedResultList> cursor, Action<Exception> fallback = null) {

            var entli = new List<object>();
            foreach (var chkv in _Dict) {
                var child = chkv.Value as ISingle<TEntity>;
                var e = child.InsertIfNotExists();
                entli.Add(e);
            }
            cursor(new ExecutedResultList(entli.ToArray()));

        }

        public ISingle<TSingle> Cascade<TSingle>(Func<IExecutedResult<TEntity>, TSingle> cursor)
            where TSingle : DOBase<TSingle> {

            return new Single<TSingle>(cursor(new ExecutedResult<TEntity>(InsertIfNotExistsSync() as TEntity)), _Dict);

        }

        public ISingle<TEntity> SetAutoIdentity(bool on = true) {
            AutoIdentity = on;
            return this;
        }

        public HDSResponse InsertIfNotExistsSync() {
            HDSResponse response;
            if (ExistsSync(out var existingID)) {
                entity.ID = entity.ID > 0 ? entity.ID : existingID;
                response = new HDSResponse(entity);
                response.EntityID = existingID;
            } else {
                GenerateInsertEngine(out var engine);
                try {
                    engine.ExecuteCommandSync(out var rowc);
                    if (rowc == 1) {
                        engine.IdentifyCommand(entity.SchemaBuilder.GetFormatted()).ExecuteScalarSync(out var eid);
                        entity.ID = eid;
                        response = new HDSResponse(entity) { LogSource = SDbParams.LogSource() };
                    } else {
                        response = new HDSResponse(engine.Fault) { LogSource = SDbParams.LogSource() };
                    }
                } catch (Exception ex) {
                    response = new HDSResponse(ex) { LogSource = SDbParams.LogSource() };
                } finally {
                    engine.Dispose();
                }
            }
            return response;
        }

        public async Task<HDSResponse> InsertIfNotExists() {
            HDSResponse response;
            if (ExistsSync(out var existingID)) {
                entity.ID = entity.ID > 0 ? entity.ID : existingID;
                response = new HDSResponse(entity);
                response.EntityID = existingID;
            } else {
                GenerateInsertEngine(out var engine);
                try {
                    var rowc = await engine.ExecuteCommand();
                    if (rowc == 1) {
                        engine.IdentifyCommand(entity.SchemaBuilder.GetFormatted()).ExecuteScalarSync(out var eid);
                        entity.ID = eid;
                        response = new HDSResponse(entity) { LogSource = SDbParams.LogSource() };
                    } else {
                        response = new HDSResponse(engine.Fault) { LogSource = SDbParams.LogSource() };
                    }
                } catch (Exception ex) {
                    response = new HDSResponse(ex) { LogSource = SDbParams.LogSource() };
                } finally {
                    engine.Dispose();
                }
            }
            return response;
        }

        public HDSResponse UpdateSync() {
            HDSResponse response;
            GenerateUpdateEngine(out var engine);
            try {
                engine.ExecuteCommandSync(out var rowc);
                response = new HDSResponse(entity);
            } catch (Exception ex) {
                response = new HDSResponse(ex) { LogSource = SDbParams.LogSource() };
            } finally {
                engine.Dispose();
            }
            return response;
        }

        public async Task<HDSResponse> Update() {
            HDSResponse response;
            GenerateUpdateEngine(out var engine);
            try {
                await engine.ExecuteCommand();
                response = new HDSResponse(entity);
            } catch (Exception ex) {
                response = new HDSResponse(ex) { LogSource = SDbParams.LogSource() };
            } finally {
                engine.Dispose();
            }
            return response;
        }

        private void GenerateUpdateEngine(out IDataEngine engine) {
            var assignedFields = this.entity.GetFields(EResolveBy.Assigned);
            var script = UpdateToString(assignedFields).ToString();
            engine = SDbParams.DataTools.GenerateEngine().AlterCommand(script);

            var map = assignedFields.Select(f => new KeyValuePair<string, object>(f.Name, Convert.ChangeType(f.GetValue(this.entity), f.GetMemberType())));
            foreach (var m in map) {
                engine.AddWithValue($"{SDbParams.ParamKey}{m.Key}", m.Value);
            }
        }


        private StringBuilder UpdateToString(IEnumerable<MemberInfo> assignedFields) {
            this.entity.UpdatedAt = DateTime.Now;
            var updateString = new StringBuilder("UPDATE #table# SET #sets# WHERE #where#");
            var pkli = (entity as IDOBase).GetFields(EResolveBy.PKs);
            updateString
                .Replace("#table#", this.entity.SchemaBuilder.GetFormatted())
                .Replace("#where#", pkli.Select(pk =>
                    "{0}={1}{2}".Puts(SDbParams.NameFormatter.Puts(pk.Name), SDbParams.ParamKey, pk.Name))
                        .Aggregate((prev, next) => "{0}, {1}".Puts(prev, next)))
                .Replace("#sets#", assignedFields.Except(pkli).Select(up =>
                    "{0}={1}{2}".Puts(SDbParams.NameFormatter.Puts(up.Name), SDbParams.ParamKey, up.Name))
                        .Aggregate((prev, next) => "{0}, {1}".Puts(prev, next)));
            return updateString;
        }

        private void GenerateInsertEngine(out IDataEngine engine) {
            GenerateInsertBuilder(out var insertClause, out var autoGeneratedBuilders, out var fields);

            engine = SDbParams.DataTools.GenerateEngine().AlterCommand(insertClause.ToString());
            fields = fields.Except(autoGeneratedBuilders.Select(agb => agb.Prop));
            foreach (var field in fields) {
                if (field.GetMemberType().IsEnum) {
                    engine.AddWithValue(field.Name, (int)field.GetValue(entity));
                } else if (field.GetMemberType().GetInterfaces().Any(inf => inf.Equals(typeof(IConvertible)))) {
                    engine.AddWithValue(field.Name, Convert.ChangeType(field.GetValue(entity), field.GetMemberType()));
                } else {
                    engine.AddWithValue(field.Name, field.GetValue(entity));
                }
            }
            foreach (var agbuilder in autoGeneratedBuilders) {
                engine.AddWithValue($"{SDbParams.ParamKey}{agbuilder.Prop.Name}",
                    agbuilder.GetGenerated(agbuilder.Prop.GetMemberType()));
            }
        }
        private bool ExistsSync(out long existingID) {
            GenerateExistsEngine(out var engine);
            try {
                var reader = engine.ReadCommandSync();
                return (existingID = reader.Read() ? reader["ID"].ToInt64() : 0) > 0;
            } catch (Exception ex) {
                throw ex;
            } finally {
                engine.Dispose();
            }
        }

        private void GenerateExistsEngine(out IDataEngine engine) {
            var uqs = (entity as IDOBase).GetFields(EResolveBy.UQs);
            var keys = uqs.Count() > 0 ? uqs : (entity as IDOBase).GetFields(EResolveBy.PKs).Union(uqs);
            var whereClause = keys
                .Select(key => $" AND {SDbParams.NameFormatter.Puts(key.Name)}={SDbParams.ParamKey}{key.Name}")
                .Aggregate("WHERE 1=1", (prv, nxt) => prv + nxt);
            string existRepr = "";
            switch (SDbParams.CurrentServerType) {
                case EServerType.Unset:
                    throw new MissingFieldException("CurrentServerType not set");
                case EServerType.MSSql:
                    existRepr = $"SELECT TOP 1 ID FROM {entity.SchemaBuilder.GetFormatted()} {whereClause}";
                    break;
                case EServerType.MySql:
                    existRepr = $"SELECT ID FROM {entity.SchemaBuilder.GetFormatted()} {whereClause} LİMİT 1";
                    break;
            }
            engine = SDbParams.DataTools.GenerateEngine().AlterCommand(existRepr);

            var values = keys.Select(f => (
                name: f.Name,
                value: Convert.ChangeType(f.GetValue(this.entity), f.GetMemberType()))
            );
            foreach (var (name, value) in values) {
                engine.AddWithValue($"{SDbParams.ParamKey}{name}", value);
            }
        }
        private void GenerateInsertBuilder(out StringBuilder insertClause, out IEnumerable<DOPropBuilder> autoGeneratedBuilders, out IEnumerable<MemberInfo> fields) {
            this.entity.InsertedAt = DateTime.Now;
            insertClause = new StringBuilder("INSERT INTO #table#(#fields#) VALUES(#values#)");
            var pkli = (entity as IDOBase).GetFields(EResolveBy.PKs);
            autoGeneratedBuilders = (entity as IDOBase).GetBuilders().Where(p => p.AutoGenerated);
            fields = (entity as IDOBase).GetFields(EResolveBy.Assigned).Except(pkli);
            insertClause
                .Replace("#table#", (entity as IDOBase).SchemaBuilder.GetFormatted())
                .Replace("#fields#", SDbParams.DataTools.ParseFields(fields))
                .Replace("#values#", SDbParams.DataTools.ParseParams(fields));
        }

    }
}