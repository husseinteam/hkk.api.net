﻿
using System;
using System.Collections.Generic;

namespace HKK.Core.Domain.DML {

    public interface IEntityDictionary {

        List<KeyValuePair<Type, object>> Dict { get; }

    }

}