﻿
using HKK.Core.Domain.Data;
using HKK.Core.Domain.Enums;
using HKK.Core.Domain.Objects;
using HKK.Core.Extensions.Static;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace HKK.Core.Domain.DML {

    class Sibling<TEntity> : ISibling<TEntity>
        where TEntity : DOBase<TEntity> {

        protected readonly TEntity entity;
        private List<KeyValuePair<Type, object>> chainedEntities = new List<KeyValuePair<Type, object>>();

        private IDataTools DataTools { get; set; }

        public Sibling(TEntity entity) {
            this.entity = entity;
            this.DataTools = SDbParams.DataTools;
        }

        public ISibling<TEntity> Next<TOther>(Func<IExecutedResult<TEntity>, TOther> cursor)
            where TOther : DOBase<TOther> {

            var other = cursor(new ExecutedResult<TEntity>(entity));
            chainedEntities.Add(new KeyValuePair<Type, object>(typeof(TOther), other));
            return this;

        }

        public TEntity Execute(Action<IExecutedResultList> cursor) {

            var celi = new List<object>();
            foreach (var chained in chainedEntities) {
                IDOBase ce = (IDOBase)Convert.ChangeType(chained.Value, chained.Key);
                var insertClause = new StringBuilder("INSERT INTO #table#(#fields#) VALUES(#values#)");
                var fields = ce.GetFields(EResolveBy.Assigned).Where(f => f.Name != "ID");
                insertClause
                    .Replace("#table#", ce.SchemaBuilder.GetFormatted())
                    .Replace("#fields#", DataTools.ParseFields(fields))
                    .Replace("#values#", DataTools.ParseParams(fields));

                using (var engine = DataTools.GenerateEngine().AlterCommand(insertClause.ToString())) {
                    foreach (var field in fields) {
                        if (field.GetMemberType().IsEnum) {
                            engine.AddWithValue(field.Name, (int)field.GetValue(ce));
                        } else if (field.GetMemberType().GetInterfaces().Any(inf => inf.Equals(typeof(IConvertible)))) {
                            engine.AddWithValue(field.Name, Convert.ChangeType(field.GetValue(ce), field.GetMemberType()));
                        } else {
                            engine.AddWithValue(field.Name, field.GetValue(ce));
                        }
                    }
                    engine.ExecuteCommandSync(out var rowc);
                    if (rowc == 1) {
                        engine.IdentifyCommand(entity.SchemaBuilder.GetFormatted());
                        engine.ExecuteScalarSync(out var ceid);
                        ce.ID = ceid;
                        celi.Add(ce);
                    } else {
                        return entity;
                    }
                }
            }
            cursor(new ExecutedResultList(celi.ToArray()));
            return entity;

        }

    }

}