﻿using HKK.Core.Domain.Objects;
using System;
using System.Collections.Generic;

namespace HKK.Core.Domain.DML {

    public interface IExecutedResult<TEntity> where TEntity : DOBase<TEntity> {

        TEntity Output { get; }
        int Index { get; }
    }

    public interface IExecutedResultList {

        void Parse<TEntity>(Action<IExecutedResult<TEntity>> cursor) 
            where TEntity : DOBase<TEntity>;
        IEnumerable<IExecutedResult<TEntity>> Output<TEntity>() 
            where TEntity : DOBase<TEntity>;
        TEntity FirstOccurrance<TEntity>() 
            where TEntity : DOBase<TEntity>;
    }

}