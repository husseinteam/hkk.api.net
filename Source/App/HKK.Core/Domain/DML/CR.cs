﻿using HKK.Core.Domain.Objects;

namespace HKK.Core.Domain.DML {

    public static class CR {

		public static ISingle<TEntity> Single<TEntity>(TEntity entity)
			where TEntity : DOBase<TEntity> {
			return new Single<TEntity>(entity);
		}

	}

}
