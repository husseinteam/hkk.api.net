﻿using HKK.Core.Domain.Objects;
using System;

namespace HKK.Core.Domain.DML {

    public interface ISibling<TEntity>
        where TEntity : DOBase<TEntity> {

        ISibling<TEntity> Next<TOther>(Func<IExecutedResult<TEntity>, TOther> cursor) where TOther : DOBase<TOther>;
        TEntity Execute(Action<IExecutedResultList> cursor);

    }

}