﻿using HKK.Core.Domain.Objects;
using HKK.Core.Extensions.Static;
using System;
using System.Collections.Generic;
using System.Linq;

namespace HKK.Core.Domain.DML {

    internal class ExecutedResult<TEntity> : IExecutedResult<TEntity>
        where TEntity : DOBase<TEntity> {

        private readonly TEntity entity;
        private readonly int index;

        public ExecutedResult(TEntity entity, int index = 0) {
            this.entity = entity;
            this.index = index;
        }

        public int Index {
            get {
                return index;
            }
        }

        public TEntity Output { get { return this.entity; } }

    }

    internal class ExecutedResultList : IExecutedResultList {

        private object[] entities;

        public ExecutedResultList(object[] entities) {
            this.entities = entities;
        }

        public void Parse<TEntity>(Action<IExecutedResult<TEntity>> cursor) where TEntity : DOBase<TEntity> {

            var i = 0;
            foreach (var item in this.entities.Where(e => (e as TEntity) != null)) {
                if (typeof(TEntity).IsConvertible()) {
                    var entity = Convert.ChangeType(item, typeof(TEntity)) as TEntity;
                    cursor(new ExecutedResult<TEntity>(entity, i++));
                } else {
                    var entity = item as TEntity;
                    cursor(new ExecutedResult<TEntity>(entity, i++));
                }
            }

        }

        public IEnumerable<IExecutedResult<TEntity>> Output<TEntity>() where TEntity : DOBase<TEntity> {

            var i = 0;
            var li = new List<ExecutedResult<TEntity>>();
            foreach (var item in this.entities.Where(e => (e as TEntity) != null)) {
                if (typeof(TEntity).GetInterfaces().Any(inf => inf.Equals(typeof(IConvertible)))) {
                    var entity = Convert.ChangeType(item, typeof(TEntity)) as TEntity;
                    li.Add(new ExecutedResult<TEntity>(entity, i++));
                } else {
                    var entity = item as TEntity;
                    li.Add(new ExecutedResult<TEntity>(entity, i++));
                }
            }
            return li.ToArray();

        }

        public TEntity FirstOccurrance<TEntity>() where TEntity : DOBase<TEntity> {
            return Output<TEntity>().First().Output;
        }
    }

}