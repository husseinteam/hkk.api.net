﻿using HKK.Core.Domain.Objects;
using System;
using System.Threading.Tasks;

namespace HKK.Core.Domain.DML {

    public interface ISingle<TEntity>
        where TEntity : DOBase<TEntity> {

        ISingle<TEntity> SetAutoIdentity(bool on = true);
        ISingle<TOther> Cascade<TOther>(Func<IExecutedResult<TEntity>, TOther> cursor)
            where TOther : DOBase<TOther>;
        ISibling<TEntity> Sibling();

        void ExecuteMany(Action<IExecutedResultList> cursor, Action<Exception> fallback = null);

        Task<HDSResponse> InsertIfNotExists();
        HDSResponse InsertIfNotExistsSync();

        Task<HDSResponse> Update();
        HDSResponse UpdateSync();

    }

}