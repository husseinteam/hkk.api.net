﻿using HKK.Core.Domain.Aggregate;
using HKK.Core.Domain.Contracts;
using HKK.Core.Domain.Data;
using HKK.Core.Domain.Objects;
using HKK.Core.Engines.Core;
using HKK.Core.Extensions.Static;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using HKK.Core.Domain.Enums;
using System.Data;
using HKK.Core.Api.Static;

namespace HKK.Core.Engines {
    public static class SDataEngine {

        internal static List<IDOBase> GeneratedDomains { get; private set; }
        internal static List<IAGBase> GeneratedAggregates { get; private set; }

        public static IDOEngine<TEntity> GenerateDOEngine<TEntity>()
            where TEntity : DOBase<TEntity> {
            return new DOEngine<TEntity>();
        }

        public static bool SchemaContainsTable(string currentSchema) {
            var script = "";
            switch (SDbParams.CurrentServerType) {
                case EServerType.MySql:
                    script = @"SELECT COUNT(*) FROM `information_schema`.`tables` WHERE `table_schema` = '{0}' and `table_type`  = 'BASE TABLE';".Puts(currentSchema);
                    break;
                case EServerType.MSSql:
                    script = @"SELECT COUNT(*) FROM sys.tables WHERE type_desc = 'USER_TABLE';".Puts(currentSchema);
                    break;
                default:
                    throw new ArgumentException("Unsupported Server Type");
            }
            using (var engine = SDbParams.DataTools.GenerateEngine()) {
                engine.AlterCommand(script).ExecuteScalarSync(out var tablecount);
                return tablecount > 0;
            }
        }

        public static IAGEngine<TAggregate> GenerateAGEngine<TAggregate>()
            where TAggregate : AGBase<TAggregate> {
            return new AGEngine<TAggregate>();
        }

        public static IGenericEngine GenerateDOEngine(Type domainType) {
            return Activator.CreateInstance(typeof(DOEngine<>).MakeGenericType(domainType)).As<IGenericEngine>();
        }

        public static IGenericEngine GenerateAGEngine(Type aggregateType) {
            return Activator.CreateInstance(typeof(AGEngine<>).MakeGenericType(aggregateType)).As<IGenericEngine>();
        }

        public static void BuildDomain(params AssemblyName[] assemblyNames) {
            WipeTablesAndViews();

            GenerateDomainObjects(assemblyNames);
            GeneratedDomains.ForEach(dobj => dobj.BuildTable());
            GeneratedDomains.ForEach(dobj => dobj.BuildConstraints());
            GeneratedAggregates.ForEach(aggobj => aggobj.BuildView());
            CreateIndexesOnForeignKeys();
        }

        public static void GenerateDomainObjects(params AssemblyName[] assemblyNames) {
            GeneratedDomains = new List<IDOBase>();
            GeneratedAggregates = new List<IAGBase>();
            foreach (var an in assemblyNames) {
                GenerateObjects(an);
            }
        }

        private static void GenerateObjects(AssemblyName assemblyName) {
            var assembly = Assembly.Load(assemblyName);
            var domainObjects = new List<IDOBase>();
            var aggregateObjects = new List<IAGBase>();
            foreach (var t in assembly.GetTypes().Where(t => t.IsClass && !t.IsGenericTypeDefinition && typeof(IDOBase).IsAssignableFrom(t))) {
                domainObjects.Add(Activator.CreateInstance(t) as IDOBase);
            }

            foreach (var t in assembly.GetTypes().Where(t => t.IsClass && !t.IsGenericTypeDefinition && typeof(IAGBase).IsAssignableFrom(t))) {
                aggregateObjects.Add(Activator.CreateInstance(t) as IAGBase);
            }
            GeneratedDomains.AddRange(domainObjects);
            GeneratedAggregates.AddRange(aggregateObjects);
        }

        private static void CreateIndexesOnForeignKeys() {
            var script = "";
            switch(SDbParams.CurrentServerType) {
                case EServerType.MSSql:
                    script = @"
DECLARE scripts_cursor CURSOR  
    FOR SELECT 'CREATE INDEX [IX_' + f.name + '] ON ' + SCHEMA_NAME(f.schema_id) + '.' + OBJECT_NAME(f.parent_object_id) + '([' + COL_NAME(fc.parent_object_id, fc.parent_column_id) + '])'
		FROM sys.foreign_keys AS f 
		INNER JOIN sys.foreign_key_columns AS fc ON f.object_id = fc.constraint_object_id;

DECLARE @script NVARCHAR(512);
OPEN scripts_cursor;
FETCH NEXT FROM scripts_cursor INTO @script;

WHILE @@FETCH_STATUS = 0  
BEGIN  
	EXECUTE sp_executesql @script;
	FETCH NEXT FROM scripts_cursor INTO @script;
END

CLOSE scripts_cursor;  
DEALLOCATE scripts_cursor;
";
                    break;
                case EServerType.MySql:
                    throw new InvalidOperationException("MySQL Not Supported For Supplying Foreign Key Indexes For All Tables");
                case EServerType.Unset:
                    throw new ArgumentException("Unsupported Server Type");

            }
            using (var engine = SDbParams.DataTools.GenerateEngine()) {
                engine.SwitchLoggingOff().AlterCommand(script).ExecuteCommandSync(out var rowc);
                if (engine.Fault != null) {
                    throw engine.Fault;
                }
            }
        }

        private static void WipeTablesAndViews() {
            var script = "";
            switch (SDbParams.CurrentServerType) {
                case EServerType.MySql:
                    script = @"
SET FOREIGN_KEY_CHECKS = 0;
SET GROUP_CONCAT_MAX_LEN=32768;
SET @views = NULL;
SELECT GROUP_CONCAT('`', TABLE_NAME, '`') INTO @views
  FROM information_schema.views
  WHERE table_schema = (SELECT DATABASE());
SELECT IFNULL(@views,'dummy') INTO @views;

SET @views = CONCAT('DROP VIEW IF EXISTS ', @views);
PREPARE stmt FROM @views;
EXECUTE stmt;
DEALLOCATE PREPARE stmt;

SET @tables = NULL;
SELECT GROUP_CONCAT('`', table_name, '`') INTO @tables
  FROM information_schema.tables
  WHERE table_schema = (SELECT DATABASE());
SELECT IFNULL(@tables,'dummy') INTO @tables;

SET @tables = CONCAT('DROP TABLE IF EXISTS ', @tables);
PREPARE stmt2 FROM @tables;
EXECUTE stmt2;
DEALLOCATE PREPARE stmt2;
SET FOREIGN_KEY_CHECKS = 1;
";
                    break;
                case EServerType.MSSql:
                    script = $@"
declare @n char(1)
set @n = char(10)

declare @stmt nvarchar(max)

-- views
select @stmt = isnull( @stmt + @n, '' ) + 
'drop view [' + sc.name + '].[' + vw.name + ']'
from sys.views vw
    inner join sys.schemas sc on vw.schema_id = sc.schema_id
    where vw.type = 'U'

-- foreign keys
select @stmt = isnull( @stmt + @n, '' ) +
    'alter table [' + f.CONSTRAINT_SCHEMA + '].[' + f.TABLE_NAME + '] drop constraint [' + f.CONSTRAINT_NAME + ']'
from information_schema.table_constraints f
where f.CONSTRAINT_TYPE = 'FOREIGN KEY'

-- primary keys
select @stmt = isnull( @stmt + @n, '' ) +
    'alter table [' + f.CONSTRAINT_SCHEMA + '].[' + f.TABLE_NAME + '] drop constraint [' + f.CONSTRAINT_NAME + ']'
from information_schema.table_constraints f
where f.CONSTRAINT_TYPE = 'PRIMARY KEY'

-- unique keys
select @stmt = isnull( @stmt + @n, '' ) +
    'alter table [' + f.CONSTRAINT_SCHEMA + '].[' + f.TABLE_NAME + '] drop constraint [' + f.CONSTRAINT_NAME + ']'
from information_schema.table_constraints f
where f.CONSTRAINT_TYPE = 'UNIQUE'

-- tables
select @stmt = isnull( @stmt + @n, '' ) +
    'drop table [' + sc.name + '].[' + t.name + ']'
from sys.tables t
    inner join sys.schemas sc on t.schema_id = sc.schema_id
    where t.type = 'USER TABLE'

use {SRPIValues.CurrentSchema}
exec sp_executesql @stmt
";
                    break;
                default:
                    throw new ArgumentException("Unsupported Server Type");
            }
            using (var engine = SDbParams.DataTools.GenerateEngine()) {
                engine.SwitchLoggingOff().AlterCommand(script).ExecuteCommandSync(out var rowc);
                if (engine.Fault != null) {
                    throw engine.Fault;
                }
            }
        }

    }
}
