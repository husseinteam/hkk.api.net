﻿using HKK.Core.Domain.Aggregate;
using HKK.Core.Domain.Contracts;
using HKK.Core.Domain.Objects;
using System;
using System.Linq.Expressions;
using System.Threading.Tasks;
using System.Reflection;
using System.Collections.Generic;
using System.Linq;
using HKK.Core.Domain.Data;

namespace HKK.Core.Engines.Core {
    internal class AGEngine<TAggregate> : AGSyncEngine<TAggregate>, IAGEngine<TAggregate>, IGenericEngine
        where TAggregate : AGBase<TAggregate> {

        public async Task<HDSResponse> SelectAll(Func<TAggregate, bool> filter = null) {
            HDSResponse response = null;
            var list = new List<TAggregate>();
            await AG<TAggregate>.Views().SelectAllColumns().ExecuteList((item) => {
                if (filter != null) {
                    if (filter(item)) list.Add(ItemResolver(item));
                } else {
                    list.Add(ItemResolver(item));
                }
            }, () => {}, (ex) => response = Failback(ex));
            response = new HDSResponse(list) { LogSource = SDbParams.LogSource(), Fault = response?.Fault };
            return response;
        }

        public async Task<HDSResponse> SelectBy(params Expression<Func<TAggregate, bool>>[] selectors) {
            HDSResponse response = null;
            var list = new List<TAggregate>();
            var confinement = AG<TAggregate>.Views().SelectAllColumns().Where(selectors.First());
            selectors.Skip(1).ToList().ForEach(sel => confinement.AndAlso(sel));
            await confinement.ExecuteList((item) => {
                list.Add(ItemResolver(item));
            }, () => {}, (ex) => response = Failback(ex));
            response = new HDSResponse(list) { LogSource = SDbParams.LogSource(), Fault = response?.Fault };
            
            return response;
        }

        public async Task<HDSResponse> SelectByID(long id) {
            HDSResponse response = null;
            var confinement = AG<TAggregate>.Views().SelectAllColumns().Where((agg) => agg.ID == id);
            await confinement.ExecuteSingle((item) => {
                response = new HDSResponse(ItemResolver(item)) { LogSource = SDbParams.LogSource() };
            }, () => {}, (ex) => response = Failback(ex));
            
            return response;
        }

        public async Task<HDSResponse> SelectByOne(Expression<Func<TAggregate, bool>> selector) {
            HDSResponse response = null;
            var confinement = AG<TAggregate>.Views().SelectAllColumns().Where(selector);
            await confinement.ExecuteSingle((item) => {
                response = new HDSResponse(ItemResolver(item)) { LogSource = SDbParams.LogSource() };
            }, () => {}, (ex) => response = Failback(ex));
            return response;
        }


        public async Task<HDSResponse> SelectSingle(Expression<Func<TAggregate, bool>> selector) {
            HDSResponse response = null;
            await AG<TAggregate>.Views().SelectAllColumns().Where(selector).ExecuteSingle((item) => {
                response = new HDSResponse(ItemResolver(item)) { LogSource = SDbParams.LogSource() };
            }, () => {}, (ex) => response = Failback(ex));
            
            return response;
        }

        public async Task<IHDSResponse> GenericSelectAll(Func<dynamic, bool> filter = null) {
            return await SelectAll(filter);
        }

        public IHDSResponse GenericSelectAllSync() {
            return SelectAllSync();
        }

        public async Task<IHDSResponse> GenericSelectByID(long id) {
            return await SelectByID(id);
        }

        public IHDSResponse GenericSelectByIDSync(long id) {
            return SelectByIDSync(id);
        }

        public Task<IHDSResponse> GenericDelete(long id) {
            throw new NotImplementedException();
        }

        public Task<IHDSResponse> GenericUpdateAggregate(IAGBase aggregate) {
            throw new NotImplementedException();
        }

        public IHDSResponse GenericSelectByComparison(MemberInfo targetProp, object targetValue) {
            return SelectByComparison(targetProp, targetValue);
        }

        public IHDSResponse GenericSelectAllByReferenceSync(long id, string referencedColumn) {
            return SelectAllByReferenceSync(id, referencedColumn);
        }
    }
}
