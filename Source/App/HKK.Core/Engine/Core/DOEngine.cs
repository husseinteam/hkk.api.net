﻿using HKK.Core.Domain.Aggregate;
using HKK.Core.Domain.Contracts;
using HKK.Core.Domain.DML;
using HKK.Core.Domain.Enums;
using HKK.Core.Domain.Objects;
using HKK.Core.Domain.Query;
using HKK.Core.Extensions.Static;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using System.Reflection;
using HKK.Core.Domain.Data;

namespace HKK.Core.Engines.Core {
    internal class DOEngine<TEntity> : DOSyncEngine<TEntity>, IDOEngine<TEntity>, IGenericEngine
        where TEntity : DOBase<TEntity> {

        public DOEngine(Func<Exception, HDSResponse> fallback)
            : base(fallback) {

        }
        public DOEngine() : base() {

        }
        public async Task<HDSResponse> Delete<TKey>(TKey key) {
            var id = long.Parse(key.ToString());
            HDSResponse response = null;
            await Q<TEntity>.SelectAllColumns().Where(x => x.ID == id).ExecuteOne(xq => {
                response = xq.Delete();
            }, () => response = new HDSResponse(new Exception($"Entity ID: {key} not found")) {
                LogSource = SDbParams.LogSource()
            });
            return response;
        }

        public async Task<HDSResponse> InsertIfNotExists(TEntity entity) {
            return await CR.Single(entity).InsertIfNotExists();
        }

        public async Task<HDSResponse> Update(TEntity entity) {
            return await CR.Single(entity).Update();
        }

        public async Task<IHDSResponse> Update(long id, Func<IUpdateClause<TEntity>, IUpdateClause<TEntity>> updater) {
            IHDSResponse result = null;
            await Q<TEntity>.SelectAllColumns().Where(u => u.ID == id).ExecuteOne(cursor => {
                result = updater(cursor.UpdateClause()).PersistUpdate();
            }, () => result = new HDSResponse(new Exception($"Entity with ID: {id} not found")) {
                LogSource = SDbParams.LogSource()
            });
            return result;
        }
        public async Task<IHDSResponse> UpdateAggregate(IAGBase aggregate) {
            HDSResponse result = null;
            IExecutedQuery<TEntity> execq = null;
            await Q<TEntity>.SelectAllColumns().Where(u => u.ID == aggregate.ID).ExecuteOne(cursor => {
                execq = cursor;
            }, 
            () => result = new HDSResponse(new Exception($"Entity with ID: {aggregate.ID} not found")) { LogSource = SDbParams.LogSource() });
            result = result ?? execq.UpdateClause().UpsertAggregate(aggregate);
            return result;
        }

        public async Task<HDSResponse> SelectAll(Func<TEntity, bool> filter = null) {
            HDSResponse response = null;
            var list = new List<TEntity>();
            await Q<TEntity>.SelectAllColumns().ExecuteMany((query) => {
                if (filter != null) {
                    if (filter(query.ResolvedEntity)) list.Add(query.ResolvedEntity);
                } else {
                    list.Add(query.ResolvedEntity);
                }
            });
            response = new HDSResponse(list) { LogSource = SDbParams.LogSource() };
            return response;
        }

        public async Task<HDSResponse> SelectSingle(Expression<Func<TEntity, bool>> selector) {
            HDSResponse response = null;
            TEntity entity = default(TEntity);
            var confinement = Q<TEntity>.SelectAllColumns().Where(selector);
            await confinement.ExecuteOne((query) => {
                entity = query.ResolvedEntity;
            });
            response = new HDSResponse(entity) { LogSource = SDbParams.LogSource() };
            return response;
        }

        public async Task<HDSResponse> SelectLastRecord<TKey>(Expression<Func<TEntity, TKey>> keySelector) {
            HDSResponse response = null;
            await Q<TEntity>.SelectAllColumns().Order().By(keySelector, EOrderBy.ASC).ExecuteMany((query) => {
                response = new HDSResponse(query.ResolvedEntity) { LogSource = SDbParams.LogSource() };
            });
            return response;
        }

        public async Task<HDSResponse> SelectSingleByID<TKey>(TKey key) {
            HDSResponse response = null;
            await Q<TEntity>.SelectAllColumns().Where(e => (object)e.ID == (object)key).ExecuteOne((query) => {
                response = new HDSResponse(query.ResolvedEntity) { LogSource = SDbParams.LogSource() };
            });
            return response;
        }

        public async Task<IHDSResponse> GenericSelectAll(Func<dynamic, bool> filter = null) {
            return await SelectAll(filter);
        }
        public IHDSResponse GenericSelectAllSync() {
            return SelectAllSync();
        }

        public async Task<IHDSResponse> GenericSelectByID(long id) {
            return await SelectSingleByID(id);
        }

        public IHDSResponse GenericSelectByIDSync(long id) {
            return SelectSingleByIDSync(id);
        }

        public async Task<IHDSResponse> GenericDelete(long id) {
            return await Delete(id);
        }

        public async Task<IHDSResponse> GenericUpdateAggregate(IAGBase aggregate) {
            return await UpdateAggregate(aggregate);
        }

        public IHDSResponse GenericSelectByComparison(MemberInfo targetProp, object targetValue) {
            return SelectByComparison(targetProp, targetValue);
        }

        public IHDSResponse GenericSelectAllByReferenceSync(long id, string referencedColumn) {
            throw new NotImplementedException();
        }
    }
}
