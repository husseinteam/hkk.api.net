﻿using HKK.Core.Domain.Aggregate;
using HKK.Core.Domain.Contracts;
using HKK.Core.Domain.Data;
using HKK.Core.Domain.Objects;
using HKK.Core.Api.CoreModel.Decorators;
using HKK.Core.Extensions.Static;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;

namespace HKK.Core.Engines.Core {
    internal class AGSyncEngine<TAggregate>
        where TAggregate : AGBase<TAggregate> {

        protected Func<TAggregate, TAggregate> ItemResolver;
        protected Func<Exception, HDSResponse> Failback { get; private set; }

        public AGSyncEngine() {
            Failback = (ex) => new HDSResponse(ex) {
                LogSource = SDbParams.LogSource()
            };
            ItemResolver = (agg) => {
                typeof(TAggregate).ResolveProperties().Where(a => a.HasAttribute<HavingCollectionAttribute>())
                    .Each((i, a) => {
                        var collProp = agg.GetPropertyInfo(a.Name);
                        var referencedType = collProp.PropertyType.GetGenericArguments().FirstOrDefault();
                        var referencedEngine =
                            Activator.CreateInstance(typeof(AGEngine<>).MakeGenericType(referencedType)) as IGenericEngine;
                        var collValue = referencedEngine.GenericSelectAllByReferenceSync(agg.ID, a.GetCustomAttribute<HavingCollectionAttribute>().InverseIDProperty);
                        if (collValue.HasData) {
                            collProp.SetValue(agg, collValue.Items.CastToList(referencedType));
                        }
                    });
                return agg;
            };
        }
        public HDSResponse SelectAllSync(Func<TAggregate, bool> filter = null) {
            HDSResponse response = null;
            var list = new List<TAggregate>();
            AG<TAggregate>.Views().SelectAllColumns().ExecuteListSync((item) => {
                if (filter != null) {
                    if (filter(item)) list.Add(ItemResolver(item));
                } else {
                    list.Add(ItemResolver(item));
                }
            }, () => {}, (ex) => response = Failback(ex));
            response = new HDSResponse(list) { LogSource = SDbParams.LogSource(), Fault = response?.Fault };
            return response;
        }

        public long SelectCount() {
            return AG<TAggregate>.Views().SelectCount();
        }

        public HDSResponse SelectBySync(params Expression<Func<TAggregate, bool>>[] selectors) {
            HDSResponse response = null;
            var list = new List<TAggregate>();
            var confinement = AG<TAggregate>.Views().SelectAllColumns().Where(selectors.First());
            selectors.Skip(1).ToList().ForEach(sel => confinement.AndAlso(sel));
            confinement.ExecuteListSync((item) => {
                list.Add(ItemResolver(item));
            }, () => {}, (ex) => response = Failback(ex));

            response = new HDSResponse(list) { LogSource = SDbParams.LogSource(), Fault = response?.Fault };
            return response;
        }

        public HDSResponse SelectByIDSync(long id) {
            HDSResponse response = null;
            var confinement = AG<TAggregate>.Views().SelectAllColumns().Where((agg) => agg.ID == id);
            confinement.ExecuteSingleSync((item) => {
                response = new HDSResponse(ItemResolver(item)) { LogSource = SDbParams.LogSource() };
            }, () => {}, (ex) => response = Failback(ex));
            
            return response;
        }

        public HDSResponse SelectByOneSync(Expression<Func<TAggregate, bool>> selector) {
            HDSResponse response = null;
            var confinement = AG<TAggregate>.Views().SelectAllColumns().Where(selector);
            confinement.ExecuteSingleSync((item) => {
                response = new HDSResponse(ItemResolver(item)) { LogSource = SDbParams.LogSource() };
            }, () => {}, (ex) => response = Failback(ex));
            
            return response;
        }


        public HDSResponse SelectSingleSync(Expression<Func<TAggregate, bool>> selector) {
            HDSResponse response = null;
            AG<TAggregate>.Views().SelectAllColumns().Where(selector).ExecuteSingleSync((item) => {
                response = new HDSResponse(ItemResolver(item)) { LogSource = SDbParams.LogSource() };
            }, () => {}, (ex) => response = Failback(ex));
            return response;
        }

        public IHDSResponse SelectByComparison(MemberInfo aggregateProp, object targetValue) {
            HDSResponse response = null;
            var list = new List<TAggregate>();
            AG<TAggregate>.Views().SelectAllColumns().WhereDynamic(aggregateProp, targetValue).ExecuteListSync((item) => {
                list.Add(ItemResolver(item));
            }, () => {}, (ex) => response = Failback(ex));
            return new HDSResponse(list);
        }

        public IHDSResponse SelectAllByReferenceSync(long id, string referencedColumn) {
            HDSResponse response = null;
            var list = new List<TAggregate>();
            var confinement = AG<TAggregate>.Views().SelectAllColumns();
            var aggregateProp = typeof(TAggregate).GetProperty(referencedColumn);
            confinement.WhereDynamic(aggregateProp, id).ExecuteListSync((item) => {
                list.Add(ItemResolver(item));
            }, () => {}, (ex) => response = Failback(ex));
            return new HDSResponse(list);
        }
    }
}
