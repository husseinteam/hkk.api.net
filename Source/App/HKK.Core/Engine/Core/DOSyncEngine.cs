﻿using HKK.Core.Domain.Aggregate;
using HKK.Core.Domain.Contracts;
using HKK.Core.Domain.Data;
using HKK.Core.Domain.DML;
using HKK.Core.Domain.Enums;
using HKK.Core.Domain.Objects;
using HKK.Core.Domain.Query;
using HKK.Core.Extensions.Static;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;

namespace HKK.Core.Engines.Core {
    internal class DOSyncEngine<TEntity>
        where TEntity : DOBase<TEntity> {

        protected readonly Func<Exception, HDSResponse> _Fallback;

        public DOSyncEngine(Func<Exception, HDSResponse> fallback) {
            _Fallback = fallback;
        }
        public DOSyncEngine() {
            _Fallback = (exc) => new HDSResponse(exc) {
                LogSource = SDbParams.LogSource()
            };
        }
        public HDSResponse DeleteSync<TKey>(TKey key) {

            var id = long.Parse(key.ToString());
            IExecutedQuery<TEntity> exq = null;
            HDSResponse response = null;
            var fault = AsyncExtensions.AwaitResult(() => Q<TEntity>.SelectAllColumns().Where(x => x.ID == id).ExecuteOne(xq => {
                exq = xq;
            }, () => response = new HDSResponse(new Exception($"Entity ID: {key} not found")) {
                LogSource = SDbParams.LogSource()
            }));
            response = exq?.Delete() ?? new HDSResponse(fault) { LogSource = SDbParams.LogSource() };
            return response;

        }

        public HDSResponse InsertSync(TEntity entity) {

            return CR.Single(entity).InsertIfNotExistsSync();

        }

        public HDSResponse UpdateSync(long id, Func<IUpdateClause<TEntity>, IUpdateClause<TEntity>> updater) {

            HDSResponse response = null;
            var fault = AsyncExtensions.AwaitResult(() => Q<TEntity>.SelectAllColumns().Where(u => u.ID == id)
            .ExecuteOne(cursor => {
                response = new HDSResponse(updater(cursor.UpdateClause()).PersistUpdate());
            }, () => response.ReplaceFault(new Exception($"Entity with ID: {id} not found")))
            );
            if (fault != null) {
                response.ReplaceFault(fault);
            }
            return response;

        }

        public IHDSResponse UpdateAggregateSync(IAGBase aggregate) {

            IHDSResponse response = null;
            var fault = AsyncExtensions.AwaitResult(() => Q<TEntity>.SelectAllColumns().Where(u => u.ID == aggregate.ID).ExecuteOne(cursor => {
                response = cursor.UpdateClause().UpsertAggregate(aggregate);
            }, () => response = new HDSResponse(new Exception($"Entity with ID: {aggregate.ID} not found")) {
                LogSource = SDbParams.LogSource()
            }));
            if (fault != null) {
                response.ReplaceFault(fault);
            }
            return response;

        }

        public HDSResponse SelectAllSync() {
            var list = new List<TEntity>();
            Q<TEntity>.SelectAllColumns().ExecuteMany((query) => {
                list.Add(query.ResolvedEntity);
            });
            var response = new HDSResponse(list) { LogSource = SDbParams.LogSource() };
            return response;
        }

        public HDSResponse SelectBySync(params Expression<Func<TEntity, bool>>[] selectors) {
            var list = new List<TEntity>();
            var confinement = Q<TEntity>.SelectAllColumns().Where(selectors.First());
            selectors.Skip(1).ToList().ForEach(sel => confinement = confinement.And(sel));
            var fault = confinement.ExecuteManySync((query) => {
                list.Add(query.ResolvedEntity);
            });
            var response = new HDSResponse(list) { LogSource = SDbParams.LogSource() };
            if (fault != null) {
                response.ReplaceFault(fault);
            }
            return response;
        }

        public HDSResponse SelectSingleSync(Expression<Func<TEntity, bool>> selector) {
            HDSResponse response = null;
            TEntity entity = null;
            var fault = Q<TEntity>.SelectAllColumns().Where(selector).ExecuteOneSync((query) => {
                entity = query.ResolvedEntity;
            });
            response = new HDSResponse(entity) { LogSource = SDbParams.LogSource() };
            if (fault != null) {
                response.ReplaceFault(fault);
            }
            return response;
        }
        public HDSResponse SelectLastRecordSync<TKey>(Expression<Func<TEntity, TKey>> keySelector) {
            HDSResponse response = null;
            TEntity entity = null;
            var fault = Q<TEntity>.SelectAllColumns().Order().By(keySelector, EOrderBy.ASC).ExecuteManySync((query) => {
                entity = query.ResolvedEntity;
            });
            response = new HDSResponse(entity) { LogSource = SDbParams.LogSource() };
            if (fault != null) {
                response.ReplaceFault(fault);
            }
            return response;
        }

        public HDSResponse SelectSingleByIDSync<TKey>(TKey key) {
            HDSResponse response = null;
            TEntity entity = null;
            var fault = Q<TEntity>.SelectAllColumns().Where(e => (object)e.ID == (object)key).ExecuteOneSync((query) => {
                entity = query.ResolvedEntity;
            });
            response = new HDSResponse(entity) { LogSource = SDbParams.LogSource() };
            if (fault != null) {
                response.ReplaceFault(fault);
            }
            return response;
        }

        public IHDSResponse SelectByComparison(MemberInfo targetProp, object targetValue) {
            HDSResponse response = null;
            var li = new List<TEntity>();
            var fault = Q<TEntity>.SelectAllColumns().WhereDynamic(targetProp, targetValue).ExecuteManySync((item) => {
                li.Add(item.ResolvedEntity);
            }, () => response = _Fallback(new Exception($"SelectByComparison Failed with {targetProp.Name} == {targetValue}")));
            response = new HDSResponse(li) { LogSource = SDbParams.LogSource() };
            if (fault != null) {
                response.ReplaceFault(fault);
            }
            return response;
        }
    }
}
