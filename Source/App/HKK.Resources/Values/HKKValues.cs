﻿using System;
namespace HKK.Resources.Values {
    public static class HKKValues {
        public static string DebugHost { get; } = "http://localhost:1980";
        public static string LocalhostConnectionString { get; } = $"Server=.; Database={LocalhostSchemaName}; User Id=hakkani; Password=Hkkn-293117;";
        public static string LocalhostSchemaName { get; } = "hakkanidb";
        public static string ProductionHost { get; } = "http://endp.ecmain.com";
        public static string RemoteConnectionString { get; } = "";
        public static string RemoteSchemaName { get; } = "hkkdb";
        public static string RPIKey { get; } = "rpi";
        public static string RPIVersion { get; } = "1";
    }
}
