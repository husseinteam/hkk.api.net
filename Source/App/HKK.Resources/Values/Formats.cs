﻿using System;
namespace HKK.Resources.Values {
    public static class Formats {
        public static string ComparableDateFormat { get; } = "{0:yyyy.MM.dd}";
        public static string DateFormat { get; } = "yyyy.MM.dd";
    }
}
