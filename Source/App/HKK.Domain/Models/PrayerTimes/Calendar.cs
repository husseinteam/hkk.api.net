﻿using HKK.Core.Domain.Objects;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HKK.Domain.Models.PrayerTimes {

    public class Calendar : DOBase<Calendar> {

        #region Properties

        public string Key { get; set; }
        public decimal Latitude { get; set; }
		public decimal Longitude { get; set; }
        public int Month { get; set; }
        public int Year { get; set; }

        #endregion

        public override string TextValue => $"{Latitude}, {Longitude}";

        protected override void Map(IDOTableBuilder<Calendar> builder) {
			builder.MapsTo(x => x.SchemaName("PYR").TableName("Calendars"));
			builder.For(x => x.Key).IsTypeOf(EDataType.String).HasMaxLength(255).IsRequired();
			builder.For(x => x.Latitude).IsTypeOf(EDataType.Spatial).IsRequired();
			builder.For(x => x.Longitude).IsTypeOf(EDataType.Spatial).IsRequired();
			builder.For(x => x.Month).IsTypeOf(EDataType.Int).IsRequired();
			builder.For(x => x.Year).IsTypeOf(EDataType.Int).IsRequired();

            builder.UniqueKey(x => x.Key);
            builder.UniqueKey(x => x.Month);
            builder.UniqueKey(x => x.Year);
		}
    }
}
