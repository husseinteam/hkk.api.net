﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HKK.Core.Domain.Objects;
using HKK.Domain.Models.PrayerTimes;

namespace HKK.Domain.Models.Forecasts {

	public class Forecast : DOBase<Forecast> {

        #region Properties
        public DateTime CurrentDate { get; set; }
        public string Summary { get; set; }
        public string IconCode { get; set; }
        public byte[] IconData { get; set; }
        public double WindSpeed { get; set; }
        public double TemperatureMin { get; set; }
        public double TemperatureMax { get; set; }

        public long CalendarID { get; set; }
        #endregion

        public override string TextValue => Summary;


        protected override void Map(IDOTableBuilder<Forecast> builder) {
            builder.MapsTo(x => x.SchemaName("FC").TableName("Forecasts"));
            builder.For(x => x.CurrentDate).IsTypeOf(EDataType.DateTime).IsRequired();
            builder.For(x => x.Summary).IsTypeOf(EDataType.String).HasMaxLength(255).IsRequired();
            builder.For(x => x.IconCode).IsTypeOf(EDataType.String).HasMaxLength(32).IsRequired();
            builder.For(x => x.IconData).IsTypeOf(EDataType.BinaryMax).IsRequired();
            builder.For(x => x.WindSpeed).IsTypeOf(EDataType.Spatial).IsRequired();
            builder.For(x => x.TemperatureMin).IsTypeOf(EDataType.Spatial).IsRequired();
            builder.For(x => x.TemperatureMax).IsTypeOf(EDataType.Spatial).IsRequired();

			builder.ForeignKey(t => t.CalendarID).References<Calendar>(t => t.ID);
        }

    }

}
