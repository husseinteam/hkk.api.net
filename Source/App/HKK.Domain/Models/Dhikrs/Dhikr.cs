﻿using HKK.Core.Domain.Objects;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HKK.Domain.Models.Dhikrs {
	public class Dhikr : DOBase<Dhikr> {

		#region Properties

		public string Title { get; set; }
		public string Transcript { get; set; }
		public string Arabic { get; set; }
        public int Target { get; set; }
        public int Current { get; set; }
        public int Order { get; set; }
        public long DhikrGroupID { get; set; }

        #endregion

        public override string TextValue => Title;

        protected override void Map(IDOTableBuilder<Dhikr> builder) {

			builder.MapsTo(x => x.SchemaName("DH").TableName("Dhikrs"));
			builder.For(x => x.Title).IsTypeOf(EDataType.String).HasMaxLength(128).IsRequired();
			builder.For(x => x.Transcript).IsTypeOf(EDataType.Text).IsRequired();
			builder.For(x => x.Arabic).IsTypeOf(EDataType.Text);
			builder.For(x => x.Target).IsTypeOf(EDataType.Int).IsRequired();
			builder.For(x => x.Current).IsTypeOf(EDataType.Int).IsRequired();
			builder.For(x => x.Order).IsTypeOf(EDataType.Int).IsRequired();

            builder.ForeignKey(t => t.DhikrGroupID).References<DhikrGroup>(t => t.ID);
        }
    }
}
