﻿using HKK.Core.Domain.Objects;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HKK.Domain.Models.Dhikrs {
	public class DhikrGroup : DOBase<DhikrGroup> {

		#region Properties

		public String Name { get; set; }
        public string Appeal { get; set; }
        public string Endowment { get; set; }

        #endregion

        public override string TextValue => Name;

        protected override void Map(IDOTableBuilder<DhikrGroup> builder) {

			builder.MapsTo(x => x.SchemaName("DH").TableName("DhikrGroups"));
            builder.For(x => x.Name).IsTypeOf(EDataType.String).HasMaxLength(512).IsRequired();
            builder.For(x => x.Appeal).IsTypeOf(EDataType.Text);
            builder.For(x => x.Endowment).IsTypeOf(EDataType.Text);
            builder.UniqueKey(t => t.Name);

        }
    }
}
