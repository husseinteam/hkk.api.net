﻿
using HKK.Domain.Models;
using HKK.Core.Domain.Objects;
using HKK.Core.Api.CoreModel.Decorators;
using System;
using System.Collections.Generic;

namespace HKK.Domain.Models.Main {

    public class Source : DOBase<Source> {

        public long ScholarID { get; set; }

        public string Title { get; set; }

        public override string TextValue => Title;

        protected override void Map(IDOTableBuilder<Source> builder) {

            builder.MapsTo(x => { x.SchemaName("HKK").TableName("Sources"); });

            builder.For(d => d.Title).IsTypeOf(EDataType.String).IsRequired().HasMaxLength(512);

            builder.ForeignKey(x => x.ScholarID).References<Scholar>(y => y.ID);
            
        }

    }

}
