﻿
using HKK.Domain.Models;
using HKK.Core.Domain.Objects;
using HKK.Core.Api.CoreModel.Decorators;
using System;
using System.Collections.Generic;

namespace HKK.Domain.Models.Main {

    public class Hadith : DOBase<Hadith> {

        public long TranscriptorID { get; set; }
        public long SubjectID { get; set; }
        public long QuarterID { get; set; }
        public long SourceID { get; set; }

        public string Text { get; set; }

        public override string TextValue => Text;

        protected override void Map(IDOTableBuilder<Hadith> builder) {

            builder.MapsTo(x => { x.SchemaName("HKK").TableName("Hadiths"); });

            builder.For(d => d.Text).IsTypeOf(EDataType.Text).IsRequired();

            builder.ForeignKey(x => x.SourceID).References<Source>(y => y.ID);
            builder.ForeignKey(x => x.TranscriptorID).References<Scholar>(y => y.ID);
            builder.ForeignKey(x => x.SubjectID).References<Subject>(y => y.ID);
            builder.ForeignKey(x => x.QuarterID).References<Quarter>(y => y.ID);

        }

    }

}
