﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HKK.Core.Domain.Objects;

namespace HKK.Domain.Models.Main {
    public class Page : DOBase<Page> {

        public long SourceID { get; set; }

        public int Number { get; set; }

        public string Text { get; set; }

        public byte[] PictureData { get; set; }

        public byte[] AudioData { get; set; }

        public string Notes { get; set; }

        public override string TextValue => Number.ToString();

        protected override void Map(IDOTableBuilder<Page> builder) {

            builder.MapsTo(x => { x.SchemaName("HKK").TableName("Pages"); });

            builder.For(s => s.Number).IsTypeOf(EDataType.Int).IsRequired();
            builder.For(s => s.PictureData).IsTypeOf(EDataType.BinaryMax).IsNullable();
            builder.For(s => s.AudioData).IsTypeOf(EDataType.BinaryMax).IsNullable();
            builder.For(s => s.Text).IsTypeOf(EDataType.Text).IsRequired();
            builder.For(s => s.Notes).IsTypeOf(EDataType.Text).IsNullable();

            builder.ForeignKey(x => x.SourceID).References<Source>(y => y.ID);
            
        }
    }
}
