﻿
using HKK.Domain.Models;
using HKK.Core.Domain.Objects;
using HKK.Core.Api.CoreModel.Decorators;
using System;
using System.Collections.Generic;

namespace HKK.Domain.Models.Main {

    public class Quote : DOBase<Quote> {

        public long ScholarID { get; set; }

        public string Text { get; set; }

        public override string TextValue => Text;

        protected override void Map(IDOTableBuilder<Quote> builder) {

            builder.MapsTo(x => { x.SchemaName("HKK").TableName("Quotes"); });

            builder.For(d => d.Text).IsTypeOf(EDataType.Text).IsRequired();

            builder.ForeignKey(x => x.ScholarID).References<Scholar>(y => y.ID);

        }

    }

}
