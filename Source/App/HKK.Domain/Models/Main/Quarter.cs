﻿
using HKK.Domain.Models;
using HKK.Core.Domain.Objects;
using HKK.Core.Api.CoreModel.Decorators;
using System;
using System.Collections.Generic;

namespace HKK.Domain.Models.Main {

    public class Quarter : DOBase<Quarter> {

        public long TranslationID { get; set; }

        public override string TextValue => String.Empty;

        protected override void Map(IDOTableBuilder<Quarter> builder) {

            builder.MapsTo(x => { x.SchemaName("HKK").TableName("Quarters"); });
            
            builder.ForeignKey(x => x.TranslationID).References<Translation>(y => y.ID);
            
        }

    }

}
