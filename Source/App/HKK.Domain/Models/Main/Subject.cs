﻿
using HKK.Domain.Models;
using HKK.Core.Domain.Objects;
using HKK.Core.Api.CoreModel.Decorators;
using System;
using System.Collections.Generic;

namespace HKK.Domain.Models.Main {

    public class Subject : DOBase<Subject> {

        public long QuarterID { get; set; }

        public long TranslationID { get; set; }

        public override string TextValue => String.Empty;

        protected override void Map(IDOTableBuilder<Subject> builder) {

            builder.MapsTo(x => { x.SchemaName("HKK").TableName("Subjects"); });
            
            builder.ForeignKey(x => x.TranslationID).References<Translation>(y => y.ID);
            builder.ForeignKey(x => x.QuarterID).References<Quarter>(y => y.ID);
           
        }

    }

}
