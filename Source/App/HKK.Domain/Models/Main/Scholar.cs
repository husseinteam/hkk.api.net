﻿
using HKK.Domain.Models;
using HKK.Core.Domain.Objects;
using HKK.Core.Api.CoreModel.Decorators;
using System;
using System.Collections.Generic;

namespace HKK.Domain.Models.Main {

    public class Scholar : DOBase<Scholar> {

        public string FullName { get; set; }

        public DateTime DayOfBirth { get; set; }

        public DateTime DayOfPassAway { get; set; }

        public string Biography { get; set; }

        public byte[] Picture { get; set; }

        public override string TextValue => FullName;

        protected override void Map(IDOTableBuilder<Scholar> builder) {

            builder.MapsTo(x => { x.SchemaName("HKK").TableName("Scholars"); });

            builder.For(d => d.FullName).IsTypeOf(EDataType.String).IsRequired().HasMaxLength(512);
            builder.For(d => d.DayOfBirth).IsTypeOf(EDataType.Date).IsNullable();
            builder.For(d => d.DayOfPassAway).IsTypeOf(EDataType.Date).IsNullable();
            builder.For(d => d.Biography).IsTypeOf(EDataType.Text).IsNullable();
            builder.For(d => d.Picture).IsTypeOf(EDataType.BinaryMax).IsNullable();
            
            builder.UniqueKey(d => d.FullName);
        }

    }

}
