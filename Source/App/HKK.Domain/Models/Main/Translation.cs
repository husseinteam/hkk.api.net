﻿
using HKK.Domain.Models;
using HKK.Core.Domain.Objects;
using HKK.Core.Api.CoreModel.Decorators;
using System;
using System.Collections.Generic;

namespace HKK.Domain.Models.Main {

    public class Translation : DOBase<Translation> {

        public string Key { get; set; }

        public string Value { get; set; }

        public string LanguageCode { get; set; }

        public override string TextValue => Key;

        protected override void Map(IDOTableBuilder<Translation> builder) {

            builder.MapsTo(x => { x.SchemaName("HKK").TableName("Translations"); });

            builder.For(d => d.Key).IsTypeOf(EDataType.String).HasMaxLength(255).IsRequired();
            builder.For(d => d.Value).IsTypeOf(EDataType.Text).IsRequired();
            builder.For(d => d.LanguageCode).IsTypeOf(EDataType.String).HasMaxLength(5).IsRequired();
            
            builder.UniqueKey(b => b.Key, b => b.LanguageCode);
        }

    }

}
