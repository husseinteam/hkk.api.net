﻿using HKK.Core.Api.Controller;
using HKK.Core.Domain.Aggregate;
using HKK.Core.Api.CoreModel.Decorators;
using HKK.Core.Extensions.Static;
using HKK.Domain.Models.Dhikrs;
using HKK.Resources.DomainViews.Dhikrs;
using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HKK.Domain.Views.Dhikrs {

    [ResourceLocator(typeof(DhikrGroupResource))]
    public class DhikrGroupView : AGBase<DhikrGroupView> {

        #region Properties

        [IDLocator]
        public long DhikrGroupID { get; set; }

        [ClientSide("name")]
        public string DhikrGroupName { get; set; }
        [ClientSide("appeal")]
        public string DhikrGroupAppeal { get; set; }
        [ClientSide("endowment")]
        public string DhikrGroupEndowment { get; set; }

        [ClientSide("dhikrs")]
        [HavingCollection("DhikrDhikrGroupID")]
        public List<DhikrView> Dhikrs { get; set; } = new List<DhikrView>();

        public override Type MainDomain => typeof(DhikrGroup);

        public override string TextValue => DhikrGroupName;

        public override string ResourceName => "dhikr-groups";

        #endregion

        protected override void Map(IAGViewBuilder<DhikrGroupView> builder) {
            builder.MapsTo(b => b.SchemaName("DH").ViewName("DhikrGroupsView"))
                .Select<DhikrGroup>(li => li
                    .Map(t => t.ID, t => t.DhikrGroupID)
                    .Map(t => t.Name, t => t.DhikrGroupName)
                    .Map(t => t.Appeal, t => t.DhikrGroupAppeal)
                    .Map(t => t.Endowment, t => t.DhikrGroupEndowment)
                );
        }
    }
}
