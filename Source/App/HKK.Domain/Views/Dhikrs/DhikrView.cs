﻿using HKK.Core.Api.Controller;
using HKK.Core.Domain.Aggregate;
using HKK.Core.Api.CoreModel.Decorators;
using HKK.Domain.Models.Dhikrs;
using HKK.Resources.DomainViews.Dhikrs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HKK.Domain.Views.Dhikrs {

    [ResourceLocator(typeof(DhikrResource))]
    public class DhikrView : AGBase<DhikrView> {

		#region Properties

        [IDLocator]
		public long DhikrID { get; set; }
        public long DhikrDhikrGroupID { get; set; }

        [ClientSide("groupTitle")]
        public string DhikrGroupTitle { get; set; }
        [ClientSide("title")]
        public string DhikrTitle { get; set; }
        [ClientSide("transcript")]
        public string DhikrTranscript { get; set; }
        [ClientSide("arabic")]
        public string DhikrArabic { get; set; }
        [ClientSide("current")]
        public int DhikrCurrent { get; set; }
        [ClientSide("target")]
        public int DhikrTarget { get; set; }
        [ClientSide("order")]
        public int DhikrOrder { get; set; }
  
        #endregion

        public override Type MainDomain => typeof(Dhikr);

        public override string TextValue => DhikrTitle;

        public override string ResourceName => "dhikrs";

        protected override void Map(IAGViewBuilder<DhikrView> builder) {
			builder.MapsTo(b => b.SchemaName("DH").ViewName("DhikrsView"))
				.Select<Dhikr>(li => li
					.Map(t => t.ID, t => t.DhikrID)
					.Map(t => t.Title, t => t.DhikrTitle)
					.Map(t => t.Transcript, t => t.DhikrTranscript)
					.Map(t => t.Arabic, t => t.DhikrArabic)
					.Map(t => t.Current, t => t.DhikrCurrent)
					.Map(t => t.Target, t => t.DhikrTarget)
					.Map(t => t.Order, t => t.DhikrOrder)
                )
				.OuterJoin<DhikrGroup>(li => li
                    .Map(y => y.ID, t => t.DhikrDhikrGroupID, typeof(DhikrGroupView))
                    .Map(y => y.Name, t => t.DhikrGroupTitle)
                ).On<Dhikr>((a, b) => a.ID == b.DhikrGroupID);
		}
	}
}
