﻿
using System;
using HKK.Core.Domain.Aggregate;
using HKK.Domain.Models;
using HKK.Core.Api.CoreModel.Decorators;
using HKK.Domain.Models.Main;
using HKK.Resources.DomainViews.Forecasts;
using HKK.Core.Engines;
using HKK.Core.Domain.Contracts;
using System.Collections.Generic;
using HKK.Domain.Models.Forecasts;
using HKK.Domain.Views.PrayerTimes;
using HKK.Domain.Models.PrayerTimes;
using HKK.Resources.Values;
using System.Threading;
using HKK.Core.Api.Controller;

namespace HKK.Domain.Main.Views {

    [ResourceLocator(typeof(ForecastResource))]
    public class ForecastView : AGBase<ForecastView> {
        
        [IDLocator]
        public long ForecastID { get; set; }

        public long ForecastCalendarID { get; set; }

        [TableColumn(Order = 1)]
        public string CalendarKey { get; set; }

        [TableColumn(Order = 2)]
        public decimal CalendarLatitude { get; set; }

        [TableColumn(Order = 3)]
        public decimal CalendarLongitude { get; set; }

        [TableColumn(Order = 4)]
        public string ForecastSummary { get; set; }

        [TableColumn(Order = 5)]
        public string ForecastIconCode { get; set; }

        [TableColumn(Order = 5)]
        public byte[] ForecastIconData { get; set; }

        [TableColumn(Order = 7)]
        public double ForecastTemperatureMin { get; set; }

        [TableColumn(Order = 8)]
        public double ForecastTemperatureMax { get; set; }

        [TableColumn(Order = 9)]
        public double ForecastWindSpeed { get; set; }

        public DateTime ForecastCurrentDate { get; set; }

        [TableColumn(Order = 10)]
        [ComputedField(EComputedType.FormattedString, typeof(DateTime))]
        public string ForecastTimeString {
            get => ForecastCurrentDate.ToShortTimeString();
            set => ForecastCurrentDate = ForecastCurrentDate;
        }

        [TableColumn(Order = 11)]
        [ComputedField(EComputedType.FormattedString, typeof(DateTime))]
        public string ForecastDateString {
            get => String.Format(Formats.ComparableDateFormat, ForecastCurrentDate);
            set => ForecastCurrentDate = ForecastCurrentDate;
        }

        public override Type MainDomain => typeof(Forecast);

        public override string TextValue => ForecastSummary;

        public override string ResourceName => "forecasts";

        protected override void Map(IAGViewBuilder<ForecastView> builder) {
            builder
                .MapsTo(x => x.SchemaName("FC").ViewName("ForecastView"))
                .Select<Forecast>(li =>
                    li
                    .Map(x => x.ID, x => x.ForecastID)
                    .Map(x => x.Summary, x => x.ForecastSummary)
                    .Map(x => x.IconCode, x => x.ForecastIconCode)
                    .Map(x => x.IconData, x => x.ForecastIconData)
                    .Map(x => x.TemperatureMax, x => x.ForecastTemperatureMax)
                    .Map(x => x.TemperatureMin, x => x.ForecastTemperatureMin)
                    .Map(x => x.WindSpeed, x => x.ForecastWindSpeed)
                    .Map(x => x.CurrentDate, x => x.ForecastCurrentDate)
                ).InnerJoin<Calendar>(li => li
                    .Map(x => x.ID, y => y.ForecastCalendarID, typeof(CalendarView))
                    .Map(x => x.Key, y => y.CalendarKey)
                    .Map(x => x.Latitude, y => y.CalendarLatitude)
                    .Map(x => x.Longitude, y => y.CalendarLongitude))
                .On<Forecast>((x, y) => x.ID == y.CalendarID);
        }
    }
}