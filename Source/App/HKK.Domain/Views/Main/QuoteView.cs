﻿
using System;
using HKK.Core.Domain.Aggregate;
using HKK.Domain.Models;
using HKK.Core.Api.CoreModel.Decorators;
using HKK.Domain.Models.Main;
using System.Globalization;
using HKK.Resources.DomainViews.Main;
using HKK.Core.Engines;
using HKK.Core.Domain.Contracts;
using System.Collections.Generic;
using HKK.Core.Api.Controller;

namespace HKK.Domain.Main.Views {

    [ResourceLocator(typeof(QuoteResource))]
    public class QuoteView : AGBase<QuoteView> {
        
        [IDLocator]
        public long QuoteID { get; set; }

        public long QuoteScholarID { get; set; }

        [TableColumn(Order = 1)]
        public string QuoteText { get; set; }

        [TableColumn(Order = 2)]
        public string ScholarFullName { get; set; }

        public DateTime ScholarDayOfBirth { get; set; }
        [TableColumn(Order = 3)]
        [ComputedField(EComputedType.FormattedString, typeof(double))]
        public string ScholarDayOfBirthComputed {
            get => ScholarDayOfBirth.ToString("d", CultureInfo.DefaultThreadCurrentUICulture);
            set => ScholarDayOfBirth = Convert.ToDateTime(value, CultureInfo.DefaultThreadCurrentUICulture.DateTimeFormat);
        }

        public DateTime ScholarDayOfPassAway { get; set; }
        [TableColumn(Order = 4)]
        [ComputedField(EComputedType.FormattedString, typeof(double))]
        public string ScholarDayOfPassAwayComputed {
            get => ScholarDayOfPassAway.ToString("d", CultureInfo.DefaultThreadCurrentUICulture);
            set => ScholarDayOfPassAway = Convert.ToDateTime(value, CultureInfo.DefaultThreadCurrentUICulture.DateTimeFormat);
        }
        [TableColumn(Order = 5)]
        public string ScholarBiography { get; set; }

        [TableColumn(Order = 6)]
        public byte[] ScholarPicture { get; set; }

        public override Type MainDomain => typeof(Quote);

        public override string TextValue => QuoteText;

        public override string ResourceName => "quotes";

        protected override void Map(IAGViewBuilder<QuoteView> builder) {
            builder
                .MapsTo(x => x.SchemaName("HKK").ViewName("QuoteView"))
                .Select<Quote>(li =>
                    li
                    .Map(x => x.ID, x => x.QuoteID)
                    .Map(x => x.Text, x => x.QuoteText)
                ).OuterJoin<Scholar>(li =>
                    li
                    .Map(x => x.ID, x => x.QuoteScholarID, typeof(ScholarView))
                    .Map(x => x.FullName, x => x.ScholarFullName)
                    .Map(x => x.Biography, x => x.ScholarBiography)
                    .Map(x => x.DayOfBirth, x => x.ScholarDayOfBirth)
                    .Map(x => x.DayOfPassAway, x => x.ScholarDayOfPassAway)
                    .Map(x => x.Picture, x => x.ScholarPicture)
                ).On<Quote>((a, b) => a.ID == b.ScholarID);
        }
    }
}