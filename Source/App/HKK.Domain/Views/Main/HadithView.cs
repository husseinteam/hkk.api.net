﻿
using System;
using HKK.Core.Domain.Aggregate;
using HKK.Domain.Models;
using HKK.Core.Api.CoreModel.Decorators;
using HKK.Domain.Models.Main;
using System.Globalization;
using HKK.Resources.DomainViews.Main;
using HKK.Core.Engines;
using HKK.Core.Domain.Contracts;
using System.Collections.Generic;
using HKK.Core.Api.Controller;

namespace HKK.Domain.Main.Views {

    [ResourceLocator(typeof(HadithResource))]
    public class HadithView : AGBase<HadithView> {

        [IDLocator]
        public long HadithID { get; set; }

        public long HadithTranscriptorID { get; set; }

        public long HadithQuarterTranslationID { get; set; }

        public long HadithSubjectTranslationID { get; set; }

        public long HadithSourceID { get; set; }

        public long HadithSubjectID { get; set; }

        public long HadithQuarterID { get; set; }

        [TableColumn(Order = 1)]
        public string HadithText { get; set; }

        [TableColumn(Order = 2)]
        public string HadithSourceTitle { get; set; }

        [TableColumn(Order = 3)]
        public string HadithQuarterKey { get; set; }

        [TableColumn(Order = 4)]
        public string HadithQuarterValue { get; set; }

        [TableColumn(Order = 5)]
        public string HadithQuarterLanguageCode { get; set; }

        [TableColumn(Order = 6)]
        public string HadithSubjectKey { get; set; }

        [TableColumn(Order = 7)]
        public string HadithSubjectValue { get; set; }

        [TableColumn(Order = 8)]
        public string HadithSubjectLanguageCode { get; set; }

        [TableColumn(Order = 9)]
        public string HadithTranscriptorFullName { get; set; }

        public override Type MainDomain => typeof(Hadith);

        public override string TextValue => HadithText;

        public override string ResourceName => "hadiths";

        protected override void Map(IAGViewBuilder<HadithView> builder) {
            builder
                .MapsTo(x => x.SchemaName("HKK").ViewName("HadithView"))
                .Select<Hadith>(li =>
                    li
                    .Map(x => x.ID, x => x.HadithID)
                    .Map(x => x.Text, x => x.HadithText)
                )
                .OuterJoin<Source>(li =>
                    li
                    .Map(x => x.ID, x => x.HadithSourceID, typeof(SourceView))
                    .Map(x => x.Title, x => x.HadithSourceTitle)
                ).On<Hadith>((a, b) => a.ID == b.SourceID)
                .OuterJoin<Scholar>(li =>
                    li
                    .Map(x => x.ID, x => x.HadithTranscriptorID, typeof(ScholarView))
                    .Map(x => x.FullName, x => x.HadithTranscriptorFullName)
                ).On<Hadith>((a, b) => a.ID == b.TranscriptorID)
                .OuterJoin<Subject>(li =>
                    li
                    .Map(x => x.ID, x => x.HadithSubjectID, typeof(SubjectView))
                ).On<Hadith>((a, b) => a.ID == b.SourceID)
                .OuterJoin<Translation>(li =>
                    li
                    .Map(x => x.ID, y => y.HadithSubjectTranslationID, typeof(TranslationView))
                    .Map(x => x.Key, x => x.HadithSubjectKey)
                    .Map(x => x.Value, x => x.HadithSubjectValue)
                    .Map(x => x.LanguageCode, x => x.HadithSubjectLanguageCode)
                ).On<Subject>((a, b) => a.ID == b.TranslationID)
                .OuterJoin<Quarter>(li =>
                    li
                    .Map(x => x.ID, x => x.HadithQuarterID, typeof(QuarterView))
                ).On<Hadith>((a, b) => a.ID == b.SourceID)
                .OuterJoin<Translation>(li =>
                    li
                    .Map(x => x.ID, y => y.HadithQuarterTranslationID, typeof(TranslationView))
                    .Map(x => x.Key, x => x.HadithQuarterKey)
                    .Map(x => x.Value, x => x.HadithQuarterValue)
                    .Map(x => x.LanguageCode, x => x.HadithQuarterLanguageCode)
                ).On<Quarter>((a, b) => a.ID == b.TranslationID);
        }
    }
}