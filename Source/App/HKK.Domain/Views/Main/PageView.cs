﻿
using System;
using HKK.Core.Domain.Aggregate;
using HKK.Domain.Models;
using HKK.Core.Api.CoreModel.Decorators;
using HKK.Domain.Models.Main;
using System.Globalization;
using HKK.Resources.DomainViews.Main;
using HKK.Core.Engines;
using HKK.Core.Domain.Contracts;
using System.Collections.Generic;
using HKK.Core.Api.Controller;

namespace HKK.Domain.Main.Views {

    [ResourceLocator(typeof(PageResource))]
    public class PageView : AGBase<PageView> {

        [IDLocator]
        public long PageID { get; set; }

        public long PageSourceID { get; set; }

        [TableColumn(Order = 1)]
        public int PageNumber { get; set; }

        [TableColumn(Order = 2)]
        public string PageText { get; set; }

        [TableColumn(Order = 3)]
        public byte[] PagePictureData { get; set; }

        [TableColumn(Order = 4)]
        public byte[] PageAudioData { get; set; }

        [TableColumn(Order = 5)]
        public string PageNotes { get; set; }

        [TableColumn(Order = 6)]
        public string PageSourceTitle { get; set; }

        public override Type MainDomain => typeof(Page);

        public override string TextValue => PageNumber.ToString();

        public override string ResourceName => "pages";

        protected override void Map(IAGViewBuilder<PageView> builder) {
            builder
                .MapsTo(x => x.SchemaName("HKK").ViewName("PageView"))
                .Select<Page>(li =>
                    li
                    .Map(x => x.ID, x => x.PageID)
                    .Map(x => x.Number, x => x.PageNumber)
                    .Map(x => x.Text, x => x.PageText)
                    .Map(x => x.PictureData, x => x.PagePictureData)
                    .Map(x => x.AudioData, x => x.PageAudioData)
                    .Map(x => x.Notes, x => x.PageNotes)
                )
                .OuterJoin<Source>(li =>
                    li
                    .Map(x => x.ID, x => x.PageSourceID, typeof(SourceView))
                    .Map(x => x.Title, x => x.PageSourceTitle)
                ).On<Page>((a, b) => a.ID == b.SourceID);
        }
    }
}