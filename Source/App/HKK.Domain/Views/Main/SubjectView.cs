﻿using System;
using HKK.Core.Domain.Aggregate;
using HKK.Domain.Models;
using HKK.Core.Api.CoreModel.Decorators;
using HKK.Domain.Models.Main;
using System.Globalization;
using HKK.Resources.DomainViews.Main;
using HKK.Core.Engines;
using HKK.Core.Domain.Contracts;
using System.Collections.Generic;
using HKK.Core.Api.Controller;

namespace HKK.Domain.Main.Views {

    [ResourceLocator(typeof(SubjectResource))]
    public class SubjectView : AGBase<SubjectView> {

        [IDLocator]
        public long SubjectID { get; set; }

        public long SubjectTranslationID { get; set; }

        public long SubjectQuarterTranslationID { get; set; }

        public long SubjectQuarterID { get; set; }

        [TableColumn(Order = 1)]
        public string SubjectTranslationKey { get; set; }

        [TableColumn(Order = 2)]
        public string SubjectTranslationValue { get; set; }

        [TableColumn(Order = 3)]
        public string SubjectTranslationLanguageCode { get; set; }

        [TableColumn(Order = 4)]
        public string SubjectQuarterTranslationKey { get; set; }

        [TableColumn(Order = 5)]
        public string SubjectQuarterTranslationValue { get; set; }

        [TableColumn(Order = 6)]
        public string SubjectQuarterTranslationLanguageCode { get; set; }

        public override Type MainDomain => typeof(Subject);

        public override string TextValue => SubjectTranslationKey;

        public override string ResourceName => "subjects";

        protected override void Map(IAGViewBuilder<SubjectView> builder) {
            builder
                .MapsTo(x => x.SchemaName("HKK").ViewName("SubjectView"))
                .Select<Subject>(li =>
                    li
                    .Map(x => x.ID, x => x.SubjectID)
                ).OuterJoin<Quarter>(li =>
                    li
                    .Map(x => x.ID, x => x.SubjectQuarterID, typeof(QuarterView))
                ).On<Subject>((a, b) => a.ID == b.QuarterID)
                .OuterJoin<Translation>(li =>
                    li
                    .Map(x => x.ID, x => x.SubjectTranslationID, typeof(TranslationView))
                    .Map(x => x.Key, x => x.SubjectTranslationKey)
                    .Map(x => x.Value, x => x.SubjectTranslationValue)
                    .Map(x => x.LanguageCode, x => x.SubjectTranslationLanguageCode)
                ).On<Subject>((a, b) => a.ID == b.TranslationID)
                .OuterJoin<Translation>(li =>
                    li
                    .Map(x => x.ID, x => x.SubjectQuarterTranslationID, typeof(TranslationView))
                    .Map(x => x.Key, x => x.SubjectQuarterTranslationKey)
                    .Map(x => x.Value, x => x.SubjectQuarterTranslationValue)
                    .Map(x => x.LanguageCode, x => x.SubjectQuarterTranslationLanguageCode)
                ).On<Quarter>((a, b) => a.ID == b.TranslationID);
        }
    }
}