﻿
using System;
using HKK.Core.Domain.Aggregate;
using HKK.Domain.Models;
using HKK.Core.Api.CoreModel.Decorators;
using HKK.Domain.Models.Main;
using System.Globalization;
using HKK.Resources.DomainViews.Main;
using HKK.Core.Engines;
using HKK.Core.Domain.Contracts;
using System.Collections.Generic;
using HKK.Core.Api.Controller;

namespace HKK.Domain.Main.Views {

    [ResourceLocator(typeof(TranslationResource))]
    public class TranslationView : AGBase<TranslationView> {
        
        [IDLocator]
        public long TranslationID { get; set; }

        [TableColumn(Order = 1)]
        public string TranslationKey { get; set; }

        [TableColumn(Order = 2)]
        public string TranslationValue { get; set; }

        [TableColumn(Order = 3)]
        public string TranslationLanguageCode { get; set; }
        
        public override Type MainDomain => typeof(Translation);

        public override string TextValue => TranslationKey;

        public override string ResourceName => "translations";

        protected override void Map(IAGViewBuilder<TranslationView> builder) {
            builder
                .MapsTo(x => x.SchemaName("HKK").ViewName("TranslationView"))
                .Select<Translation>(li =>
                    li
                    .Map(x => x.ID, x => x.TranslationID)
                    .Map(x => x.Key, x => x.TranslationKey)
                    .Map(x => x.Value, x => x.TranslationValue)
                    .Map(x => x.LanguageCode, x => x.TranslationLanguageCode)
                );
        }
    }
}