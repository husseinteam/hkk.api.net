﻿
using System;
using HKK.Core.Domain.Aggregate;
using HKK.Domain.Models;
using HKK.Core.Api.CoreModel.Decorators;
using HKK.Domain.Models.Main;
using System.Globalization;
using HKK.Resources.DomainViews.Main;
using HKK.Core.Engines;
using HKK.Core.Domain.Contracts;
using System.Collections.Generic;
using HKK.Core.Api.Controller;

namespace HKK.Domain.Main.Views {

    [ResourceLocator(typeof(ScholarResource))]
    public class ScholarView : AGBase<ScholarView> {
        
        [IDLocator]
        public long ScholarID { get; set; }

        [TableColumn(Order = 1)]
        public string ScholarFullName { get; set; }

        public DateTime ScholarDayOfBirth { get; set; }
        [TableColumn(Order = 2)]
        [ComputedField(EComputedType.FormattedString, typeof(double))]
        public string ScholarDayOfBirthComputed {
            get => ScholarDayOfBirth.ToString("d", CultureInfo.DefaultThreadCurrentUICulture);
            set => ScholarDayOfBirth = Convert.ToDateTime(value, CultureInfo.DefaultThreadCurrentUICulture.DateTimeFormat);
        }

        public DateTime ScholarDayOfPassAway { get; set; }
        [TableColumn(Order = 3)]
        [ComputedField(EComputedType.FormattedString, typeof(double))]
        public string ScholarDayOfPassAwayComputed {
            get => ScholarDayOfPassAway.ToString("d", CultureInfo.DefaultThreadCurrentUICulture);
            set => ScholarDayOfPassAway = Convert.ToDateTime(value, CultureInfo.DefaultThreadCurrentUICulture.DateTimeFormat);
        }
        [TableColumn(Order = 4)]
        public string ScholarBiography { get; set; }

        [TableColumn(Order = 5)]
        public byte[] ScholarPicture { get; set; }

        [HavingCollection("HadithTranscriptorID")]
        public ICollection<HadithView> Hadiths { get; set; } = new List<HadithView>();

        [HavingCollection("QuoteScholarID")]
        public ICollection<QuoteView> Quotes { get; set; } = new List<QuoteView>();

        public override Type MainDomain => typeof(Scholar);

        public override string TextValue => ScholarFullName;

        public override string ResourceName => "scholars";

        protected override void Map(IAGViewBuilder<ScholarView> builder) {
            builder
                .MapsTo(x => x.SchemaName("HKK").ViewName("ScholarView"))
                .Select<Scholar>(li =>
                    li
                    .Map(x => x.ID, x => x.ScholarID)
                    .Map(x => x.FullName, x => x.ScholarFullName)
                    .Map(x => x.DayOfBirth, x => x.ScholarDayOfBirth)
                    .Map(x => x.DayOfPassAway, x => x.ScholarDayOfPassAway)
                    .Map(x => x.Picture, x => x.ScholarPicture)
                    .Map(x => x.Biography, x => x.ScholarBiography)
                );
        }
    }
}