﻿
using System;
using HKK.Core.Domain.Aggregate;
using HKK.Domain.Models;
using HKK.Core.Api.CoreModel.Decorators;
using HKK.Domain.Models.Main;
using System.Globalization;
using HKK.Resources.DomainViews.Main;
using HKK.Core.Engines;
using HKK.Core.Domain.Contracts;
using System.Collections.Generic;
using HKK.Core.Api.Controller;

namespace HKK.Domain.Main.Views {

    [ResourceLocator(typeof(QuarterResource))]
    public class QuarterView : AGBase<QuarterView> {
        
        [IDLocator]
        public long QuarterID { get; set; }
        
        public long QuarterTranslationID { get; set; }

        [TableColumn(Order = 1)]
        public string QuarterTranslationKey { get; set; }

        [TableColumn(Order = 2)]
        public string QuarterTranslationValue { get; set; }

        [TableColumn(Order = 3)]
        public string QuarterTranslationLanguageCode { get; set; }

        [HavingCollection("SubjectQuarterID")]
        public ICollection<SubjectView> Subjects { get; set; } = new List<SubjectView>();
        
        public override Type MainDomain => typeof(Quarter);

        public override string TextValue => QuarterTranslationKey;

        public override string ResourceName => "quarters";

        protected override void Map(IAGViewBuilder<QuarterView> builder) {
            builder
                .MapsTo(x => x.SchemaName("HKK").ViewName("QuarterView"))
                .Select<Quarter>(li =>
                    li
                    .Map(x => x.ID, x => x.QuarterID)
                ).OuterJoin<Translation>(li =>
                    li
                    .Map(x => x.ID, x => x.QuarterTranslationID, typeof(TranslationView))
                    .Map(x => x.Key, x => x.QuarterTranslationKey)
                    .Map(x => x.Value, x => x.QuarterTranslationValue)
                    .Map(x => x.LanguageCode, x => x.QuarterTranslationLanguageCode)
                ).On<Quarter>((a, b) => a.ID == b.TranslationID);
        }
    }
    
}