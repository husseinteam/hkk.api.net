﻿
using System;
using HKK.Core.Domain.Aggregate;
using HKK.Domain.Models;
using HKK.Core.Api.CoreModel.Decorators;
using HKK.Domain.Models.Main;
using System.Globalization;
using HKK.Resources.DomainViews.Main;
using HKK.Core.Engines;
using HKK.Core.Domain.Contracts;
using System.Collections.Generic;
using HKK.Core.Api.Controller;

namespace HKK.Domain.Main.Views {

    [ResourceLocator(typeof(SourceResource))]
    public class SourceView : AGBase<SourceView> {
        
        [IDLocator]
        public long SourceID { get; set; }
        
        public long SourceScholarID { get; set; }

        [TableColumn(Order = 1)]
        public string SourceTitle { get; set; }

        [TableColumn(Order = 2)]
        public string ScholarFullName { get; set; }

        public DateTime ScholarDayOfBirth { get; set; }
        [TableColumn(Order = 3)]
        [ComputedField(EComputedType.FormattedString, typeof(double))]
        public string ScholarDayOfBirthComputed {
            get => ScholarDayOfBirth.ToString("d", CultureInfo.DefaultThreadCurrentUICulture);
            set => ScholarDayOfBirth = Convert.ToDateTime(value, CultureInfo.DefaultThreadCurrentUICulture.DateTimeFormat);
        }

        public DateTime ScholarDayOfPassAway { get; set; }
        [TableColumn(Order = 4)]
        [ComputedField(EComputedType.FormattedString, typeof(double))]
        public string ScholarDayOfPassAwayComputed {
            get => ScholarDayOfPassAway.ToString("d", CultureInfo.DefaultThreadCurrentUICulture);
            set => ScholarDayOfPassAway = Convert.ToDateTime(value, CultureInfo.DefaultThreadCurrentUICulture.DateTimeFormat);
        }
        [TableColumn(Order = 5)]
        public string ScholarBiography { get; set; }

        [TableColumn(Order = 6)]
        public byte[] ScholarPicture { get; set; }
        
        [HavingCollection("HadithSourceID")]
        public ICollection<HadithView> Hadiths { get; set; } = new List<HadithView>();

        [HavingCollection("PageSourceID")]
        public ICollection<PageView> Pages { get; set; } = new List<PageView>();

        public override Type MainDomain => typeof(Source);

        public override string TextValue => SourceTitle;

        public override string ResourceName => "sources";

        protected override void Map(IAGViewBuilder<SourceView> builder) {
            builder
                .MapsTo(x => x.SchemaName("HKK").ViewName("SourceView"))
                .Select<Source>(li =>
                    li
                    .Map(x => x.ID, x => x.SourceID)
                    .Map(x => x.Title, x => x.SourceTitle)
                ).OuterJoin<Scholar>(li =>
                    li
                    .Map(x => x.ID, x => x.SourceScholarID, typeof(ScholarView))
                    .Map(x => x.FullName, x => x.ScholarFullName)
                    .Map(x => x.DayOfBirth, x => x.ScholarDayOfBirth)
                    .Map(x => x.DayOfPassAway, x => x.ScholarDayOfPassAway)
                    .Map(x => x.Picture, x => x.ScholarPicture)
                    .Map(x => x.Biography, x => x.ScholarBiography)
                ).On<Source>((a, b) => a.ID == b.ScholarID);
        }
    }
}