﻿using HKK.Core.Api.Controller;
using HKK.Core.Domain.Aggregate;
using HKK.Core.Api.CoreModel.Decorators;
using HKK.Domain.Main.Views;
using HKK.Domain.Models.PrayerTimes;
using HKK.Resources.DomainViews.PrayerTimes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HKK.Domain.Views.PrayerTimes {

    [ResourceLocator(typeof(CalendarResource))]
    public class CalendarView : AGBase<CalendarView> {

		#region Properties

        [IDLocator]
		public long CalendarID { get; set; }

        [TableColumn(Order = 1)]
        public string CalendarKey { get; set; }
        [TableColumn(Order = 2)]
        public decimal CalendarLatitude { get; set; }
        [TableColumn(Order = 3)]
        public decimal CalendarLongitude { get; set; }

        [TableColumn(Order = 4)]
        public int CalendarMonth { get; set; }

        [TableColumn(Order = 5)]
        public int CalendarYear { get; set; }

        [HavingCollection("TimingCalendarID")]
        public List<TimingView> Timings { get; set; } = new List<TimingView>();

        [HavingCollection("ForecastCalendarID")]
        public List<ForecastView> Forecasts { get; set; } = new List<ForecastView>();

        public override Type MainDomain => typeof(Calendar);

        public override string TextValue => $"{CalendarLatitude} : {CalendarLongitude}";

        public override string ResourceName => "calendars";

        #endregion

        protected override void Map(IAGViewBuilder<CalendarView> builder) {
			builder.MapsTo(b => b.SchemaName("PYR").ViewName("CalendarsView"))
				.Select<Calendar>(li => li
					.Map(t => t.ID, t => t.CalendarID)
					.Map(t => t.Key, t => t.CalendarKey)
					.Map(t => t.Latitude, t => t.CalendarLatitude)
					.Map(t => t.Longitude, t => t.CalendarLongitude)
					.Map(t => t.Month, t => t.CalendarMonth)
					.Map(t => t.Year, t => t.CalendarYear));
		}
	}
}
