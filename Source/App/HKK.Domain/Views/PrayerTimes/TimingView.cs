﻿using HKK.Core.Api.Controller;
using HKK.Core.Domain.Aggregate;
using HKK.Core.Api.CoreModel.Decorators;
using HKK.Domain.Models.PrayerTimes;
using HKK.Resources.DomainViews.PrayerTimes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HKK.Domain.Views.PrayerTimes {

    [ResourceLocator(typeof(TimingResource))]
    public class TimingView : AGBase<TimingView> {

        #region Properties

        [IDLocator]
        public long TimingID { get; set; }
        
        public long TimingCalendarID { get; set; }

        [TableColumn(Order = 1)]
        public DateTime TimingReadableDate { get; set; }
        [TableColumn(Order = 2)]
        public int TimingTimestamp { get; set; }

        [TableColumn(Order = 3)]
        public string TimingFajr { get; set; }
        [TableColumn(Order = 4)]
        public string TimingSunrise { get; set; }
        [TableColumn(Order = 5)]
        public string TimingDhuhr { get; set; }
        [TableColumn(Order = 6)]
        public string TimingAsr { get; set; }
        [TableColumn(Order = 7)]
        public string TimingSunset { get; set; }
        [TableColumn(Order = 8)]
        public string TimingMaghrib { get; set; }
        [TableColumn(Order = 9)]
        public string TimingIsha { get; set; }
        [TableColumn(Order = 10)]
        public string TimingImsak { get; set; }
        [TableColumn(Order = 11)]
        public string TimingMidnight { get; set; }

        [TableColumn(Order = 12)]
        public decimal CalendarLatitude { get; set; }
        [TableColumn(Order = 13)]
        public decimal CalendarLongitude { get; set; }
        #endregion

        public override Type MainDomain => typeof(Timing);

        public override string TextValue => TimingReadableDate.ToShortDateString();

        public override string ResourceName => "timings";

        protected override void Map(IAGViewBuilder<TimingView> builder) {
            builder.MapsTo(b => b.SchemaName("PYR").ViewName("TimingsView"))
                .Select<Timing>(li => li
                    .Map(t => t.ID, t => t.TimingID)
                    .Map(t => t.ReadableDate, t => t.TimingReadableDate)
                    .Map(t => t.Fajr, t => t.TimingFajr)
                    .Map(t => t.Sunrise, t => t.TimingSunrise)
                    .Map(t => t.Dhuhr, t => t.TimingDhuhr)
                    .Map(t => t.Asr, t => t.TimingAsr)
                    .Map(t => t.Sunset, t => t.TimingSunset)
                    .Map(t => t.Maghrib, t => t.TimingMaghrib)
                    .Map(t => t.Isha, t => t.TimingIsha)
                    .Map(t => t.Imsak, t => t.TimingImsak)
                    .Map(t => t.Midnight, t => t.TimingMidnight))
                .OuterJoin<Calendar>(li => li
                    .Map(y => y.ID, t => t.TimingCalendarID, typeof(CalendarView))
                    .Map(y => y.Latitude, t => t.CalendarLatitude)
                    .Map(y => y.Longitude, t => t.CalendarLongitude)
                ).On<Timing>((a, b) => a.ID == b.CalendarID);
        }
    }
}
