﻿using HKK.Resources.Values;
using HKK.Domain.Models;
using HKK.Core.Api.Static;
using HKK.Core.Domain.Data;
using HKK.Core.Domain.Enums;
using HKK.Core.Engines;
using System;
using HKK.Core.Api.CoreModel.Models.Membership;
using HKK.Core.Api.Controller;
using HKK.Core.Api.CoreModel.Models.Data;

namespace HKK.Domain {
    public static class Builder {

        public static bool ShouldRebuild() {
#if DEBUG
            var should = SDataEngine.GenerateDOEngine<Dictribute>().SelectSingleSync(dct => dct.Key == "dispose")
                .ContainsSingleOut<Dictribute>(out var item) && item.Value == "1";
            return should || !SDataEngine.SchemaContainsTable(CurrentSchema);
#else
            return !SDataEngine.IsDomainBuild(CurrentSchema);
#endif
        }

        public static void Bootstrap() {
            SDbParams.SetConnectionString(HKKValues.LocalhostConnectionString, EConnectionStringMode.Debug, EServerType.MSSql);
            SDbParams.SetConnectionString(HKKValues.RemoteConnectionString, EConnectionStringMode.Release, EServerType.MSSql);
            SRPIValues.CurrentDebugHost = HKKValues.DebugHost;
            SRPIValues.CurrentRPIKey = HKKValues.RPIKey;
            SRPIValues.CurrentProductionHost = HKKValues.ProductionHost;
            SRPIValues.CurrentRPIVersion = $"v{HKKValues.RPIVersion}";
        }


        public static void RebuildDomain() {
            SDataEngine.BuildDomain(typeof(User).Assembly.GetName(), typeof(Builder).Assembly.GetName());
        }

        public static void RebuildConditional(bool forceToRebuild = false) {
            if (forceToRebuild || Builder.ShouldRebuild()) {
                Builder.RebuildDomain();
            }
        }

        internal static string CurrentConnectionString {
            get {
                return SDbParams.ConnectionString();
            }
        }

        internal static string CurrentSchema {
            get {
                return SDbParams.IsLocalMachine() ? HKKValues.LocalhostSchemaName : HKKValues.RemoteSchemaName;
            }
        }
    }
}
