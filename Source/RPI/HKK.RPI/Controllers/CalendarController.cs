﻿using HKK.Core.Api.Controller;
using HKK.Core.Api.CoreModel.Decorators;
using HKK.Core.Api.Decorators;
using HKK.Core.Engines;
using HKK.Core.Extensions.Static;
using HKK.Domain.Views.PrayerTimes;
using HKK.Engine.Calendar;
using HKK.Engine.Calendar.Vectors;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Cors;

namespace Hkk.Api.Controllers {
    public class CalendarController : GenericController {
        public CalendarController() : base(typeof(CalendarView)) {
        }

        public override Type AggregateType => typeof(CalendarView);

        [RouteGet(path: "one-month/latitude/{latitude}/longitude/{longitude}/month/{month:int:min(1):max(12)}/year/{year:int:min(2016):max(2099)}",
            ArgumentTypes = new[] { typeof(string), typeof(string), typeof(int), typeof(int) })]
        [Tahditli, Kaide("lists")]
        public async Task<IHttpActionResult> OneMonth(string latitude, string longitude, int month, int year) {
            var resp = await SCalendarEngine.UpsertMonthlyTimings(month, year);
            var calendars = resp.Items.CastToList(typeof(CalendarView)).As<IEnumerable<CalendarView>>();
            CalendarView foundCalendar = calendars.FirstOrDefault();
            var calendarLatitude = latitude.ParseExactDecimal();
            var calendarLongitude = longitude.ParseExactDecimal();
            foreach (var cv in calendars.Skip(1)) {
                if (foundCalendar.CalendarLatitude > cv.CalendarLatitude) {
                    if (foundCalendar.CalendarLatitude > calendarLatitude && calendarLatitude > cv.CalendarLatitude) {
                        foundCalendar = cv;
                    }
                } else if (cv.CalendarLatitude > calendarLatitude && calendarLatitude > foundCalendar.CalendarLatitude) {
                    foundCalendar = cv;
                }
            }
            return foundCalendar.ParseGenericJson(Request);
        }
    }

}
