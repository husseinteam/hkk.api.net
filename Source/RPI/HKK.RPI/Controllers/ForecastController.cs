﻿using HKK.Core.Api.Controller;
using HKK.Core.Api.Decorators;
using HKK.Core.Api.CoreModel.Decorators;
using HKK.Core.Domain.Contracts;
using HKK.Core.Engines;
using HKK.Core.Extensions.Static;
using HKK.Domain.Main.Views;
using HKK.Domain.Views.PrayerTimes;
using HKK.Engine.Forecasts;
using HKK.Engine.Forecasts.Vectors;
using HKK.Resources.Values;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Cors;

namespace Hkk.Api.Controllers {
    public class ForecastController : GenericController {
        public ForecastController() : base(typeof(ForecastView)) {
        }

        public override Type AggregateType => typeof(ForecastView);

        [RouteGet(path: "multiday/city/{city:alpha:minlength(2)}/countryCode/{countryCode:alpha:length(2)=TR}",
            ArgumentTypes = new[] { typeof(string), typeof(string) })]
        [Tahditli, Kaide("lists")]
        public async Task<IHttpActionResult> GetMultiDayForecasts(string city, string countryCode) {
            return (await SForecastEngine.UpsertCountryWideWeeklyForecasts(city, countryCode)).ParseJsonToClient(Request);
        }

    }
}
