﻿using HKK.Core.Api.Controller;
using HKK.Core.Api.CoreModel.Decorators;
using HKK.Core.Api.Decorators;
using HKK.Core.Api.Static;
using HKK.Core.Domain.Aggregate;
using HKK.Core.Extensions.Static;
using HKK.Domain;
using HKK.Engine.Hadiths;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Cors;

namespace Hkk.Api.Controllers {
    [RoutePrefix("will/seed")]
    public class SeedController : ApiController {

        [HttpGet]
        [Route("hadiths/{take:int:min(1)=571}")]
        public IHttpActionResult Hadiths(int take) {
            Builder.RebuildConditional(forceToRebuild: true);
            var multiResponse = SHadithEngine.CollectHadiths()
                .Take(take)
                .InsertUs();
            return multiResponse.ParseJsonToClient(Request);
        }
    }

}
