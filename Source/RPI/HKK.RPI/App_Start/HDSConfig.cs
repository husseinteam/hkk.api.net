﻿
using HKK.Core.Api.Static;
using HKK.Domain;
using HKK.Domain.Main.Views;
using HKK.Domain.Models.Dhikrs;
using HKK.Domain.Views.Dhikrs;
using HKK.Engine;
using HKK.Engine.Hadiths;
using HKK.Engine.Dhikrs;
using System.Web.Http;

namespace HKK.RPI {
    public static class HDSConfig {
        public static void Register(HttpConfiguration config) {
            Builder.Bootstrap();
            SRouter.Configure<WebApiApplication, DhikrView>(config);

            var title = "Hakkani Zikri";
            SRouter.SubscribeSeeder<DhikrGroupView>(
                (agg) => agg.DhikrGroupName == title,
                () => new[] { SDhikrEngine.SeedHakkaniDhikr(title) }
            );
            SRouter.SubscribeSeeder<HadithView>(
                () => SHadithEngine.CollectHadiths()
            );
            SRouter.Seed(()=> Builder.RebuildConditional(forceToRebuild: true));
        }
    }
}
