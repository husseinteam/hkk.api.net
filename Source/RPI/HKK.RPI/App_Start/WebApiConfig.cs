﻿using HKK.Core.Api.Controller;
using HKK.Core.Api.Static;
using HKK.Core.Extensions.Static;
using HKK.Domain.Models.Dhikrs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web.Http;
using System.Web.Http.Cors;

namespace HKK.RPI {
    public static class WebApiConfig {
        public static void Register(HttpConfiguration config) {
            HDSConfig.Register(config);
            config.EnableHDSCors();
            config.EnsureInitialized();
        }
        static void EnableHDSCors(this HttpConfiguration config) {
            var exposedHeaders = "x-hds-tahdit, X-Content-Range";
            var headers = $"Content-Type, X-CSRF-Token, X-Requested-With, Accept, Accept-Version, Content-Length, Content-MD5, Date, X-Api-Version, X-File-Name, {exposedHeaders}";
            var methods = "GET,HEAD,POST,DEBUG,PUT,DELETE,PATCH,OPTIONS";
            EnableCorsAttribute cors;
            if (Assembly.GetExecutingAssembly().IsDebugBuild()) {
                cors = new EnableCorsAttribute("*", headers, methods, exposedHeaders);
            } else {
                cors = new EnableCorsAttribute(SRPIValues.CurrentDebugHost, headers, methods, exposedHeaders);
            }
            config.EnableCors(cors);
        }
    }

}
